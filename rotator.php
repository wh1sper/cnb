<?php

include("inc/scripts/conf.php");
include('inc/scripts/rotator_base.php');
include('inc/scripts/rotator_regular.php');
include('inc/scripts/rotator_zoom.php');
include('inc/scripts/rotator_context.php');

$class = "RotatorRegular";

if(isset($_GET['type']))
{
    if($_GET['type']=='context') $class = "RotatorContext";
    if($_GET['type']=='zoom') $class = "RotatorZoom";
}

$rotator = new $class;
$banner = $rotator->rotate();
if(!$banner) exit;

$rotator->out();

?>