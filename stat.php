<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 21.11.14
 * Time: 11:49
 */

include("conf.php");

$db = new PDO($db['connect_string'],$db['user'],$db['password']);;
$db->exec("set names cp1251");

if(!isset($_GET['code'])) die();

// �������� ���������� ����
$str = $db->prepare("SELECT c.id,c.name,cl.name as clientname FROM campaign c INNER JOIN client cl ON c.id_client=cl.id WHERE c.code=:code");
$str->bindParam(":code",$_GET['code']);
$str->execute();
$campaign = $str->fetch(PDO::FETCH_ASSOC);
if(sizeof($campaign)) {
    $id_campaign = $campaign['id'];
    $name = $campaign['name'];
    $client = $campaign['clientname'];
}
if(!isset($id_campaign)) die('');

// ����� ����� �� ����� ��� ��������
$str = $db->prepare("SELECT SUM(sc.impressions) as imp,SUM(sc.clicks) as clk,date FROM stat_counts sc
                     INNER JOIN banner b ON sc.id_banner=b.id
                     WHERE b.id_campaign=:id_campaign
                     GROUP BY sc.date
                     ORDER BY sc.date
                     LIMIT 30
                     ");
$str->bindParam(":id_campaign",$id_campaign);
$str->execute();

?>
<html>
<head>
    <style type="text/css">
        body { font-family: Arial; font-size:15px; margin-left:30px; margin-top:30px; }
        table { border-collapse: collapse; }
        table td { border:1px solid #ccc; padding:3px; min-width:100px; }
        h2 { font-size:18px; }
        h1 { font-size:21px; }
        h3 { font-size:16px; margin:40px 0px; color:blue; }
    </style>
</head>

<? if(isset($name)): ?>
    <h1>��������: <?= $name ?></h1>
<? endif; ?>

<? if(isset($client)):?>
    <h2>������: <?= $client ?></h2>
<? endif; ?>

<h3>���������� �� ������</h3>
<table cellspacing="0">
<thead>
<th>����</th><th>������</th><th>�����</th><th>CTR</th>
</thead>
<tbody>

<?php

$daysum['imp'] = $daysum['clk'] = 0;

foreach($str->fetchAll(PDO::FETCH_ASSOC) as $row)
{
    $row['ctr'] = round((($row['clk'] / $row['imp']) * 100),2);
    $daysum['imp'] += $row['imp'];
    $daysum['clk'] += $row['clk'];
    echo "<tr><td>".$row['date']."</td><td>".$row['imp']."</td><td>".$row['clk'].'</td><td>'.$row['ctr'].'</td></tr>';
}

$daysum['ctr'] = round((($daysum['clk'] / $daysum['imp']) * 100),2);

echo "<tr><td><i>�����</i></td><td>".$daysum['imp']."</td><td>".$daysum['clk'].'</td><td>'.$daysum['ctr'].'</td></tr>';
?>
</tbody>
</table>

<h3>���������� �� ������� (<?php echo date("Y-m-d"); ?>)</h3>

<?php

$str = $db->prepare("SELECT sc.hour,SUM(sc.impressions) as imp,SUM(sc.clicks) as clk
                     FROM stat_counts sc
                     INNER JOIN banner b ON sc.id_banner=b.id
                     WHERE b.id_campaign=:id_campaign AND sc.date=CURDATE()
                     GROUP BY sc.hour");
$str->bindParam(":id_campaign",$id_campaign);
$str->execute();

?>
<table cellspacing="0">
    <thead>
    <th>����</th><th>������</th><th>�����</th><th>CTR</th>
    </thead>
    <tbody>

    <?php

    $rows = $str->fetchAll(PDO::FETCH_ASSOC);

    $sum['clk'] = $sum['imp'] = 0;

    foreach($rows as $v)
    {
        $key = $v['hour'];
        $hours[$key]['imp'] = $v['imp'];
        $hours[$key]['clk'] = $v['clk'];
        $hours[$key]['ctr'] = round((($v['clk'] / $v['imp']) * 100),2);
        $sum['clk'] += $v['clk'];
        $sum['imp'] += $v['imp'];
    }

    $sum['ctr'] = round((($sum['clk'] / $sum['imp']) * 100),2);

    for($i=0;$i<24;$i++)
    {
        $clicks = isset($hours[$i]['clk']) ? $hours[$i]['clk'] : 0;
        $impressions = isset($hours[$i]['imp']) ? $hours[$i]['imp'] : 0;
        $ctr = isset($hours[$i]['ctr']) ? $hours[$i]['ctr'] : 0;

        if($i <= date("H"))
           echo "<tr><td>".$i.":00 - ".$i.":59</td><td>".$impressions."</td><td>".$clicks."</td><td>".$ctr."</td></tr>";
    }

    ?>
    <tr>
        <td><i>�����</i></td><td><?= $sum['imp'] ?></td><td><?= $sum['clk'] ?></td><td><?= $sum['ctr'] ?></td>
    </tr>
    </tbody>
</table>

</body>

</html>
