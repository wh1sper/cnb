<?php

include('conf.php');

$id_zone = $_GET['zone'];

$sites = unserialize(base64_decode(file_get_contents("cache/sites.txt")));

if(!isset($_SERVER['HTTP_REFERER']))
{
  //  die("������ ������");
}
else $referer = $_SERVER['HTTP_REFERER'];

$ok = 0;
foreach($sites as $site)
{
    $url = str_replace(array("http://","www."),"",$site['url']);
    if(strpos($referer,"http://".$url) !== false) $ok = 1;
    if(strpos($referer,".".$url) !== false) $ok = 1;
}

//if(!$ok) die("������ ������");
if(!is_numeric($id_zone)) die("����������� ������� �� �������� ������");

$db = new PDO($db['connect_string'],$db['user'],$db['password']);

// ��� �������� ������ ���� ���� �� ���� ��� �������. ����� ������ ����� ������
if(!isset($_GET['banner']))
{
    $str = $db->prepare("SELECT id_banner FROM banner_zone WHERE id_zone=:id_zone");
    $str->bindParam(":id_zone",$id_zone);
    $str->execute();
    $banner = $str->fetch(PDO::FETCH_ASSOC);
    if(sizeof($banner)) $id_banner = $banner['id_banner'];
}
else
{
    if(!is_numeric($_GET['banner'])) die("����������� ������� �� �������� ������");
    $id_banner = $_GET['banner'];
}

// ����� ������

$str = $db->prepare("SELECT link FROM banner WHERE id=:id_banner");
$str->bindParam("id_banner",$id_banner);
$str->execute();
$banner = $str->fetch(PDO::FETCH_ASSOC);

$url_to_follow = isset($_GET['goto']) ? $_GET['goto'] : $banner['link'];

// ����� ����
$str = $db->prepare("SELECT id FROM stat_counts WHERE  date=CURDATE() AND hour=:hour AND id_banner=:id_banner AND id_zone=:id_zone");
$str->bindParam(":id_zone",$id_zone);
$str->bindParam(":id_banner",$id_banner);
$str->bindParam(":hour",date("H"));
$str->execute();
$stat = $str->fetch(PDO::FETCH_ASSOC);
if(sizeof($stat)) $id_stat = $stat['id'];

if(isset($id_stat))
    $str = $db->prepare("UPDATE stat_counts SET clicks=clicks+1 WHERE date=CURDATE() AND hour=:hour AND id_banner=:id_banner AND id_zone=:id_zone");
else
    $str = $db->prepare("INSERT INTO stat_counts SET clicks=1,date=CURDATE(),hour=:hour,id_banner=:id_banner,id_zone=:id_zone");

$str->bindParam(":id_banner",$id_banner);
$str->bindParam(":id_zone",$id_zone);
$str->bindParam(":hour",date("H"));
$str->execute();

// ����� ����
$str = $db->prepare("INSERT INTO stat_click SET date=CURDATE(),url=:url,hour=:hour,id_banner=:id_banner,id_zone=:id_zone");
$str->bindParam(":id_banner",$id_banner);
$str->bindParam(":id_zone",$id_zone);
$str->bindParam(":url",$url_to_follow);
$hour = date("H");
$str->bindParam(":hour",$hour);
$str->execute();

Header("Location: ".$url_to_follow);

?>