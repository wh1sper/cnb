<?php
/**
 * ������� ��������� �� ������
 * �������������� ������ ����:
 * "���� � �������"/redirect.php?[some_key_word+]http://new_location
 *
 * ������������� ������ ����: "���� � �������"/redirect.php?[key=some_key_word&]url=new_location
 * �������� ��� ������������
 *
 */

/**
 * ��������� ����� �� ����������� ������ ������
 *
 * @link http://www.php.net/preg_filter
 *
 * @param string $url �����
 * @param array $whitelist ������ ���������� ��������� (����� ������)
 * @return bool return TRUE if valid
 */
function url_compare_whitelist($url, $whitelist)
{
    $result = FALSE;

//    if (version_compare(PHP_VERSION, '5.3.0') >= 0)
//    {
//        if (preg_filter($whitelist, '$0', $url))
//        {
//            $result = TRUE;
//        }
//    } else
//    {
    foreach ($whitelist as $key => $regexp)
    {
        if (filter_var($url, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => $regexp, "default" => FALSE))))
        {
            $result = TRUE;
            break;
        }
    }
//    }
    return $result;
}

/**
 * �������� ip �����
 * @return str
 */
function get_remote_ip($ip = NULL)
{
    if (empty($ip))
    {
        if (getenv('REMOTE_ADDR'))
        {
            $ip = getenv('REMOTE_ADDR');
        }
        elseif (getenv('HTTP_CLIENT_IP'))
        {
            $ip = getenv('HTTP_CLIENT_IP');
        }
        elseif (getenv('HTTP_X_FORWARDED_FOR'))
        {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        }
        elseif (getenv('HTTP_X_FORWARDED'))
        {
            $ip = getenv('HTTP_X_FORWARDED');
        }
        elseif (getenv('HTTP_FORWARDED_FOR'))
        {
            $ip = getenv('HTTP_FORWARDED_FOR');
        }
        elseif (getenv('HTTP_FORWARDED'))
        {
            $ip = getenv('HTTP_FORWARDED');
        }
    }

    /**
     * @todo ������ $ip ������� ��������� ip ����� �������
     */
    $ip = explode(',', $ip);

    foreach ($ip as $key => $value)
    {
        $value = trim($value);
        if ($value = filter_var($value, FILTER_VALIDATE_IP))
        {
            /* ���  IPv6 �������� � ����������� ���� */
            $ip[$key] = inet_ntop(inet_pton($value));
        }
        else
        {
            unset($ip[$key]);
        }
    }

    $ip = implode(',', $ip);

    return $ip;
}

/**
 * �������� ���������� ID ����������
 */
function get_user_suid()
{
    $cookie = (!empty($_COOKIE['_suid'])) ? $_COOKIE['_suid'] : FALSE;

    if (!$cookie)
    {
        srand((double) microtime() * 1000000);
        $suid = uniqid(rand());
    }
    else
    {
        $suid = $cookie;
    }

    if (!$cookie || empty($_COOKIE['_suid_renew']))
    {
        /**
         * ��� ������������� cookie ��� ����������� �� ������, �� �������� ��� �������� ����, �������� URL � ������ ������� � �����, �������� ���: .your-domain.com
         */
        setcookie("_suid", $suid, time() + 60 * 60 * 24 * 30 * 12, "/", '.cnews.ru');
        setcookie("_suid_renew", "1", time() + 60 * 60 * 24 * 30, "/", '.cnews.ru');
    }
    return $suid;
}

/**
 * @var array ������ ������ ��� ���������� � ����������
 */
$new_row = array(
    ':redirect_referer' => (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '',
    ':redirect_location' => '',
    ':redirect_visitor_suid' => '', // �������� ���� �� �����
    ':redirect_visitor_useragent' => (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '',
    ':redirect_visitor_ip' => get_remote_ip(),
    ':redirect_blocked' => 1,
    ':redirect_keyword' => ''
);

$query = (isset($_SERVER["QUERY_STRING"])) ? $_SERVER["QUERY_STRING"] : '';
preg_match("#^(?P<key>[a-z1-9_]*)\+(?P<url>(http|https)://.*)$#i", $query, $matches);

if ($matches)
{
    $redirect = $matches["url"];
    $new_row[':redirect_keyword'] = $matches["key"];
}
else
{
    $redirect = $_SERVER["QUERY_STRING"];
}

$parsed_url = parse_url($redirect);

$new_row[':redirect_location'] = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
$new_row[':redirect_location'] .= isset($parsed_url['user']) ? $parsed_url['user'] : '';
$new_row[':redirect_location'] .= isset($parsed_url['pass']) ? ':' . $parsed_url['pass'] : '';
$new_row[':redirect_location'] .= (!empty($parsed_url['user']) || !empty($parsed_url['pass'])) ? '@' : '';
$new_row[':redirect_location'] .= isset($parsed_url['host']) ? $parsed_url['host'] : '';
$new_row[':redirect_location'] .= isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
$new_row[':redirect_location'] .= isset($parsed_url['path']) ? '/' . trim($parsed_url['path'], '/') : '/';
$new_row[':redirect_location'] .= isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
$new_row[':redirect_location'] .= isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';


$parsed_url = parse_url($new_row[':redirect_referer']);

$new_row[':redirect_referer'] = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
$new_row[':redirect_referer'] .= isset($parsed_url['user']) ? $parsed_url['user'] : '';
$new_row[':redirect_referer'] .= isset($parsed_url['pass']) ? ':' . $parsed_url['pass'] : '';
$new_row[':redirect_referer'] .= (!empty($parsed_url['user']) || !empty($parsed_url['pass'])) ? '@' : '';
$new_row[':redirect_referer'] .= isset($parsed_url['host']) ? $parsed_url['host'] : '';
$new_row[':redirect_referer'] .= isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
$new_row[':redirect_referer'] .= isset($parsed_url['path']) ? '/' . trim($parsed_url['path'], '/') : '/';
$new_row[':redirect_referer'] .= isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
$new_row[':redirect_referer'] .= isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';

if ($new_row[':redirect_location'])
{
    define('BASEPATH', TRUE);
    require_once dirname(__FILE__) . '/app/config/database.php';

    $dsn = 'mysql:host=' . $db['default']['hostname'] . ';dbname=' . $db['default']['database'];

    try
    {
        $connect = new PDO($dsn, $db['default']['username'], $db['default']['password']);

        /* ��������� ������������ ��������� ������������� �� ����� �� �������� */
        $sql = 'SELECT whitelist_pattern FROM statistics_redirects_whitelist WHERE whitelist_status=1';
        if ($whitelist = $connect->query($sql)->fetchAll(PDO::FETCH_COLUMN, 0))
        {
            $new_row[':redirect_blocked'] = (int) !url_compare_whitelist($new_row[':redirect_referer'], $whitelist);
        }

        /* ��������� ��� ����������, ��������� �����(������) � ���������� �� ���� */
        $sql = 'SELECT COUNT(*) FROM statistics_useragent WHERE useragent_string = ' . $connect->quote($new_row[':redirect_visitor_useragent']) . ' AND useragent_type = "robot"';
        if ($res = $connect->query($sql))
        {
            if ($res->fetchColumn() > 0)
            {
                $new_row = array();
            }
            else
            {
                $new_row[':redirect_visitor_suid'] = get_user_suid();
            }
            $res->closeCursor();
        }

        if ($new_row)
        {
            $insert = $connect->prepare('INSERT INTO statistics_redirects (redirect_date, redirect_time, redirect_referer, redirect_location, redirect_visitor_suid, redirect_visitor_useragent, redirect_visitor_ip, redirect_blocked, redirect_keyword) values (CURDATE(), CURTIME(), :redirect_referer, :redirect_location, :redirect_visitor_suid, :redirect_visitor_useragent, :redirect_visitor_ip, :redirect_blocked, :redirect_keyword)');
            $insert->execute($new_row);
        }
    } catch (PDOException $e)
    {
        // echo 'Connection failed: ' . $e->getMessage();
        $new_row[':redirect_blocked'] = 0;
    }


    if (!$new_row[':redirect_blocked'])
    {
        header('Location: ' . $redirect, TRUE, 302);
        exit();
    }
}
?>

Follow link is not allowed!
