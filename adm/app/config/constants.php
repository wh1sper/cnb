<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/

define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 					'ab');
define('FOPEN_READ_WRITE_CREATE', 				'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 			'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define('ROOT', dirname(__FILE__) . '/../../');
define("ROOT_TMP", ROOT . 'files/proxy_tmp/');
define("REMOTE_FILEARCHIVE_DOMAIN", "http://filearchive.cnews.ru/img/");
define("REMOTE_GALLERY_DOMAIN", "http://filearchive.cnews.ru/galleries/");
define("STATISTIC_REDIRECT_URL", "http://club.cnews.ru/redirect.php");
define("FILEARCHIVE_DOMAIN", "http://filearchive.cnews.ru/");

if(isset($_SERVER['HTTP_HOST']))
{
    $_SERVER['SERVER_NAME'] = $_SERVER["HTTP_HOST"];
    define('DOMAIN', 'http://' . $_SERVER['SERVER_NAME']);
}

define("ADMIN_LOGIN", "cnews");
define("ADMIN_PASSWORD", "ckj;ysqgfhjkm");
define("CSS_ROOT", ROOT . "/adm/inc/css/");
define("JS_ROOT", ROOT . "/adm/inc/js/");

define('DATETIME_MYSQL', date('Y-m-d H:i:s'));

define("TEMPLATE_VERSION", "2013-11-21 1");

/* End of file constants.php */
/* Location: ./system/application/config/constants.php */