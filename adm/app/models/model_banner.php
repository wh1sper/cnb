<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:19
 */

class Model_Banner extends Model_Common
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_zone");
    }

    // ������ ������, ���������� ��� � ��������
    public function select_full_list($data,$limit=null)
    {
        $this->load->model("model_stat");
        $this->load->model("model_channel");

        $this->db->select('b.status,b.id,b.name as bannername,b.type,b.link,b.date_add,b.id_campaign,c.name,cl.name as clientname');
        $this->db->from('banner b');
        $this->db->join('campaign c','b.id_campaign=c.id','left');
        $this->db->join('client cl','c.id_client=cl.id');
        if(isset($data['client'])) $this->db->where('c.id_client',$data['client']);
        if(isset($data['campaign'])) $this->db->where('b.id_campaign',$data['campaign']);
        if(isset($data['name'])) $this->db->where('b.name LIKE "%'.$data['name'].'%"');
        $this->db->group_by('b.id');
        if(!is_null($limit)) $this->db->limit(10,$limit);
        $this->db->order_by('b.status desc,b.id desc');
        $list = $this->db->get()->result_array();
        foreach($list as &$v)
        {
            $zones = $this->db->select('z.id,z.name')->from('banner_zone bz')->join('zone z','z.id=bz.id_zone')->where('bz.id_banner',$v['id']);
            $v['stat']['clicks'] = $v['stat']['impressions'] = 0;
            if(sizeof($zones))
            {
                $v['zones'] = $this->db->get()->result_array();
                foreach ($v['zones'] as &$zone)
                {
                    $zone['stat'] = $this->model_stat->get_stat($zone['id'],$v['id']);
                    $v['stat']['impressions'] += $zone['stat']['impressions'];
                    $v['stat']['clicks'] += $zone['stat']['clicks'];
                }
            }

            if($v['stat']['impressions'])
            {
                $v['stat']['ctr'] = round((($v['stat']['clicks'] / $v['stat']['impressions']) * 100),2);
            }
            else $v['stat']['ctr'] = 0;


            $channel = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("c.share",0)->where('co.id_object',$v['id'])->where('co.object_type','banner')->get()->result_array();
            if(sizeof($channel))
            {
                $v['settings'] = $this->model_channel->select_settings_by_channel($channel[0]['id_channel']);
            }

            $channels = $this->db->select('c.id,c.name')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("co.object_type","banner")->where("co.id_object",$v['id'])->where('c.share',1)->get()->result_array();
            if(sizeof($channels))
                $v['channels'] = $channels;

        }

        return $list;
    }


    public function select_by_page($page,$data)
    {
        $this->db->select("COUNT(b.id) as cnt");
        $this->db->from('banner b');
        $this->db->join('campaign c','b.id_campaign=c.id','left');
        $this->db->join('client cl','c.id_client=cl.id','left');
        if(isset($data['client'])) $this->db->where('c.id_client',$data['client']);
        if(isset($data['campaign'])) $this->db->where('b.id_campaign',$data['campaign']);
        if(isset($data['name'])) $this->db->where('b.name LIKE "%'.$data['name'].'%"');

        $count = $this->db->get()->result_array();
        $count = $count[0]['cnt'];

        $page = $page - 1;

        $limit = $page * 10;

        $ret['count'] = $count;
        $ret['data'] = $this->select_full_list($data,$limit);

        return $ret;
    }

    // ��������� ������
    public function select_full_single($id)
    {
        $this->db->select('b.*,c.weight as campaignweight,c.name as campaignname,cn.name as clientname,c.id as campaignid,cn.id as clientid');
        $this->db->from('banner b');
        $this->db->join('campaign c','b.id_campaign=c.id');
        $this->db->join('client cn','cn.id=c.id_client');
        $this->db->where('b.id',$id);
        $res = $this->db->get()->result_array();
        $banner = $res[0];

        $channel = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("c.share",0)->where('co.id_object',$id)->where('co.object_type','banner')->get()->result_array();
        if(sizeof($channel))
        {
            $this->load->model("model_channel");
            $banner['settings'] = $this->model_channel->select_settings_by_channel($channel[0]['id_channel']);
        }

        $channels = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("co.object_type","banner")->where("co.id_object",$id)->where('c.share',1)->get()->result_array();
        if(sizeof($channels))
            $banner['channels'] = $channels;


        $zones = $this->db->select('z.id,z.name')->from('banner_zone bz')->join('zone z','z.id=bz.id_zone')->where('bz.id_banner',$id);
        if(sizeof($zones))
            $banner['zones'] = $this->db->get()->result_array();

        return $banner;
    }


    // ��� ������� - ������� ���� �������� �� ����, �������� �� �������� � ��������
    // @param zone = id ����
    // @param type = regular/exclusive ��� ��������

    public function select_by_zone($zone,$type)
    {
        $this->db->select('b.*,s.url,bz.chance');
        $this->db->from('banner_zone bz');
        $this->db->join('banner b','b.id=bz.id_banner','inner');
        $this->db->join('campaign c','b.id_campaign=c.id','inner');
        $this->db->join('zone z','z.id=bz.id_zone','inner');
        $this->db->join('site s','z.id_site=s.id', 'inner');
        $this->db->where('c.type',$type);
        $this->db->where('b.status',1);
        // ��������-������ �� ����������
       // $this->db->where('b.type !=',"redirect");
        $this->db->where('bz.id_zone',$zone);
        // ��������� �������
        $banners = array();
        $res = $this->db->get()->result_array();

        $this->load->model("model_channel");
        $this->load->model("model_stat");

        if(sizeof($res))
        {
            $banners = $res;

            foreach($banners as &$banner)
            {
                // ���� ��������
                $campaign_dates = $this->db->select('date_start,date_end')->from('campaign')->where('id',$banner['id_campaign'])->get()->result_array();
                $banner['campaign']['dates'] = $campaign_dates[0];

                // ������ ��������
                $channels = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("co.object_type","campaign")->where("co.id_object",$banner['id_campaign'])->get()->result_array();

                if(sizeof($channels))
                {
                    foreach($channels as $k=>$ch)
                    {
                        $settings = $this->model_channel->select_settings_by_channel($ch['id_channel']);
                        $banner['campaign']['channels'][$k] = $settings;
                    }

                }
                // ����� �������
                $banner['stat'] = $this->model_stat->get_stat($zone,$banner['id']);
                $banner['campaign_impressions'] = $this->model_stat->get_campaign_impressions($banner['id_campaign']);
                $banner['total_impressions'] = $this->model_stat->get_banner_impressions($banner['id']);

                // ������ �������

                $channels = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("co.object_type","banner")->where("co.id_object",$banner['id'])->get()->result_array();

                if(sizeof($channels))
                {
                    foreach($channels as $k=>$ch)
                    {
                        $settings = $this->model_channel->select_settings_by_channel($ch['id_channel']);
                        $banner['channels'][$k] = $settings;
                    }

                }

            }
        }



        return $banners;

    }


    public function set_zero_weight($id_banner,$id_zone)
    {
        $ret = 0;
        $data['chance'] = 0;
        if($this->model_common->update("banner_zone",$data,array("id_banner"=>$id_banner,"id_zone"=>$id_zone))) $ret = 1;
        return $ret;
    }

    // ������� ����� �� �����
    // chance = (��������� ������� * ��������� �������� / �����(��������� ������� * ��������� �������� ��� ���� �������� � ����)
    public function calculate_weight($id_banner,$id_zone)
    {

        $banner = $this->select_full_single($id_banner);
        $this->load->model("model_zone");

        $first = $banner['weight'] * $banner['campaignweight'];

        $zone = $this->model_zone->select_with_banners($id_zone,1);
        $second = 0;

        $zone = $zone[0];

        if(isset($zone['banners']))
        {
            foreach($zone['banners'] as $v)
            {
                if($v['status'])
                {
                    $weight = $v['weight'] * $v['campaignweight'];
                    $second += $weight;
                }
            }
        }

        $chance = ($first / $second ) * 100;

        $ret = 0;
        $data['chance'] = $chance;
        if($banner['status'])
        {
            if($this->model_common->update("banner_zone",$data,array("id_banner"=>$id_banner,"id_zone"=>$id_zone))) $ret = 1;
        }
        return $ret;

    }


    // @param type - 1/0 ����������/�������� ��������
    // �������� �������� ������� � ����
    public function setzone($type,$zone,$banner)
    {
        $data['id_banner'] = $banner;
        $data['id_zone'] = $zone;

        $ret = 0;

        if($type == 1)
        {
            // ��������, ��� �� ����� �������� ���
            $check = $this->db->select('id')->from('banner_zone')->where('id_zone',$zone)->where('id_banner',$banner)->get()->result_array();
            if(!sizeof($check))
                if($this->model_common->insert("banner_zone",$data)) $ret = 1;
        }
        if(!$type)
        {
            if($this->model_common->delete("banner_zone",array("id_banner"=>$banner,"id_zone"=>$zone))) $ret = 1;
        }

        // ������� ����� ���� �������� � ���� ����
        $banners = $this->db->select('id_banner')->from('banner_zone')->where('id_zone',$zone)->get()->result_array();

        foreach($banners as $v)
        {
            $this->calculate_weight($v['id_banner'],$zone);
        }

        return $ret;
    }

    public function delete($id)
    {
        // �������� ������� �������� �������� ��� �������� � �����, ��� �������� � ��������, id_banner=0 � �����
        $this->db->where('id',$id);
        $this->db->delete("banner");

        $this->db->where('id_banner',$id);
        $this->db->delete("banner_zone");

        $this->db->where('id_object',$id);
        $this->db->where('object_type','banner');
        $this->db->delete("channel_object");

        // � ����� �������� �� 0 ����� �� ���� ��������, ���� ����� �������� ���������
        $data['id_banner'] = 0;
        $this->update("stat_click",$data, array("id_banner"=>$id));
        $this->update("stat_counts",$data, array("id_banner"=>$id));

    }


}