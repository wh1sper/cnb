<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:19
 */

class Model_Zone extends Model_Common
{
    public function __construct()
    {
        parent::__construct();
    }

    // ������ ������, ���������� ��� � ��������
    public function select_by_site($id_site)
    {
        return $this->db->select('*')->from('zone z')->where("id_site",$id_site)->get()->result_array();
        //     $this->db->join('zone z','z.id_site=s.id','left');
        //$this->db->select('s.*,count(z.id) as cnt_zones');
        //$this->db->group_by('s.id')
    }

    // ��� ���� � ������������ ���������
    public function select_with_banners($id = 0,$only_active=0)
    {
        $this->db->select('z.*,s.name as sitename,s.id as siteid')->from('zone z')->join('site s','s.id=z.id_site');
        if($id) $this->db->where('z.id',$id);
        $zones = $this->db->get()->result_array();

        $this->load->model("model_banner");
        $this->load->model("model_stat");

        foreach($zones as &$zone)
        {
            $this->db->select('bz.chance,b.id');
            $this->db->from('banner b');
            $this->db->join('banner_zone bz','bz.id_banner=b.id');
            $this->db->join('campaign c','c.id=b.id_campaign');
            $this->db->join('client cn','cn.id=c.id_client');
            $this->db->where('bz.id_zone',$zone['id']);
            if($only_active)
                $this->db->where('b.status',1);
            $this->db->order_by("b.status",'desc');
            $banners = $this->db->get()->result_array();

            foreach($banners as $b)
            {
                $banner = $this->model_banner->select_full_single($b['id']);
                $banner['chance'] = $b['chance'];
                $banner['stat'] = $this->model_stat->get_stat($zone['id'],$banner['id']);
                $zone['banners'][] = $banner;
            }

        }
        return $zones;
    }

    public function delete($id)
    {
        // ������� �����
        $this->db->where("id_zone",$id);
        $this->db->delete('banner_zone');

        // � ����� �������� �� 0 ����� �� ���� �����, ���� ����� �������� ���������
        $data['id_zone'] = 0;
        $this->update("stat_click",$data, array("id_zone"=>$id));
        $this->update("stat_counts",$data, array("id_zone"=>$id));

        $this->db->where('id',$id);
        $this->db->delete("zone");
    }

}