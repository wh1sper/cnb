<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 05.11.14
 * Time: 12:47
 */

class Model_Stat extends Model_Common
{

    public $total_clicks;
    public $total_imp;

    public function __construct()
    {
        parent::__construct();
    }


    // ���������/��������� ����� �� �������/������ �� ���������� ����
    // @param type = clicks/impressions
    public function add_stat($id_zone,$id_banner,$type,$date = null,$hour = null)
    {
        $data['date'] = is_null($date) ? date("Y-m-d") : $date;
        $data['hour'] = is_null($hour) ? date("H") : $hour;
        $data['id_banner'] = $id_banner;
        $data['id_zone'] = $id_zone;

        $this->db->select("id");
        $this->db->from("stat_counts");
        $this->db->where("date",$data['date']);
        $this->db->where("hour",$data['hour']);
        $this->db->where("id_zone",$id_zone);
        $this->db->where("id_banner",$id_banner);
        $res = $this->db->get()->result_array();

        if(sizeof($res))
        {
            $id = $res[0]['id'];
        }
        else
        {
            $id = $this->model_common->insert("stat_counts",$data);
        }

        $this->db->query("UPDATE stat_counts SET $type=$type + 1 WHERE id='".$id."'");

    }

    // ��������� ����
    public function add_click($id_zone,$id_banner,$url)
    {
        $data['id_zone'] = $id_zone;
        $data['id_banner'] = $id_banner;
        $data['url'] = $url;
        $data['date'] = date("Y-m-d");
        $data['hour'] = date("H");

        $this->model_common->insert("stat_click",$data);
    }

    public function get_stat_by_interval($filters)
    {

        $is_zero = 0;
        if(isset($filters['zone']))
        {
            // zeropixel cost'ill
            $this->load->model('model_zone');

            $zone = $this->model_zone->select_with_banners($filters['zone']);
            $zone = $zone[0];

            foreach($zone['banners'] as $zb)
            {
                if($zb['type']=='redirect') $is_zero = 1;
            }

            $this->db->where("sc.id_zone",$filters['zone']);
        }

        $this->db->select("SUM(sc.impressions) as imp,SUM(sc.clicks) as clk,sc.date");
        $this->db->from("stat_counts sc");

        if(!$is_zero)
        {
            $this->db->join("banner b","sc.id_banner=b.id","inner");
            $this->db->join("campaign c","b.id_campaign=c.id","inner");
            $this->db->join("client cl","c.id_client=cl.id","inner");
        }

        $this->db->join("zone z","sc.id_zone=z.id","inner");
        $this->db->join("site s","z.id_site=s.id","inner");

        if(isset($filters['campaign'])) $this->db->where("c.id",$filters['campaign']);
        if(isset($filters['banner'])) $this->db->where("b.id",$filters['banner']);
        if(isset($filters['site'])) $this->db->where("s.id",$filters['site']);
        if(isset($filters['client'])) $this->db->where("cl.id",$filters['client']);

        $this->db->where("date >= '".$filters['date_from']."'");
        $this->db->where("date <= '".$filters['date_to']."'");

        $this->db->group_by("sc.date");
        $this->db->order_by("sc.date","desc");
        $dates = $this->db->get()->result_array();

        $this->total_clicks = 0;
        $this->total_imp = 0;

        if(sizeof($dates))
        {
            foreach($dates as &$v)
            {
                $v['ctr'] = round((($v['clk'] / $v['imp']) * 100),2);
                // �� ��������� 7 ���� ���� ���������� ������, ������� ������� ��� ��������� ������, ���� ����� ����
                $this->total_clicks += $v['clk'];
                $this->total_imp += $v['imp'];
            }
        }

        else $dates = array();

        return $dates;
    }

    // ��������� ����� �� ������ �� ���
    public function get_stat_by_date($date,$filters)
    {
        $this->db->select("SUM(sc.impressions) as imp,SUM(sc.clicks) as clk,hour");
        $this->db->from("stat_counts sc");
        $this->db->join("banner b","sc.id_banner=b.id","inner");
        $this->db->join("campaign c","b.id_campaign=c.id","inner");
        $this->db->join("client cl","c.id_client=cl.id","inner");
        $this->db->join("zone z","sc.id_zone=z.id","inner");
        $this->db->join("site s","z.id_site=s.id","inner");

        if(isset($filters['zone'])) $this->db->where("sc.id_zone",$filters['zone']);
        if(isset($filters['campaign'])) $this->db->where("c.id",$filters['campaign']);
        if(isset($filters['banner'])) $this->db->where("b.id",$filters['banner']);
        if(isset($filters['site'])) $this->db->where("s.id",$filters['site']);
        if(isset($filters['client'])) $this->db->where("cl.id",$filters['client']);

        $this->db->where("sc.date",$date);
        $this->db->where("sc.id_zone !=",0);
        $this->db->where("sc.id_banner !=",0);
        $this->db->order_by("sc.hour");
        $this->db->group_by("sc.hour");
        $stat = $this->db->get()->result_array();

        $ret = array();

        if(sizeof($stat))
        {
            foreach($stat as $v){
                $key = $v['hour'];
                $ret[$key]['imp'] = $v['imp'];
                $ret[$key]['clk'] = $v['clk'];
                $ret[$key]['ctr'] = round((($v['clk'] / $v['imp']) * 100),2);
            }
        }

        return $ret;
    }

    // ���� ��� ������ ��� �������� �� ������ ���������� ����������
    public function get_campaign_impressions($id_campaign)
    {
        $this->load->model("model_campaign");
        $campaign = $this->model_campaign->select_full_single($id_campaign);
        if(!sizeof($campaign['banners'])) return 0;

        foreach($campaign['banners'] as $banner)
        {
            $in[] = $banner['id_banner'];
        }

        $in_str = join($in,',');
        $res = $this->db->query("SELECT SUM(impressions) as impr FROM stat_counts WHERE id_banner IN (".$in_str.")")->result_array();
        return $res[0]['impr'];
        //

    }

    public function get_banner_impressions($id_banner)
    {
        $this->db->select('SUM(impressions) as impr')->from('stat_counts');
        $this->db->where('id_banner',$id_banner);
        $res = $this->db->get()->result_array();
        if(!isset($res[0])) return 0;
        return $res[0]['impr'];
    }


    public function get_stat($id_zone=null,$id_banner=null,$date=null)
    {
        $this->db->select('impressions,clicks')->from('stat_counts');
        if(!is_null($id_zone)) $this->db->where('id_zone',$id_zone);
        if(!is_null($id_banner)) $this->db->where('id_banner',$id_banner);
        if(is_null($date)) $date = date("Y-m-d");
        $this->db->where('date',$date);
        $stat = $this->db->get()->result_array();

        if(sizeof($stat))
        {
            $ret = array("impressions"=>0,"clicks"=>0,"ctr"=>0);
            foreach($stat as $v)
            {
                $ret['impressions'] += $v['impressions'];
                $ret['clicks'] += $v['clicks'];
            }
            $ret['ctr'] =  round((($ret['clicks'] / $ret['impressions']) * 100),2);
        }
        else $ret = 0;
        return $ret;
    }

    public function get_stat_by_campaign($id_campaign)
    {
        $banners = $this->db->select("id")->from('banner')->where('id_campaign',$id_campaign)->get()->result_array();

        if(sizeof($banners))
        {
            $data = array("impressions"=>0,"clicks"=>0,"ctr"=>0);

            foreach($banners as $v)
            {
                $stat = $this->get_stat(null,$v['id']);
                $data['impressions'] += $stat['impressions'];
                $data['clicks'] += $stat['clicks'];
                $data['ctr'] += $stat['ctr'];
            }
        }

        if(isset($data)) $ret = $data;
        else $ret = 0;

        return $ret;
    }


}

?>