<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:19
 */

class Model_Site extends Model_Common
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("model_zone");
    }

    // ������ ������, ���������� ��� � ��������, ����� ���� ��, ��� ������ �������� �� dashboard (���� �������� �������� "�����")
    public function select_full_list($active = 0)
    {

        $this->load->model("model_stat");

        $this->db->select('s.name,s.url,s.id,count(z.id) as cnt_zones');
        $this->db->from('site s');
        $this->db->join('zone z','z.id_site=s.id','left');
        $this->db->group_by('s.id');
        $this->db->order_by('s.id','desc');
        $sites = $this->db->get()->result_array();
        // ����� ����
        foreach($sites as &$v)
        {
            $zones = $this->db->select('z.id,z.name')->from('zone z')->where('z.id_site',$v['id'])->get()->result_array();

            if(sizeof($zones))
            {
                $v['bannercount']['total'] = $v['bannercount']['active'] = $v['stat']['clicks'] = $v['stat']['impressions'] = 0;
                // ����� ���������� �������� � ������ ����, ���������� ������� ������ �� ����
                foreach($zones as $z=>&$zone)
                {
                    $zone['counts'] = $this->db->query("SELECT (SELECT COUNT(b.id) FROM banner b LEFT JOIN banner_zone bz ON bz.id_banner=b.id WHERE b.status=1 AND bz.id_zone='".$zone['id']."') AS oncount, (SELECT COUNT(b.id) FROM banner b LEFT JOIN banner_zone bz ON bz.id_banner=b.id WHERE bz.id_zone='".$zone['id']."') AS totalcount")->result_array();

                    $v['bannercount']['active'] += $zone['counts'][0]['oncount'];
                    $v['bannercount']['total'] += $zone['counts'][0]['totalcount'];
                    $zone['stat'] = $this->model_stat->get_stat($zone['id']);
                    $v['stat']['impressions'] += $zone['stat']['impressions'];
                    $v['stat']['clicks'] += $zone['stat']['clicks'];
                    // ���� �������� $active = 1 - ���������� ������ ����, ��� �������� ������� �������
                    if($active)
                    {
                        if(!sizeof($zone['counts'][0]['oncount']) || !$zone['stat']['impressions']) unset($zones[$z]);
                    }
                }

                if($v['stat']['impressions'])
                {
                    $v['stat']['ctr'] = round((($v['stat']['clicks'] / $v['stat']['impressions']) * 100),2);
                }
                else  $v['stat']['ctr'] = 0;




                $v['zones'] = $zones;
            }



        }

        return $sites;
    }

    // ������ ������ � ������
    public function select_with_zones()
    {
        $sites = $this->db->select('s.url,s.name,s.id')->from('site s')->order_by("id")->get()->result_array();
        foreach($sites as &$v)
        {
            $zones = $this->db->select('z.id,z.name')->from('zone z')->where('z.id_site',$v['id'])->get()->result_array();
            if(sizeof($zones)) $v['zones'] = $zones;
            else unset($v);
        }
        return $sites;
    }

    public function delete($id)
    {
        // �������� ����� �������� �������� ��� ��� � ������ �������� � ����� ������, �������� ���� ������ � ����������
        $this->db->where('id',$id);
        $this->db->delete("site");

        $this->db->where('id_site',$id);
        $this->db->select('*');
        $zones = $this->db->get("zone")->result_array();

        foreach($zones as $zone)
        {
            $this->model_zone->delete($zone['id']);
        }

    }


}