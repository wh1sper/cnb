<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:19
 */

class Model_Campaign extends Model_Common
{
    public function __construct()
    {
        parent::__construct();
    }

    // ������ ��������
    public function select_full_list($data,$limit=null)
    {
        $this->load->model("model_stat");
        $this->load->model("model_channel");

        $this->db->select('c.*,cn.name as clientname, count(b.id) as cnt_banners');
        $this->db->from('campaign c');
        $this->db->join('client cn','c.id_client=cn.id');
        $this->db->join('banner b','b.id_campaign=c.id','left');
        if(isset($data['client'])) $this->db->where('c.id_client',$data['client']);
        if(isset($data['name'])) $this->db->where('c.name LIKE "%'.$data['name'].'%"');
        $this->db->group_by('c.id');
        $this->db->order_by("c.id desc");
        if(!is_null($limit)) $this->db->limit(10,$limit);
        $list = $this->db->get()->result_array();

        foreach($list as &$v)
        {
            $v['stat'] = $this->model_stat->get_stat_by_campaign($v['id']);

            $channel = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("c.share",0)->where('co.id_object',$v['id'])->where('co.object_type','campaign')->get()->result_array();
            if(sizeof($channel))
            {
                $v['settings'] = $this->model_channel->select_settings_by_channel($channel[0]['id_channel']);
            }

            $channels = $this->db->select('c.id,c.name')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("co.object_type","campaign")->where("co.id_object",$v['id'])->where('c.share',1)->get()->result_array();
            if(sizeof($channels))
                $v['channels'] = $channels;


        }

        return $list;
    }

    // ������ �� ��������, � ������� ���� ����������� �������
    public function select_filled($where = 0)
    {
        $this->db->select('c.name,c.id,count(b.id) as bannercount');
        $this->db->from('campaign c');
        $this->db->join('banner b',"b.id_campaign=c.id","left");
        if($where) $this->db->where($where);
        $this->db->group_by("c.id");
        $this->db->having("bannercount > 0");
        $this->db->order_by("c.id");
        return $this->db->get()->result_array();
    }


    public function select_by_page($page,$data)
    {
        $this->db->select("COUNT(c.id) as cnt");
        $this->db->from('campaign c');
        $this->db->join('client cn','c.id_client=cn.id');

        if(isset($data['client'])) $this->db->where('c.id_client',$data['client']);
        if(isset($data['name'])) $this->db->where('c.name LIKE "%'.$data['name'].'%"');

        $count = $this->db->get()->result_array();
        $count = $count[0]['cnt'];

        $page = $page - 1;

        $limit = $page * 10;

        $ret['count'] = $count;
        $ret['data'] = $this->select_full_list($data,$limit);

        return $ret;
    }


    // ������ � ������� ������ - ��������, ��� ���������� �������
    public function select_short_list()
    {
        return $this->db->select("c.id,c.name,cl.name as clientname")->from('campaign c')->join("client cl","c.id_client=cl.id")->get()->result_array();
    }

    public function select_full_single($id)
    {
        $data = $this->db->select("c.*,cl.name as clientname,cl.id as clientid")->from("campaign c")->join("client cl","c.id_client=cl.id")->where("c.id",$id)->get()->result_array();
        $campaign = $data[0];

        // ���� � �������� ���� ���������, �� � ��� ���� ����� � share = 0
        // ����� ����� �����, ������� ���������

        $channel = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("c.share",0)->where('co.id_object',$id)->where('co.object_type','campaign')->get()->result_array();
        if(sizeof($channel))
        {
            $this->load->model("model_channel");
            $campaign['settings'] = $this->model_channel->select_settings_by_channel($channel[0]['id_channel']);
        }

        $channels = $this->db->select('co.id_channel')->from("channel_object co")->join('channel c','co.id_channel=c.id')->where("co.object_type","campaign")->where("co.id_object",$id)->where('c.share',1)->get()->result_array();
        if(sizeof($channels))
            $campaign['channels'] = $channels;

        // ��� ������� �������� � ����������
        $this->db->select("b.id as id_banner,z.id as id_zone");
        $this->db->from("banner_zone bz");
        $this->db->join("banner b","b.id=bz.id_banner","inner");
        $this->db->join("zone z","z.id=bz.id_zone","inner");
        $this->db->join("campaign c","c.id=b.id_campaign","inner");
        $this->db->where('b.id_campaign',$id);

        $campaign['banners'] = $this->db->get()->result_array();

        return $campaign;
    }

    // ��������� �������� ��������
    public function set_settings($id,$settings)
    {

        foreach($settings as $v)
        {
            $check = $this->db->select('id')->from('campaign_setting')->where('setting',$v['setting'])->where('id_campaign',$id)->get()->result_array();
            if(sizeof($check)) {
                $this->update("campaign_setting",array("value"=>$v['value']),array("id"=>$check[0]['id']));
            }
            else
            {
                $this->model_common->insert("campaign_setting",array("id_campaign"=>$id,"setting"=>$v['setting'],"value"=>$v['value']));
            }
        }
    }


    public function delete($id)
    {
        $this->load->model("model_banner");

        // �������� �������� �������� �������� �� �������� � ��������
        $this->db->where('id',$id);
        $this->db->delete("campaign");

        $this->db->where('id_object',$id);
        $this->db->where('object_type','campaign');
        $this->db->delete("channel_object");

        $this->db->where('id_campaign',$id);
        $this->db->select('*');
        $banners = $this->db->get("banner")->result_array();

        foreach($banners as $banner)
        {
            $this->model_banner->delete($banner['id']);
        }
    }

}