<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:19
 */

class Model_Client extends Model_Common
{
    public function __construct()
    {
        parent::__construct();
    }

    // ������ �������� � ����������
    public function select_full_list($data)
    {
        $this->load->model("model_stat");
        $this->db->from('client c');
        //$this->db->where("id_site",$id_site);
        $this->db->join('campaign cn','cn.id_client=c.id','left');
        $this->db->select('c.*,count(cn.id) as cnt_campaigns');
        if(isset($data['name'])) $this->db->where('c.name LIKE "%'.$data['name'].'%"');
        $this->db->group_by('c.id');
        $this->db->order_by('c.id','desc');
        $list = $this->db->get()->result_array();
        foreach($list as &$v)
        {
            $campaigns = $this->db->select('c.name,c.id')->from('campaign c')->where('id_client',$v['id'])->get()->result_array();
            $v['stat'] = array("impressions"=>0,"clicks"=>0,"ctr"=>0);
            foreach($campaigns as &$camp)
            {
                $counts = $this->db->query("SELECT (SELECT COUNT(b.id) FROM banner b WHERE b.status=1 AND b.id_campaign='".$camp['id']."') AS oncount, (SELECT COUNT(b.id) FROM banner b WHERE b.id_campaign='".$camp['id']."') AS totalcount")->result_array();
                $camp['totalcount'] = $counts[0]['totalcount'];
                $camp['oncount'] = $counts[0]['oncount'];
                $camp['stat'] = $this->model_stat->get_stat_by_campaign($camp['id']);
                $v['stat']['impressions'] += $camp['stat']['impressions'];
                $v['stat']['clicks'] += $camp['stat']['clicks'];
            }
            $v['campaigns'] = $campaigns;
            if($v['stat']['impressions'])
            {
                $v['stat']['ctr'] = round((($v['stat']['clicks'] / $v['stat']['impressions']) * 100),2);
            }
        }

        return $list;
    }

    // ������ �� �������, � ������� ���� ��������� ��������
    public function select_filled($where = 0)
    {
        $this->db->select('c.name,c.id,count(cn.id) as campaigncount');
        $this->db->from('client c');
        $this->db->join('campaign cn',"cn.id_client=c.id","left");
        if($where) $this->db->where($where);
        $this->db->group_by("c.id");
        $this->db->having("campaigncount > 0");
        $this->db->order_by("c.id");
        return $this->db->get()->result_array();
    }

    public function delete($id)
    {
        $this->load->model("model_campaign");

        $campaigns = $this->db->select("id")->from("campaign")->where("id_client",$id)->get()->result_array();
        foreach($campaigns as $camp)
        {
            $this->model_campaign->delete($camp['id']);
        }

        $this->db->where('id',$id);
        $this->db->delete("client");

    }

}