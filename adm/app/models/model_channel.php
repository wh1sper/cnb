<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:19
 */

class Model_Channel extends Model_Common
{

    public function select($id=null)
    {
        $this->db->select('id,name');
        $this->db->from("channel");
        $this->db->where("share",1);
        if(!is_null($id)) $this->db->where("id",$id);
        $this->db->order_by("id");
        $data = $this->db->get()->result_array();

        foreach($data as &$v)
        {
            $settings = $this->select_settings_by_channel($v['id']);
            if(sizeof($settings)) $v['settings'] = $settings;
        }

        return $data;
    }

    // ��������� ��������� � ������
    public function add_setting_to_channel($id_chanel,$id_setting,$value,$type)
    {
        $data['id_channel'] = $id_chanel;
        $data['id_setting'] = $id_setting;
        $data['value'] = $value;
        $data['type'] = $type;

        if(is_array($data['value']))
            $data['value'] = serialize($data['value']);

        // check
       // $check = $this->db->select("id")->from("channel_setting")->where("type",$type)->where("id_channel",$id_chanel)->where("id_setting",$id_setting)->get()->result_array();
        //if(sizeof($check))
        //{
         //   $this->model_common->update("channel_setting",$data,array("id_channel"=>$id_chanel,"id_setting"=>$id_setting));
        //}
        //else
        //{
            $this->model_common->insert("channel_setting",$data);
        //}

    }

    // ������ ������� (��������, ������) � �������
    // @param object_type = banner/campaign
    public function add_channel_to_object($id_channel,$id_object,$object_type)
    {
        $data['id_channel'] = $id_channel;
        $data['id_object'] = $id_object;
        $data['object_type'] = $object_type;
        // check

        $check = $this->db->select('id')->from('channel_object')->where('id_channel',$id_channel)->where('id_object',$id_object)->where('object_type',$object_type)->get()->result_array();
        if(!sizeof($check))
            $this->model_common->insert("channel_object",$data);
    }

    // ������� ��� ��������� ������ (����� �����������)
    public function clear_settings($id)
    {
        $this->db->from("channel_setting");
        $this->db->where("id_channel",$id);
        $this->db->delete();
    }

    public function clear_personal_channel_settings($id,$object_type)
    {
        $channel = $this->db->select("id_channel")->from("channel_object")->where("id_object",$id)->where("object_type",$object_type)->get()->result_array();
        $id_channel = $channel[0]['id_channel'];
        $this->model_common->delete("channel_setting",array("id_channel"=>$id_channel));
    }

    public function clear_public_channels($id_object,$object_type)
    {
        $this->db->select('co.id');
        $this->db->from('channel_object co');
        $this->db->join('channel c','co.id_channel=c.id');
        $this->db->where('c.share',1);
        $this->db->where('co.object_type',$object_type);
        $this->db->where('co.id_object',$id_object);
        $res = $this->db->get()->result_array();
        foreach($res as $v){
            $this->model_common->delete("channel_object",array("id"=>$v['id']));
        }
    }

    public function add_personal_channel($id_object,$object_type,$settings)
    {
        $this->db->select('c.id');
        $this->db->from('channel_object co');
        $this->db->join('channel c','co.id_channel=c.id');
        $this->db->where('c.share',0);
        $this->db->where('co.object_type',$object_type);
        $this->db->where('co.id_object',$id_object);
        $check = $this->db->get()->result_array();

        if(sizeof($check))
        {
            $id = $check[0]['id'];
        }
        else
        {
            $data['name'] = 'private';
            $data['share'] = 0;
            $id = $this->model_common->insert("channel",$data);
        }

        foreach($settings as $k=>$set)
        {
            $this->add_setting_to_channel($id,$set['id'],$set['value'],$set['type']);
        }

        $this->add_channel_to_object($id,$id_object,$object_type);

    }



    private function getDay($num)
    {
        switch($num) {
            case "1": $ret = "�����������"; break;
            case "2": $ret = "�������"; break;
            case "3": $ret = "�����"; break;
            case "4": $ret = "�������"; break;
            case "5": $ret = "�������"; break;
            case "6": $ret = "�������"; break;
            case "7": $ret = "�����������"; break;
        }
        return $ret;
    }

    // ������� �������� ������
    public function select_settings_by_channel($id_channel)
    {
        $this->db->select("s.id,s.label,cs.type,cs.value");
        $this->db->from("channel_setting cs");
        $this->db->join("setting s","s.id=cs.id_setting");
        $this->db->where("cs.id_channel",$id_channel);
        $data = $this->db->get()->result_array();

        foreach($data as &$v)
        {
            if($v['id']=="limit_time")
            {
                $v['value'] = unserialize($v['value']);
            }

            if($v['id']=="limit_days")
            {
                $days = unserialize($v['value']);
                unset($v['value']);

                foreach($days as $day)
                {
                    $v['value'][$day] = $this->getDay($day);
                }
            }

            if($v['id']=="limit_mobile")
            {
                $v['value'] = $v['value']==1 ? "��" : "���";
            }

        }

        return $data;
    }

    // ������� ������� ��� ��� �������
    public function select_cities($type,$convert = 1)
    {
        if(!in_array($type,array("city","state","region"))) return 0;
        $data = $this->db->select($type." as name")->from("geo_zone")->distinct()->order_by($type)->get()->result_array();

        if($convert) {
            foreach($data as &$v)
            {
                $v['name'] = iconv('windows-1251','utf-8',$v['name']);
            }
        }

        return $data;
    }

    // ������� ������ ��������� ��������
    public function select_settings()
    {
        $data = $this->db->select("*")->from("setting")->order_by("sort")->get()->result_array();
        return $data;
    }

    public function __construct()
    {
        parent::__construct();
    }


    public function delete($id)
    {
        $this->db->where("id_channel",$id);
        $this->db->delete("channel_setting");

        $this->db->where("id",$id);
        $this->db->delete("channel");
    }

}