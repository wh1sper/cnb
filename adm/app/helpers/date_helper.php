<?php

	// ������� � �������� ��������� ��������� ����� ������ �
	// ���������� ��������� ������������ � ���������� �����������
	// � ���� ��������� ������
	function last_visit_to_str($sec)
	{	
		$minutes = round($sec/60);
		if ($minutes <= 60)
		{
			return $minutes." ".$this->_declension($minutes, array("������", "������", "�����"));
		}

		$hourse = round($minutes/60);
		if ($hourse <= 24)
		{
			return $hourse." ".$this->_declension($hourse, array("���", "����", "�����"));
		}		

		$days = round($hourse/24);
		return $days." ".$this->_declension($days);
	}

	// ������������ ���� ���� 2009-09-16 00:05:42 �  16 ������� 2009 � 00:05
	function date_convert($date)
	{
	    $full_date = explode(" ", $date);
	    $date = explode("-", $full_date[0]);
	    if ($date[0] == "0000") return 0; // ������ ����
	    
	    $months = array(
	        "01" => "������",
    	    "02" => "�������",
    	    "03" => "�����",
    	    "04" => "������",
    	    "05" => "���",
    	    "06" => "����",
    	    "07" => "����",
    	    "08" => "�������",
    	    "09" => "��������",
    	    "10" => "�������",
    	    "11" => "������",
    	    "12" => "�������",
	    );
	    
	    $result = $date[2]." ".$months[$date[1]]." ".$date[0];
	    
	    if (!empty($full_date[1]))
	    {
	    	$time = explode(":", $full_date[1]);
	   		$result .= " � ".$time[0].":".$time[1];
	    }
	    
	    return $result;
	}
	
	// ����������� ���� ���� 2010-09-16 00:05:42 � 16/09/10
	function date_simple_convert($date = "")
	{
		if (substr_count($date, " "))
		{
			$date = explode(" ", $date);
			$date = $date[0];
		}
		$date = explode("-", $date);
		return $date[2]."/".$date[1]."/".substr($date[0], 2);
	}
	
	// ������� ���������� ������� ����/����� �������� �� ��������� ����
	function time_stay($date)
	{
	    //2009-12-31 21:08:00
	    $y = substr($date, 0, 4);
	    $m = substr($date, 5, 2);
	    $d =substr($date, 8, 2);
	    $h =substr($date, 11, 2);
	    $min =substr($date, 14, 2);
	    $res_h = floor((mktime($h, $min, 0, $m, $d, $y) - time())/60/60); // ����� ����� �� �������
	   // if ($res_h <=0) return false;
	    $res_d = abs(($res_h - $res_h%24)/24); // ������ ���� �� �������
	    $res_h = abs($res_h%24); // ������ ����� �� �������
	    
	    return array(
		    	"d" => round($res_d), 
		    	"d_str" => $this->_declension($res_d),
		    	"h" => round($res_h), 
		    	"h_str" => $this->_declension($res_h, array('���', '����', '�����'))
	    	);
	}

	function days_to_str($days)
	{	
		return $days." ".$this->_declension($days);
	}	

	function str_to_day($date_str)
	{	
		$date_array = array("-0-" => "��", "-1-" => "��", "-2-" => "��", "-3-" => "��", "-4-" => "��", "-5-"=> "��", "-6-" => "��");

		return strtr($date_str, $date_array);
	}	

	
	// ���� ������ $date_str �������� � ���� ����� ������ ����������� � ������ -, 
	// �� ������� �������� ��� �� ������������� �������� ������.
	// � �������, ����� ������� �� ����� ���� 1 ��� 2009 ����. 
	// �������� ���� �� ���� � ���� 1 -5- 2009 � ������������ ���� ��������
	function convert_month_in_str($date_str)
	{
		$date_array = array("-01-" => "������", "-02-" => "�������", "-03-" => "�����", "-04-" => "������", "-05-" => "���", "-06- "=> "����", "-07-" => "����", "-08-" => "�������", "-09-" => "��������", "-10-" => "�������", "-11-" => "������", "-12-" => "�������");

		return strtr($date_str, $date_array);
	}

	// ������ ����� ��� ����������� ������
	function get_years_array()
	{
		$a_years = array();
		$y = date("Y")-12;
		for ($i = 1; $i < 70; $i++)
		{
			$a_years[] = $y-$i;
		}
		
		return $a_years;
	}
	
?>