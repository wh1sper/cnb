<?php 
/**
 * 
 * ����� ������ � �������
 * @author ashmits
 *
 */
class My_Text_Helper{
	
	protected static $instance;
	private function __construct(){}
	private function __wakeup(){}
	private function __clone(){}
	
	public static function getInstance()
	{
		
		if (is_null(self::$instance))
		{
			$classname = __CLASS__;
			self::$instance = new $classname;
		}
		
		return self::$instance;
		
	}

	/**
	 * 
	 * ������� ���������� ����� ���������� ����� � ������, ������������ ��� �������� keywords ����
	 * @param unknown_type $text
	 * @param unknown_type $count_words
	 */
    public static function get_keywords($text = "", $count_words = 12)
    {
    	
        $text = str_replace(array("\n", "<p>", "<p />", "<br />", "<br>", ".", ",", ":", ";", "-"), " ", $text);
        $text = str_replace(array("&nbsp"), " ", $text);
        $text = self::text_format(strip_tags($text), 0);
        $text = preg_replace("/[^�-��-�a-zA-Z ]/ies", ' ', $text);

        $array = explode(" ", $text);

        foreach ($array as $key => $val)
        {
            if (strlen($val) < 5) unset($array[$key]);
        }

        //usort($array, function($a, $b){ return My_Text_Helper::sort_max_word_len($a, $b); });
        
        
        $array_result = array();

        if (isset($array[0]))
        {
        	$array_result[] = array('word' => $array[0], "count" => "1");
        	unset($array[0]);
        }

        foreach ($array as $key => $word)
        {
            $i = 0;
            foreach ($array_result as $r_key => $r)
            {
                if (substr_count($r['word'], $word))
                {
                    $array_result[$r_key]['count']++;
                    $i = 1;
                    break;
                }
            }

            if ($i == 0) $array_result[] = array('word' => $word, "count" => "1");

        }

        //usort($array_result, function($a, $b){ return My_Text_Helper::sort_max_word_len($a, $b); });
		
        $array_result = array_slice($array_result, 0, $count_words);

        $result_str = "";
        foreach ($array_result as $a)
        {
            if (!empty($result_str))
            {
            	$result_str .= ", ";
            }
            $result_str .= $a['word'];
        }

        return $result_str;
        
    }

	public static function sort_max_word_len($a, $b)
	{
		if (!is_string($a) or !is_string($b))
			return null;
			
		if (strlen($a) == strlen($b)) {
			return 0;
		}
		return (strlen($a) < strlen($b)) ? 1 : -1;
	}
    
	
	public static function text_format($word, $fl) 
 	{
		$up_letter   = '�����Ũ��������������������������';
	 	$down_letter = '��������������������������������';
	  	if($fl == '1') 
	  	{
	  		return trim(strtr($word, $down_letter, $up_letter));
	  	}
	    
	  	return trim(strtr($word, $up_letter, $down_letter));
 	}    
	
	/**
	 * 
	 * ������������ ����� ������
	 * @param String $text
	 * @param String $page
	 */
	public static function get_text_by_page($text, $page = 1)
	{
		
		$CI =& get_instance();
		$page = $CI->validate->set_int($page);
		$page;
		
		$start = "\<\!--\s{1,}page_" . ($page-1) . "\s--\>";
		$end = "\<\!--\s{1,}page_{$page}\s--\>";
		
		//���� � ������ ��� �������� �� ���������, ������� �������� �����
		if (!preg_match("/\<\!--\s{1,}page_(\d{1,})\s{1,}--\>/", $text) and empty($page))
		{
			return (string)$text;
		}
		
		$paging = array();
		
		if (preg_match_all("/\<\!--\s{1,}page_([0-9]+)\s{1,}--\>/i", $text, $pages))
		{
			if (count($pages[0]) > 0)
			{
				for ($key=1; $key <= count($pages[0])+1; $key++)
				{
					$class = "";
					if ($key == $page)
					{ 
						$class = " class = 'active' ";
					}
					
					if (preg_match("/\/page([0-9]+)\/?$/", My_Url_Helper::get_current_url()))
					{
						$paging[] = "<a {$class} href='" . preg_replace("/(?:\/)page([0-9]+)(?:\/)?$/", "/page{$key}/", My_Url_Helper::get_current_url()) . "'>" . $key . "</a>";
					}
					else 
					{
						$paging[] = "<a {$class} href='" . preg_replace("/\/$/", "/page{$key}/", My_Url_Helper::get_current_url()) . "'>" . $key . "</a>";
					}
				}
			}
		}
		
		//������� ��������
		if ($page == 1)
		{
			//���� ������ ��������
			if(preg_match("/(?:$start)?(.+?)(?:$end)/is", $text, $match))
			{
				$text = (string)$match[1];
			}
		}
		else 
		{
			//���� �������� ����� ������ � ����������
			if(preg_match("/(?:{$start})(.+?)(?:{$end})/is", $text, $match))
			{
				$text = (string)$match[1];
			}
			//��������� ��������
			elseif (preg_match("/(?:$start)(.+?)$/is", $text, $match))
			{
				$text = (string)$match[1];
			}
			else
			{
				show_error("�������� �� �������");
			}
		}
		
		if (count($paging))
		{
			$text .= "<div class='next_page'>" . implode("&nbsp;&nbsp;", $paging) . "</div>";
		}
		
		return (string)$text;
		
	}
	
	/**
	 * 
	 * ���� ������� �� �������
	 * @param unknown_type $text
	 */
	public static function set_pages_by_sort($text)
	{
		
		if (!preg_match("/\{page([0-9]+)?/i", $text))
		{
			return $text;
		}
		
		global $count;
		$count = 1;
		
		$pages = preg_split("/\{page([0-9]+)?\}/", $text);
		$text = "";
		if ($pages and is_array($pages))
		{
			for($i = 0; $i < count($pages); $i++)
			{
				$j = $i+1;
				$text .= $pages[$i] . (($i < count($pages)-1) ? "{page{$j}}" : "");
			}
		}
		
		return $text;
		
	}
	
	/**
	 * 
	 * ������ ���� {page(:num)} -> <!-- page_(:num) -->
	 * @param string $text
	 */
	public static function set_pages_tags($text)
	{
		return preg_replace("/\{page([0-9]+)\}/","<!-- page_$1 -->", $text);
	}
	
	/**
	 * 
	 * �������� ������, ��� ��� ���
	 * @param string $str
	 */
	public static function is_hash($str)
	{
		return (bool)preg_match("/^([0-9a-f]{40})$/", $str);
	}
	
	public static function strip_tags($text)
	{
		
		$text = preg_replace("/\<(tr|td|table|div|th|tbody|thead|tfoot|a|span|hr|em|br|form|input|ul|li|ol|h1|h2|h3|h4|h5|h6|img|object)[^\>]*\>/i", "", $text);
		$text = preg_replace("/\<\/(tr|td|table|div|th|tbody|thead|tfoot|a|span|hr|em|br|form|input|ul|li|ol|h1|h2|h3|h4|h5|h6|img|object)\>/i", "", $text);
		
		return $text;
		
	}
	
	/*
	 * �������� ������ ������ �� ������� ����� ������
	 */
	public static function set_gallery_scripts($text)
	{
		//������� ��� ���� <script>
		if (preg_match_all("/\<script([^\>]*)\>/i", $text, $matches))
		{

			foreach ($matches[1] as $key=>$val)
			{
				//������� ������ �� �������
				if (preg_match("/src=(\'|\")([^(\'|\")]*)(\'|\")/i", $matches[1][$key], $match2))
				{
					//���� ������ �� �������
					if (preg_match("/^http:\/\/zoom\.cnews\.ru\/\/?inc\/\/?gallery\.php/i", $match2[2]))
					{
						$src = DOMAIN . '/admin/ajax/get_remote_url/?url=' . $match2[2];
						$new_script = str_replace($match2[2], $src, $matches[0][$key]);
						$text = str_replace($matches[0][$key], $new_script, $text);
					}
				}
			}
		}
		
		return (string) $text;
		
	}
	
	
	//������� ����� ���� {table}, {td} �� <table>, <td>
	public static function set_tags_in_text($text)
	{
		
		$CI =& get_instance();
		$tags = $CI->config->item('tags');
		
		foreach ($tags as $tag)
		{
			
			$regexp_open = sprintf("/\{(%s)([^\}]*)\}/i", $tag);
			$regexp_close = sprintf("/\{\/(%s)(\})/i", $tag);
			
			$text = preg_replace($regexp_open, "<$1$2>", $text);
			$text = preg_replace($regexp_close, "</$1>", $text);
			
		}
		
		return (string)$text;
		
	}
	
	
	//������� ����� �� ������ �� youtube
	public static function get_youtube_key($video_link)
	{
		
		$array = explode("/", $video_link);
		
		if (count($array))
		{
			$key = $array[count($array)-1];
			$key = preg_replace("/\?.*/","", $key);
			
			return (string)$key;
		}
		
		return null;
		
	}
	
	
	/**
	 * 
	 * �������� �����, �������� ����� ������
	 * @param unknown_type $text
	 * @param unknown_type $length
	 */
	public static function truncate_text($text, $length)
	{
		
		if (strlen($text) > $length)
		{
			$t_text = "";
			
			if (preg_match_all("/([^(\s|\,|\.)]+)(\s{1,}|\,|\.)?/", $text, $matches))
			{
				
				if (!empty($matches[1]))
				{
					foreach ($matches[1] as $key=>$val)
					{
						if (strlen($t_text) < $length)
						{
							
							$separator = $matches[2][$key];
							$t_text .= $val;
							if (strlen($t_text) < $length)
							{
								$t_text .= $separator;
							}
							
						}
						else 
						{
							
							break;
							
						}
					}
				}
				
			}
			
		}
		else 
		{
			$t_text = $text;
		}
		
		return $t_text;
		
	}
	
	
	/**
	 * 
	 * ������ ����.����
	 * @author ashmits by 23.04.13 14:15
	 * @param String $text
	 */
	public static function clear_special_tags($text)
	{
		
		$text = preg_replace("/\{table[^\}]*\}/i", "", (string)$text);
		
		$text = preg_replace("/\{gallery[^\}]*\}/i", "", (string)$text);
		
		$text = preg_replace("/\{photogallery[^\}]*\}/i", "", (string)$text);
		
		$text = preg_replace("/\{incut[^\}]*\}[^\{]*\{\/incut\}/i", "", (string)$text);
		
		$text = preg_replace("/\{video.*?\}/i", "", (string)$text);
		
		$text = preg_replace("/\{videos.*?\}/i", "", (string)$text);
		
		$text = preg_replace("/\<br\sclass=(\'|\")wysiwyg-block-newline(\'|\")\>/", "", $text);
		
		$text = preg_replace("/\[\/?(div|p)([^\]]*)\]/", "", $text);
		
		return (string)$text;
		
	}
	
	/**
	 * 
	 * ������ �������� ����.����
	 * @param string $text
	 * @return string
	 */
	public static function clear_bootstrap_spec_tags($text)
	{
		
		$text = preg_replace("/\<br\sclass=(\'|\")wysiwyg-block-newline(\'|\")\>/", "", $text);
		
		$text = preg_replace("/\[\/?(div|p)([^\]]*)\]/", "", $text);
		
		return (string)$text;
		
	}
	
	/**
	 * 
	 * ������ �������� � css
	 * @param String $text
	 */
	public static function clear_comments_cssjs($text)
	{
		
		if (preg_replace("/\s+/","", $text) == "")
			return false;
		
		$text = preg_replace("/\n/", "", $text);
		
		$text = preg_replace("/\/\*.*?\*\//", "", $text);
		
		return $text;
		
	}
	
	/**
	 * 
	 * ������� ����� � ������
	 * @param unknown_type $text
	 */
	public static function set_videos_in_text($text = null)
	{
		
		if (trim($text) != "")
		{
			$CI =& get_instance();
			
			if (preg_match_all("/\{video(.*?)?\}/", $text, $matches))
			{
				
				foreach ($matches[1] as $key=>$val)
				{
					
					if (preg_match("/src=\"([^\"]*)\"/", strip_tags($val), $src))
					{
						$video_src = $src[1];
						$video_src = str_replace("[youtube]", "//www.youtube.com/embed/", $video_src);
						$CI->smarty->assign('video_src', $video_src);
					}
					
					if (preg_match("/title=\"([^\"]*)\"/", strip_tags($val), $title))
					{
						$video_title = $title[1];
						$CI->smarty->assign('video_title', $video_title);
					}
					
					
					if (preg_match("/id=\"([0-9]+)\"/", strip_tags($val), $id))
					{
						$video_id = (int)$id[1];
						$video = $CI->model_common->select_one("videos", array("video_id" => $video_id));
						if (!empty($video) and !empty($video['video_file']))
						{
							if (preg_match("/youtu/i", $video['video_file']) and !preg_match("/\/embed\//i", $video['video_file']))
							{
								$youtube_key = self::get_youtube_key($video['video_file']);
								$video['video_file'] = "//www.youtube.com/embed/" . $youtube_key;
							}
							
							$CI->smarty->assign('video_src', $video['video_file']);
						}
					}
					
					$video_template = $CI->smarty->fetch('front/blocks/video_block.tpl');
					
					$text = str_replace($matches[0][$key], $video_template, $text);
					
				}
			}
			
			
			
		}
		
		return $text;
		
	}
	
	/**
	 * 
	 * ������� ������ � ������
	 * @param unknown_type $text
	 */
	public static function set_incut_in_text($text = null)
	{
		
		if (trim($text) != "")
		{
			
			if (preg_match_all("/\{incut([^\}]*)\}([^\{]*)\{\/incut\}/", $text, $matches))
			{

				if (!empty($matches[0]))
				{
					$CI =& get_instance();
					
					foreach ($matches[0] as $key=>$val)
					{
						
						$tag = $val;
						$float_text = 'right';
						$float_block = 'left';
						$title = "";
						$incut_text = "";
						
						//������������ ������ � �����
						if (preg_match("/float_text='(left|right)'/i", $tag, $matches1))
						{
							
							if (!empty($matches1[1]))
								$float_text = (string)$matches1[1];
								
						}
						
						//������������ �����
						if (preg_match("/float_block=(\'|\")(left|right)(\'|\")/", $tag, $matches2))
						{
							if (!empty($matches2[2]))
								$float_block = (string)$matches2[2];
						}
						
						//��������� ������
						if (preg_match("/title=(\'|\")([^\']*)(\'|\")/", $matches[1][$key], $matches3))
						{
							if (!empty($matches3[2]))
							{
								$title = (string)$matches3[2];

								//������� ��� ������ � ���������
								$title = preg_replace("'<a([^\>]*)>([^\<]*)</a>'is", "$2", $title);
								
								if (preg_match_all("/\[img(.*?)src=\"([^\"]*)\"\]/i", strip_tags($title), $timages))
								{
									
									if (!empty($timages[0]))
									{
										foreach ($timages[0] as $tkey=>$tval)
										{
											
											if (preg_match("/class=\"([^\"]*)\"/", $timages[1][$tkey], $css_class))
											{
												
												if (!empty($css_class[1]))
												{
													$CI->smarty->assign('css_class', $css_class[1]);
												}
												
											}
											
											$tsrc = $timages[2][$tkey];
											$CI->smarty->assign("src", $tsrc);
											$image_tpl = $CI->smarty->fetch("front/blocks/incut_images.tpl");
											$title = str_replace($timages[0][$tkey], $image_tpl, $title);
										}
									}
									
								}
								
							}
							
						}
						
						if (preg_match("/title_top=(\'|\")true(\'|\")/i", $tag))
						{
							$CI->smarty->assign('title_top', 'true');
						}
						
						//������� ����� ������, ������ ����� ��������, �� ���� ����� {incut dddd [img adfsdfsadf class="test"] class="ssss"}
						if (preg_match("/(?!<\[.*?)class=\"([^\"]*)\"(?!.*?\])/i", $tag, $class))
						{
							$CI->smarty->assign('class', $class[1]);
						}
						
						if (preg_match("/\{incut.*?\}([^\{]*)?\{\/incut\}/", $tag, $matches4))
						{
							if (!empty($matches4[1]))
								$incut_text = (string)$matches4[1];
						}
						
						$incut_text = $incut_text;
						
						if (preg_match_all("/\[img\s{1,}src=\"([^\"]*)\"\]/i", strip_tags($incut_text), $images))
						{
							
							foreach ($images[1] as $key=>$val)
							{
								$src = $val;
								
								$CI->smarty->assign("src", $src);
								$image_tpl = $CI->smarty->fetch("front/blocks/incut_images.tpl");
								$incut_text = str_replace($images[0][$key], $image_tpl, $incut_text);
							}
						}
						
						$CI->smarty->assign('float_text', $float_text);
						$CI->smarty->assign('float_block', $float_block);
						$CI->smarty->assign('title', $title);
						$CI->smarty->assign('incut_text', $incut_text);
						$incut_template = $CI->smarty->fetch('front/blocks/incut_block');
						
						$text = str_replace($tag, $incut_template, $text);
						
					}
				}
				
			}
			
			
		}

		return (string)$text;
		
	}
	
	/**
	 * 
	 * ������� �������� � ������
	 * @author ashmits by 08.04.13 19:00
	 * @param unknown_type $text
	 */
	public static function set_galleries_in_text($text)
	{

		if (trim($text) == "")
		{
			return false;
		}
		
		if (preg_match_all("/\{gallery.*?\}/i", $text, $galleries))
		{
			
			$CI =& get_instance();
			
			foreach ($galleries[0] as $key=>$val)
			{
				
				//������� ������ �� �������
				if (preg_match("/src=\"([^\"]*)\"/i", $val, $src))
				{
					if (!empty($src[1]))
					{
						$gallery_src = (string)$src[1];
						$CI->smarty->assign('gallery_src', $gallery_src);
					}
				}
				
				//������� �������� ��������
				if (preg_match("/description=\"([^\"]*)\"/i", $val, $description))
				{
					if (!empty($description[1]))
					{
						$gallery_description = (string)strip_tags($description[1]);
						$CI->smarty->assign('gallery_description', $gallery_description);
					}
				}
				//�������� ������
				$template = $CI->smarty->fetch('front/blocks/gallery_block');
				//�������� ��� �� �������� � ������
				$text = str_replace($galleries[0][$key], $template, $text);
			}
		}
		
		return (string)$text;
		
	}
	
	/**
	 * 
	 * ������� ������� - ������������
	 * @param string $text
	 * @return string
	 */
	public static function set_gallery_in_text($text)
	{
		
		if (preg_match_all("/\{photogallery\s{1,}id=(\'|\")([0-9]+)(\'|\")\}/i", $text, $matches))
		{
			$CI =& get_instance();
			$CI->load->model('model_gallery');
			foreach ($matches[0] as $key=>$val)
			{
				$gallery_id = $CI->validate->set_int($matches[2][$key]);
				$gallery_code = $matches[0][$key];
				
				if (!empty($gallery_id))
				{
					$images = $CI->model_gallery->get_images_by_gallery($gallery_id);
					$CI->smarty->assign('images', $images);
					$gallery_template = $CI->smarty->fetch("admin/gallery/gallery_include_block");
					$text = str_replace($gallery_code, $gallery_template, $text);
				}
			}
		}
		
		//������� ��� �� ��������� �������
		return preg_replace("/\{photogallery([^\{]+)\}/i", "", $text);
		
	}
	
	
	public static function generate_password($length = 20){
	  
		$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	
	  	$str = '';
	  	$max = strlen($chars) - 1;
		
	  	for ($i=0; $i < $length; $i++)
	    	$str .= $chars[rand(0, $max)];
	
	  	return $str;
	}
	
	
	public static function _cut_long_annotation($annotation, $len)
	{
		
		//�������� ������� ������� ��������
		if (strlen($annotation)>$len and !empty($annotation))
		{
			
			// ������ ����� ���������� �������, � �������� ����� ����������
			// len_description. ����� �� �������� ����� �� ��������.
			$annotation=stripslashes($annotation);
			$pos = strpos($annotation, ' ', $len);
			// ������ ��� � ����������� ����� ������� ���� ������� ������� � $len
			// ��� ���� ������. �� �������� ����� $len ���. ����� $pos ����� ��������� �������.
			// �������� ��������:
			if (empty($pos) and $len > 10) $pos = strpos($annotation, ' ', $len - 10);
			$annotation = substr_replace($annotation, '', $pos)."...";
			//�� ������, ���� � ����� ������ ������� ��� ����� ('���,...' ��� '���....')
			$annotation = str_replace(',...', '...', $annotation);
			$annotation = str_replace('......', '...', $annotation);
			
		}
		
		return $annotation;
		
	}
	
}

?>