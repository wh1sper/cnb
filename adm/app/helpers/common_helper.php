<?php 

/**
 * ����� ��������
 * @author ashmits by 18.12.2012 17:18
 *
 */

class Common_Helper
{
	
	protected static $instance;
	private function __construct(){}
	private function __wakeup(){}
	private function __clone(){}
	
	public static function getInstance()
	{
		if (is_null(self::$instance))
		{
			$classname = __CLASS__;
			self::$instance = new $classname;
		}
		
		return self::$instance;
	}
	
	/**
	 * 
	 * ���������� ���.������� ��������� �������� �� �������
	 */
	public static function get_cnf_min_size()
	{
		
		$CI =& get_instance();
		$CI->load->library('custom');
		$min_size = $CI->config->item('image_min_size');
		$min_size = explode("x", $min_size);
		
		if (empty($min_size) or !is_array($min_size) or count($min_size) < 2)
		{
			return null;
		}
		
		$custom = new Custom();
		$custom->width = $min_size[0];
		$custom->height = $min_size[1];
		
		return $custom;
		
	}
	
	
	/**
	 * ������� ������ �� �����
	 * @param String $text
	 * @return String
	 * @author ashmits
	 * ������� ���� ������ � ������ � �������� �� �� �������
	 * ������ �����: {table id="(:num)"[ class="class_name"]}
	 */
	public static function get_tables_by_tags($text)
	{
		
		$CI =& get_instance();
		$CI->load->helper('tables');
		return Tables_Helper::get_tables_by_tags($text);
		
	}
	
	/**
	 * �������� ������� � ����������� ����
	 * @param String $table_structure
	 * @param String $description
	 * @return string
	 */
	public static function set_structure_table($table_structure, $description = null)
	{
		
		$CI =& get_instance();
		$CI->load->helper('tables');
		return Tables_Helper::set_structure_table($table_structure, $description);
		
	}
	
	/**
	 * �������� �������, ��� ������
	 * @param String $table_structure
	 * @param String $description
	 * @param Int $count_rows
	 * @param String $custom_class - ���������������� ����� �������, ����������� � ���������
	 * @return NULL|string
	 */
	public static function table_configure_by_limit($table_structure = null, $description = null, $count_rows = 11, $custom_class=NULL)
	{
		
		$CI =& get_instance();
		$CI->load->helper('tables');
		return Tables_Helper::table_configure_by_limit($table_structure, $description, $count_rows, $custom_class);
		
	}
	
	/**
	 * ������������� �����. ������������ �������
	 * @author ashmits by 29.12.2012 11:12
	 * @param String $in_charset
	 * @param String $out_charset
	 * @param array $array
	 * @param 0,1,2 $special_chars - ������� �����������/������������� ������
	 * 0-�� �������
	 * 1-��������
	 * 2-����������
	 * @return Array
	 */
	public static function assoc_array_decode($in_charset = null, $out_charset = null, $array = null, $special_chars = 0)
	{
		
		if (empty($array) or !is_array($array))
		{
			return null;
		}
		
		foreach($array as $key=>$val)
		{
			
			if (is_array($val))
			{
				$array[$key] = Common_Helper::getInstance()->assoc_array_decode($in_charset, $out_charset, $val, $special_chars);
			}
			else
			{
				
				$val = iconv($in_charset, $out_charset, $val);
				if ($special_chars == 1)
				{
					$val = htmlspecialchars($val, ENT_QUOTES);
				}
				elseif ($special_chars == 2)
				{
					$val = htmlspecialchars_decode($val, ENT_QUOTES);
				}
				
				$array[$key] = $val;
				
			}
			
		}
		
		return $array;
		
	}
	
	
	/**
	 * �������� ������ � ��� � ������� � ���� ���� ��� ������ ����������
	 * @author ashmits by 25.12.2012 17:30
	 * @param Array $array
	 * @return string|NULL
	 */
	public static function array_to_hash($array = null, $static = 0)
	{
		//������ ������ ����
		self::array_hash_gc();
		
		if (count($array))
		{
			
			$hash = sha1(serialize($array));
			$CI =& get_instance();
			$CI->load->model('model_common');
			
			$is_hash_exist = $CI->model_common->select_one("array_hash", array("hash" => $hash));
			if (empty($is_hash_exist))
			{
				$CI->model_common->insert("array_hash", array("hash" => $hash, "serialize" => serialize($array), "static" => (int)$static));
			}
			
			return (string)$hash;
			
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * ���������� ������� �����
	 */
	public static function array_hash_gc()
	{
		
		$CI =& get_instance();
		$CI->model_common->array_hash_gc();
		
	}
	
	/**
	 * ���� ��� � ����, ������� � ���������� ��������� �������
	 * @author ashmits by 25.12.2012 17:32
	 * @param Sha1 String $hash
	 * @return Array|NULL
	 */
	public static function hash_to_array($hash = null)
	{

		if ($hash == "null")
		{
			return false;
		}
		
		$CI =& get_instance();
		$CI->load->model('model_common');
		$hash_item = $CI->model_common->select_one("array_hash", array("hash" => $hash));
		
		if (!empty($hash_item['serialize']))
		{
			return @unserialize($hash_item['serialize']);
		}
		
		if (!empty($hash) and empty($hash_item))
		{
			show_error("�������� ��������� ������");
		}
		
		return null;
		
	}
	
	
	public static function is_sha1($str)
	{
		return (bool) preg_match('/^[0-9a-f]{40}$/i', $str);
	}
	
	
	public static function _split_text($s, $len = 10)
	{
		
		$marker = "\x01";
		$hyp = " ";
	
		$wordmaxlen = $len;
	
		// ��������� ��� ���� ����� ������� �� �� ��������
		preg_match_all('/(<.*?\>)/si',$s,$tags);
	
		// �������� ��� ���� �� �������
		$s =  preg_replace('/(<.*?\>)/si', $marker, $s);
	
		// ��������� ����� �� �����
		$words = split(' ',$s);
	
		for ($i=0; $i<count($words); $i++)
		{
		// ������ ����� >= $wordmaxlen ���������
		if (strlen($words[$i])>=$wordmaxlen)
			$words[$i] = chunk_split($words[$i],$wordmaxlen,$hyp);
	
		}
	
		// �������� ������ �� ��� �������� �� ����� ����
		$s = implode(' ',$words);
	
		// ��������������� ����, ����� ������� ���� �������� ���������
		for ($i=0; $i<count($tags[1]); $i++)
		$s =  preg_replace("/$marker/si", $tags[1][$i], $s, 1);
	
		return $s;
		
	}
	
	
	public static function _text_format($word, $fl) 
	{
		
		$up_letter   = '�����Ũ��������������������������';
		$down_letter = '��������������������������������';
		
		if($fl == '1')
		{ 
			return trim(strtr($word, $down_letter, $up_letter));
		}
		
	 	return trim(strtr($word, $up_letter, $down_letter));
	 	
	}	
	
	
	public static function _text_validation($text = "")
	{
		
		$text[0] = strtoupper($this->_text_format($text[0], 1));
	
		$text = trim($text);
		foreach (array(";", ":", ",", ".", "!", "?") as $val)
		{
			$text = str_replace(" $val ", "$val ", $text);
		}
	
		foreach (array(",", ".") as $val)
		{
			$text = str_replace(" $val", "$val ", $text);
		}
	
		// �������� ' �� `
		$text = str_replace("'", "`", $text);
	
		// ������ ���� ����� �� ���������
		$text = str_replace("..", "...", $text);
	
		// �������� ��� ������� ������������� ����� 3� ��� �� 3� ����������
		preg_match_all("/[?!\.,]{3,}/is", $text, $match);
	
		foreach ($match[0] as $key => $val)
		{
			$text = str_replace($val, substr($val, 0, 3), $text);
		}
	
		// ���������� ������� ����� ���� ������ ����������
		// ���� ��� �� , : ; �� ��������� ����������� ����� � ������� �������
		$match = "";
		preg_match_all("/([�-���-�])(\.|!|\?|,|:|;|\.\.\.)([�-���-�])/is", $text, $match);
		if (!empty($match))
		{
			foreach ($match[0] as $key => $val)
			{
				$flatter = $match[1][$key];
				$sumb = $match[2][$key];
				$slatter = $match[3][$key];
	
				if ($match[0][$key] == "�.�" or $match[0][$key] == "�.�") continue;
	
				if ($sumb != "," and $sumb != ":" and $sumb != ";")
				{
					$slatter = strtoupper($this->_text_format($slatter, 1));
				}
	
				$text = str_replace($match[0][$key], $flatter.$sumb." ".$slatter, $text);
			}
		}
		
		// ����������� ����
		// �������� �������� ���� (�)
		$text = str_replace("�", "�", $text);
		// �������� �����, ��������� ��������� ( - );
		$text = str_replace(" - ", " � ", $text);
		$text = str_replace(" -", " � ", $text);
		$text = str_replace("--", "�", $text);
		// ����� � ������ ������ (������ ����);
		if ($text[0] == "-") $text[0] = "�";
		$text = str_replace("\n- ", "\n�", $text);
		$text = str_replace("\n - ", "\n�", $text);
		$text = str_replace("\n\t- ", "\n\t�", $text);
		
		// ������ ���������� ������ ��� ��������� �� ������� ���������
		return  $text;
		
	}
	
	
	//�������������� ������� ���� � �����������
	public static function rus_to_translit($s)
	{
		
		$s=trim(strtolower($s));
		$s = preg_replace("/[^�-��-�a-zA-Z0-9 +]/ies", '', $s);
		$s=str_replace("�", "a",$s); $s=str_replace("�", "b",$s);
		$s=str_replace("�", "v",$s); $s=str_replace("�", "g",$s);
		$s=str_replace("�", "d",$s); $s=str_replace("�", "e",$s);
		$s=str_replace("�", "yo",$s); $s=str_replace("�", "zh",$s);
		$s=str_replace("�", "z",$s); $s=str_replace("�", "i",$s);
		$s=str_replace("�", "j",$s); $s=str_replace("�", "k",$s);
		$s=str_replace("�", "l",$s); $s=str_replace("�", "m",$s);
		$s=str_replace("�", "n",$s); $s=str_replace("�", "o",$s);
		$s=str_replace("�", "p",$s); $s=str_replace("�", "r",$s);
		$s=str_replace("�", "s",$s); $s=str_replace("�", "t",$s);
		$s=str_replace("�", "u",$s); $s=str_replace("�", "f",$s);
		$s=str_replace("�", "h",$s); $s=str_replace("�", "ts",$s);
		$s=str_replace("�", "ch",$s); $s=str_replace("�", "sh",$s);
		$s=str_replace("�", "shch",$s); $s=str_replace("�", "",$s);
		$s=str_replace("�", "y",$s); $s=str_replace("�", "",$s);
		$s=str_replace("�", "e",$s); $s=str_replace("�", "yu",$s);
		$s=str_replace("�", "ya",$s); $s=str_replace(" ", "_",$s);
		
		$s=str_replace("�", "a",$s); $s=str_replace("�", "b",$s);
		$s=str_replace("�", "v",$s); $s=str_replace("�", "g",$s);
		$s=str_replace("�", "d",$s); $s=str_replace("�", "e",$s);
		$s=str_replace("�", "yo",$s); $s=str_replace("�", "zh",$s);
		$s=str_replace("�", "z",$s); $s=str_replace("�", "i",$s);
		$s=str_replace("�", "j",$s); $s=str_replace("�", "k",$s);
		$s=str_replace("�", "l",$s); $s=str_replace("�", "m",$s);
		$s=str_replace("�", "n",$s); $s=str_replace("�", "o",$s);
		$s=str_replace("�", "p",$s); $s=str_replace("�", "r",$s);
		$s=str_replace("�", "s",$s); $s=str_replace("�", "t",$s);
		$s=str_replace("�", "u",$s); $s=str_replace("�", "f",$s);
		$s=str_replace("�", "h",$s); $s=str_replace("�", "ts",$s);
		$s=str_replace("�", "ch",$s); $s=str_replace("�", "sh",$s);
		$s=str_replace("�", "shch",$s); $s=str_replace("�", "",$s);
		$s=str_replace("�", "y",$s); $s=str_replace("�", "",$s);
		$s=str_replace("�", "e",$s); $s=str_replace("�", "yu",$s);
		$s=str_replace("�", "ya",$s);
		
		return $s;
		
	}
	
	
	public static function _send_email($from, $to, $subject, $html_text, $text = '')
	{
		
		$this->load->library('email');
		
		$config['mailtype'] = 'html';
		$config['priority'] = '2';
		$config['charset'] = "Windows-1251";
		$this->email->initialize($config);
		
		$this->email->from($from, $from);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($html_text);
		
		$this->email->send();
		
	}
	
	
	/*
	 * ������� �������� ������� ������� ��������� � ������� ��� �������
	* ������� �������� ��������� "..."
	* ���������:
	* $annotation - ���������, ������� ��������� ��������
	* $len - ����� ���������, ������� ��������� ��������
	* ������������ �������� - ����������� ���������.
	*/
	public static function _cut_long_annotation($annotation, $len)
	{
		
		$CI =& get_instance();
		$CI->load->helper('my_text');
		return My_Text_Helper::getInstance()->_cut_long_annotation($annotation, $len);
		
	}
	
	
	public static function _declension($n,$string=array('����','���','����'))
	{
		
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $string[2];
		if ($n1 > 1 && $n1 < 5) return $string[1];
		if ($n1 == 1) return $string[0];
		return $string[2];
		
	}
	
	
	public static function _days_to_str($days)
	{
		
		return $days." ". self::_declension($days);
		
	}
	
	
	// ���� ������ $date_str �������� � ���� ����� ������ ����������� � ������ -,
	// �� ������� �������� ��� �� ������������� �������� ������.
	// � �������, ����� ������� �� ����� ���� 1 ��� 2009 ����.
	// �������� ���� �� ���� � ���� 1 -5- 2009 � ������������ ���� ��������
	public static function _convert_month_in_str($date_str)
	{
		$date_array = array("-01-" => "������", "-02-" => "�������", "-03-" => "�����", "-04-" => "������", "-05-" => "���", "-06- "=> "����", "-07-" => "����", "-08-" => "�������", "-09-" => "��������", "-10-" => "�������", "-11-" => "������", "-12-" => "�������");
	
		return strtr($date_str, $date_array);
	}
	
	
	// ������� ���������� ip ������������
	public static function _get_real_ip()
	{
		
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
		
	}
	
	
	/**
	 * ���������� ��������� � ������������� ������
	 * @param unknown_type $arr
	 * @return number
	 */
	public static function array_push_associative(&$arr)
	{
		
		$ret=0;
		$args = func_get_args();
		foreach ($args as $arg) {
			if (is_array($arg)) {
				foreach ($arg as $key => $value) {
					$arr[$key] = $value;
					$ret++;
				}
			}else{
				$arr[$arg] = "";
			}
		}
		
		return $ret;
		
	}
	
	
}

/**
 * 
 * ��������������� ����� ��� ������� � ������
 * @author ashmits by 19.09.13 11:40
 *
 */
/*class Custom{
	
	public $param = array();
	
	public function __construct(){}
	
	public function __set($key, $val)
	{
		$this->param[$key] = $val;		
	}
	
	public function __get($name)
	{
		$name = strtolower($name);
		if (isset($this->param[$name]))
			return $this->param[$name];
		return null;
	}
	
}*/

?>