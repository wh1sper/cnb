<?php 
/**
 * 
 * ������ ���������
 * @author ashmits by 23.07.13
 *
 */
final class My_History_Helper
{
	
	protected static $instance;
	
	public static function getInstance()
	{
		
		if (is_null(self::$instance))
		{
			$class = __CLASS__;
			self::$instance = new $class;
		}
		
		return self::$instance;
		
	}
	
	/**
	 * 
	 * �������� ����� ��������� � ��
	 * @param unknown_type $array
	 * @param unknown_type $history_change_id
	 */
	public static function insert_changes($array, $history_change_id)
	{
		$CI =& get_instance();
		$CI->load->model('model_common');
		$CI->model_common->insert('change_histories_serialize', array('change_id' => (int)$history_change_id, 'serialize' => serialize($array)));
	}
	
	
	/**
	 * 
	 * ����� ��������� �� ID �������
	 * ������� ��������� ������ ������� � ���������� �������, ��������� ��
	 * @param int $history_id
	 */
	public static function get_diff_by_change($history_id)
	{
		
		$CI =& get_instance();
		$CI->load->model('model_user');
		require_once ROOT . 'app/libraries/find_diff.php';
		
		//������� ����������� �������
		$history = self::get_history_by_id($history_id);
		
		if (!empty($history))
		{
			
			//��� �������
			$change_object = $history['change_object'];
			
			//������� ��� ���� �������� �������
			$cnf = $CI->config->item('change_objects');
			
			//���� � ������� ��� ������� �� ������� �������
			if (empty($cnf[$change_object]['table']))
				return false;
			
			//������� ������� ��������
			$table_name = $cnf[$change_object]['table'];
			
			//������� ���������� ������� ��������� ����� �������
			$history_before = self::get_before_history($history_id, $history['object_id'], $history['change_object']);
			
			$result = array();
			
			//���� ���� ���������� ������� ���������
			if (!empty($history_before))
			{
				
				$change = self::get_change_by_history($history_id);
				$change_before = self::get_change_by_history($history_before['change_id']);
				
				$change_date = $history['change_date'];
				$change_date_before = $history_before['change_date'];
				
				$change_user = $CI->model_user->get_user_by_id($history['change_user_id']);
				$change_user_before = $CI->model_user->get_user_by_id($history_before['change_user_id']);
				
				if (!empty($change) and !empty($change_before))
				{
					
					$array = @unserialize($change['serialize']);
					$array_before = @unserialize($change_before['serialize']);
					
					if (is_array($array) and is_array($array_before))
					{
						$array_diff = array_diff_assoc($array, $array_before);
						$array_diff2 = array_diff_assoc($array_before, $array);
					}
					
					if (!empty($array_diff))
					{
						
						foreach ($array_diff as $key=>$val)
						{
							
							if (isset($val))
							{
								
								if (preg_match_all("/^http:\/\/.*?(gif|png|jpg|jpeg)$/i", $val, $matches))
								{
									foreach ($matches[0] as $k=>$v)
									{
										$val = str_replace($matches[0][$k], "<img src=\"{$matches[0][$k]}\" />", $val);
									}
								}
								
							}

							if (isset($array_diff2[$key]))
							{
								
								if (preg_match_all("/^http:\/\/.*?(gif|png|jpg|jpeg)$/i", $array_diff2[$key], $matches))
								{
									foreach ($matches[0] as $k=>$v)
									{
										$array_diff2[$key] = str_replace($matches[0][$k], "<img src=\"{$matches[0][$k]}\" width='150' />", $array_diff2[$key]);
									}
								}
								
							}
							
							$value_after = $val;
							$value_before = isset($array_diff2[$key]) ? $array_diff2[$key] : null;
							
							/*if (isset($value_after) and isset($value_before) and preg_match("/(title|name|description|text)/", $key))
							{
								
									$from_text = mb_convert_encoding(iconv('windows-1251','utf-8', strip_tags($value_before)), 'HTML-ENTITIES', 'UTF-8');
									$to_text = mb_convert_encoding(iconv('windows-1251','utf-8', strip_tags($value_after)), 'HTML-ENTITIES', 'UTF-8');
									$diff_opcodes = FineDiff::getDiffOpcodes($from_text, $to_text);
									
									$diff_text_after = FineDiff::renderDiffToHTMLFromOpcodes($to_text, $diff_opcodes);
									$diff_text_after = mb_convert_encoding($diff_text_after, 'UTF-8', 'HTML-ENTITIES');
	
									$diff_text_before = FineDiff::renderDiffToHTMLFromOpcodes($from_text, $diff_opcodes);
									$diff_text_before = mb_convert_encoding($diff_text_before, 'UTF-8', 'HTML-ENTITIES');
									
									$diff_text_before = preg_replace("'<ins>.*?</ins>'is", "", $diff_text_before);
									$diff_text_after = preg_replace("'<del>.*?</del>'is", "", $diff_text_after);
								
									
									$value_after = html_entity_decode($diff_text_after);
									$value_before = html_entity_decode($diff_text_before);
							}*/
							
							$result[] = array(
								'column' => $key, 
								'column_comment' => $CI->model_common->get_column_comment($key, $table_name),
								'value_after' => $value_after,
								'value_before' => $value_before,
								'user_after' => $change_user,
								'user_before' => $change_user_before,
								'date_after' => $change_date,
								'date_before' => $change_date_before,
								'history' => $history );
							
						}
					}
					
					return (array)$result;
					
				}
				
			}
			
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * ������� ������� �� ID
	 * @param unknown_type $history_id
	 */
	public static function get_history_by_id($history_id)
	{
		$CI =& get_instance();
		return $CI->model_common->select_one('change_histories', array('change_id' => $history_id), '*');
	}
	
	/**
	 * 
	 * ������� ���������� �������
	 * @param int $change_id - ID ����������� �������
	 * @param int $object_id - ID ������� ���������
	 * @param string $change_object - ��� ������� ���������
	 */
	public static function get_before_history($change_id, $object_id, $change_object)
	{
		$CI =& get_instance();
		return $CI->model_common->select_one('change_histories', array('object_id' => $object_id, 'change_object' => $change_object, 'change_id <' => $change_id ), '*', 'change_id desc', 0, 1);
	}
	
	/**
	 * 
	 * ������� ��������� �� ID �������
	 * @param unknown_type $history_id
	 */
	public static function get_change_by_history($history_id)
	{
		$CI =& get_instance();
		return $CI->model_common->select_one('change_histories_serialize', array('change_id' => $history_id));
	}
	
	/**
	 * 
	 * ������� �������
	 * @param array $sections
	 */
	public static function get_sections($sections)
	{
		
		$CI =& get_instance();
		$CI->load->model('model_common');
		
		if (count($sections) > 0)
		{
			
			$result = $CI->model_common->select("sections", "section_id in (" . implode(",", array_keys($sections)) . ")");
			
			if (!empty($result))
			{
				$return = array();
				foreach ($result as $k=>$v)
				{
					$return[] = $v['section_name'];
				}
				
				return implode("<br />", $return);
				
			}
			
		}
		
		return null;
		
	}
	
	
}
