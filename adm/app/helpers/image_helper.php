<?php 
/**
 * ����������� ����� ������ � ����������
 * @author ashmits by 25.01.13 17:00
 *
 */
final class Image_Helper{
	
	protected static $instance;
	
	private function __construct(){}
	private function __clone(){}
	private function __wakeup(){}
	
	/**
	 * 
	 * ������� �����������
	 * @param string $image_src
	 * @param int $degress - ���� �������� ������ ������� �������
	 */
	public static function rotate_image($image_src, $degress = 270)
	{
		
		$image_info = self::getInstance()->get_image_info($image_src);
		
		switch ($image_info['mime'])
		{
			
			case "image/jpeg":
				//������� �����
				$source = imagecreatefromjpeg($image_src);
				//������������
				$rotate = imagerotate($source, $degress, 0);
				break;
			case "image/png":
				$source = imagecreatefrompng($image_src);
				$rotate = imagerotate($source, $degress, 0);
				break;
			
		}
		
    	//��������������� ��������
		switch ($image_info['mime'])
		{
			case 'image/jpeg':
				//����������
				imagejpeg($rotate, $image_src);
				break;
				
			case 'image/png':
				imagepng($rotate, $image_src);
				break;
		}
		
		//������� ����������
		imagedestroy($source);
		imagedestroy($rotate);
		
	}
	
	
	/**
	 * 
	 * ��������� ��������� ��������
	 * @param string $src
	 */
	public static function get_remote_image($src)
	{
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, (string)$src);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST , false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		$data = curl_exec($curl);
		
		return $data;
		
	}
	
	
	/**
	 * 
	 * ���� ��������
	 * @param string $image_origin
	 * @param array $crop_info
	 * @author ashmits by 18.07.2013 15:00
	 * 
	 */
	public static function crop_image($image_origin, $crop_info, $ratio_width = null, $ratio_height = null)
	{
		//��������� �������� �������� � tmp
		$image = self::getInstance()->download_image_by_src($image_origin);
		
		//������� ���� � ��������
		$image_info = self::getInstance()->get_image_info($image);
		
		//������� ���� � ���� ��������
		$file_info = pathinfo($image_origin);
		
		$ext = $file_info['extension'];
		$filename = $file_info['filename'];
		
		//��������� ��� �����
		if (!empty($ratio_width) and !empty($ratio_height))
			$crop_file_name = $filename . "-crop_" . $ratio_width . "-" . $ratio_height . "." . $ext;
		else 
			$crop_file_name = $filename . "-crop_" . $crop_info['w'] . "-" . $crop_info['h'] . "." . $ext;
		//��������� path ���� �����
		$image_destination = ROOT . '/files/proxy_tmp/' . $crop_file_name;
		
		switch ($image_info['mime'])
		{
			
			case "image/jpeg":
				$source = imagecreatefromjpeg($image);
				break;
			case "image/png":
				$source = imagecreatefrompng($image);
				break;
			
		}
		
		
		//������� ����� ������ ��������
		if (!empty($ratio_width) and !empty($ratio_height))
			$thumb = imagecreatetruecolor($ratio_width, $ratio_height);
		else 
			$thumb = imagecreatetruecolor($crop_info['w'], $crop_info['h']);
		
		//������ ����� ��� � ��������
		//$white = imagecolorallocate($thumb, 255, 255, 255);
		$grey = imagecolorallocate($thumb, 244, 244, 244);
		
		//��������� �����
		imagefill($thumb, 0, 0, $grey);
		
		//����������� ���� ��������
		//���� ������������ ��� ���������� ������
		if (!empty($ratio_width) and !empty($ratio_height))
			imagecopyresampled($thumb, $source, 0, 0, $crop_info['x'], $crop_info['y'], $ratio_width, $ratio_height, $crop_info['w'], $crop_info['h']);
		else 
			imagecopyresampled($thumb, $source, 0, 0, $crop_info['x'], $crop_info['y'], $crop_info['w'], $crop_info['h'], $crop_info['w'], $crop_info['h']);
		
		imagejpeg($thumb, $image_destination, 90);
		
		@chmod($image_destination, 0777);
		
		imagedestroy($source);
		
		imagedestroy($thumb);
		
		//������ ����-�������� �� ������, � �� �� �����, ��� � ��������
		self::getInstance()->upload_image_by_src($file_info['dirname'], $image_destination);
		
		//������� ���� �������� �� tmp
		@unlink($image_destination);
		//������� �������� �� tmp
		@unlink($image);
		
		//������� src ����-��������
		return (string) $file_info['dirname'] . "/" . basename($image_destination);
		
	}
	
	
	/**
	 * 
	 * �������� �������� �� src
	 * @param unknown_type $dirname
	 * @param unknown_type $image_destination
	 */
	public static function upload_image_by_src($dirname, $image_destination)
	{
		//���� ftp ������
		if (preg_match("/^http:\/\/filearchive\.cnews\.ru\//i", $dirname))
		{
			$CI =& get_instance();
			$CI->load->library('ftp');
			$CI->ftp->connect($CI->config->item('ftp'));
			$path = str_replace(FILEARCHIVE_DOMAIN, "/", $dirname);
			$CI->ftp->upload($image_destination, $path . "/" . basename($image_destination));
			$CI->ftp->chmod($path . "/" . basename($image_destination), 0777);
			$CI->ftp->close();
		}
		//���� ��������� ������
		else 
		{
			$path = str_replace(DOMAIN, ROOT, $dirname);
			$data = file_get_contents($image_destination);
			file_put_contents($path . "/". basename($image_destination), $data);
			@chmod($path . "/". basename($image_destination), 0777);
		}
		
	}
	
	/**
	 * 
	 * ��������� �������� �� src
	 * @param string $image_src
	 */
	public static function download_image_by_src($image_src)
	{
		
		$image_name = self::getInstance()->get_filename_by_src($image_src);
		$data = self::getInstance()->get_remote_image($image_src);
		file_put_contents(ROOT . '/files/proxy_tmp/' . $image_name, $data);
		@chmod(ROOT . '/files/proxy_tmp/' . $image_name, 0777);
		
		return ROOT . '/files/proxy_tmp/' . $image_name;
	}
	
	//singletone
	public static function getInstance()
	{
		
		if (is_null(self::$instance))
		{
			$classname = __CLASS__;
			self::$instance = new $classname;
		}
		
		return self::$instance;
		
	}
	
	/**
	 * 
	 * ������ ��������� �� ���������
	 * @param unknown_type $image_origin
	 */
	public static function set_resized_image_by_origin($image_origin)
	{
		
		$CI =& get_instance();
		$resized_images = array();
		
		//�������� ��� ��������� ��������
		$image_resized = $CI->config->item('resized_params');
		
		foreach ($image_resized as $key=>$val)
		{
			$image_destination = self::get_preview_full_name($image_origin, $key);
			if (file_exists($image_destination))
				@unlink($image_destination);
			self::image_resize($image_origin, $image_destination, $val['width'], $val['height'], $val['crop'], $val['fixed_size']);
			@chmod($image_destination, 0777);
			$resized_images[] = $image_destination;
		}

		return (array)$resized_images;
		
	}
	
	
	/**
	 * ������� ���� <img src="..." /> � ������ � ���������� ������ ��������, 
	 * ���� ������ ��������, �� ��������� � ��� width, height
	 * @param String $text
	 * @return NULL|mixed
	 */
	public static function set_images_in_text($text="")
	{
		
		if (trim($text) == "")
		{
			return null;
		}
		
		//������� ��� ���� img
		if (preg_match_all("'<img([^\>]*)>'is", $text, $matches))
		{

			foreach ($matches[1] as $key=>$item)
			{
				//������� ������� src
				if (preg_match("/src=(\'|\")([^(\'|\")]*)(\'|\")/i", $item, $match))
				{
					if (!empty($match[2]))
					{
						$src = (string)$match[2];
						$image_info = self::get_image_info($src);
						if ($image_info['width'] > MAX_WIDTH_IMAGE or $image_info['height'] > MAX_HEIGHT_IMAGE)
						{
							
							
							if ($image_info['width'] > MAX_WIDTH_IMAGE)
							{
								$item = preg_replace("/width=(\'|\")([^(\'|\")]*)(\'|\")/i", "", $item);
								$item .= " width = '" . MAX_WIDTH_IMAGE . "'";
							}
							elseif ($image_info['height'] > MAX_HEIGHT_IMAGE)
							{
								$item = preg_replace("/height=(\'|\")([^(\'|\")]*)(\'|\")/i", "", $item);
								$item .= " height = '" . MAX_HEIGHT_IMAGE . "'";
							}
							
							$text = str_replace($matches[0][$key], "<p class='popup_img'><a href='$src' class='popup_img'><img" . $item . "></a></p>", $text);
						}
					}
				}
			}
		}
		
		return $text;
		
	}
	
	
	/**
	 * 
	 * ������� ��� ���������
	 * @param CHAR $image_name
	 * @param CHAR $method
	 */
	public static function get_preview_name($image_name, $method)
	{
		$array = explode('.', $image_name);
		$ext = $array[count($array)-1];
		unset($array[count($array)-1]);
		$name = implode('', $array);
		return $name . "_" . $method . "." . strtolower($ext);
	}

	public static function get_preview_full_name($image_name, $size)
	{
		$preview_name = preg_replace("/\.(gif|png|jpg|jpeg)$/i", "_{$size}.$1", $image_name);
		return $preview_name;
	}
	
	/**
	 * 
	 * ������ �������� GD2
	 * @param INT $image_origin
	 * @param INT $method
	 */
	public static function get_resized_image($image_origin, $method)
	{
		
		$filename = Image_Helper::getInstance()->get_filename_by_src($image_origin);
		$preview_name = Image_Helper::getInstance()->get_preview_name($filename, $method);
		
		$segmetns = explode("/", $image_origin);
		unset($segmetns[count($segmetns)-1]);
		
		$image_path = implode('/', $segmetns);
		
		//���� ��������� ��� �� �������, ��������
		if (!file_exists(ROOT . $image_path . "/" . $preview_name))
		{
			
			$conf =& get_config();
			
			//���� ������� ������� ���� � �������
			if (isset($conf['resized_params'][$method]) and $array = $conf['resized_params'][$method])
			{
				//��������
				Image_Helper::getInstance()->image_resize(ROOT . $image_origin, ROOT . $image_path . "/" . $preview_name, $array['width'], $array['height'], $array['crop'], $array['fixed_size']);
			}
			else
			{
				Show_Errors::show_404_error("����� �������� �� ����������");
			}
			
		}
		
		$image_info = Image_Helper::getInstance()->get_image_info(ROOT . $image_path . "/" . $preview_name);
		
		if ($image_info)
		{
			
			header('Content-type: ' . $image_info['mime']);
			@readfile(ROOT . $image_path . "/" . $preview_name);
			
		}
		
	}
	
	/**
	 * 
	 * ������� ��� ��������� ��������� �� ��������� �������� � ��� �� �����
	 * @param CHAR $image_origin - ������ ���� �� ���������
	 */
	public static function get_previews_by_origin($image_origin)
	{
		
		if (file_exists($image_origin))
		{
			$previews = array();
			$image_name = self::get_filename_by_src($image_origin);
			if (preg_match("/(.*)?\.(jpg|jpeg|png|gif)$/i", $image_name, $matches))
			{
				
				$current_dir = dirname($image_origin);
				$handle = opendir($current_dir);
				while(false !== ($file = readdir($handle)))
				{
					$pattern = sprintf("/^%s_\d{1,}x\d{1,}\.%s/i", $matches[1], $matches[2] );
					if (preg_match($pattern, $file))
					{
						$previews[] = $current_dir . "/" . $file;
					}
				}
			}
			
			return $previews;
			
		}
		
		return null;
		
	}
	
	/**
	 * ���������� � ��������
	 * @param String $src
	 * @return multitype:unknown |NULL
	 */
	public static function get_image_info($src="")
	{
		if ($info = @getimagesize($src))
		{
			return array(
					"width" => $info[0],
					"height" => $info[1],
					"type" => $info[2],
					"mime" => $info['mime']
					);
		}
		
		return null;
		
	}
	
	public static function get_image_type($src)
	{
		if ($image = self::get_image_info($src))
		{
			return $image['type'];
		}
	}
	
	/**
	 * 
	 * ������� �������� ����� � ������ ����
	 * @param CHAR $src
	 */
	public static function get_filename_by_src($src="")
	{
		if (trim($src) != "")
		{
			$src_array = explode("/", $src);
			if (!empty($src_array[count($src_array)-1]))
			{
				return (string)$src_array[count($src_array)-1];
			}
		}
	}
	
	/**
	 * 
	 * ������� ������ ��������
	 * @param CHAR $image_origin - �������� ��������
	 * @param CHAR $image_destination - �������� ������
	 * @param INT $width - ����� ������
	 * @param INT $height - ����� �����
	 * @param BOOL $crop - �������� ��� ������ ������
	 * @param BOOL $fixed_size - ���������� ��������� ���������, ��� ���������
	 * @author ashmits by 28.05.2013 12:10
	 */
	public static function image_resize($image_origin, $image_destination, $width, $height, $crop = false, $fixed_size = true)
	{
		
		$image_info = Image_Helper::getInstance()->get_image_info($image_origin);
		
		if ( $image_info['width'] <= 0 or $image_info['height'] <= 0 or $width <= 0 or $height <= 0 )
		{
			return false;
		}
		
		$dst_x = 0;
		$dst_y = 0;
		$src_x = 0;
		$src_y = 0;
		$i_height = $image_info['height'];
		$i_width = $image_info['width'];
		
		//���� �� ��������, �� �������� ����� ������, ���� ��������� ������ �������
		if (!$crop)
		{
			
			//���� ������������� ������, �� �������� �� ������� ������ ��� �����
			if ($fixed_size)
			{
				//������� �� ������� ������
				if ($image_info['width'] <= $image_info['height'])
				{
					
					//�������� ������ ��������� � ����������� � �������
					$i_width = (int)($image_info['width'] * $height) / $image_info['height'];
					$i_height = $height;
					//����� ��������� ���������� X ����� ��������� �� ������
					$dst_x = (int) ($width - $i_width) / 2;
					
				}
				//���� �� ������� ������
				else 
				{
					
					//������� ������ ��������� � ����������� � �������
					$i_height = (int) ($image_info['height'] * $width) / $image_info['width'];
					$i_width = $width;
					//��������� ����������� Y �� ������ ����� ���������
					$dst_y = (int) ($height - $i_height) / 2;
					
				}
			}
			
			/*//����� �������� �� ������� ������ ��� �����
			else
			{
				//���� ������ ������ ������
				if ($image_info['width'] <= $image_info['height'])
				{
					
					//������� �������� ������ � ����������� � �������� ���������
					$i_height = (int)($width * $image_info['height']) / $image_info['width'];
					$i_width = $width;
					$height = $i_height;
					
				}
				else
				{
					
					$i_width = (int)($height * $image_info['width']) / $image_info['height'];
					$i_height = $height;
					$width = $i_width;
					
				}
							
			}*/
			
			
			//����� �������� �� ������� �������
			else
			{
				//���� ������ ������ ������, �������� �� ������� ������
				if ($image_info['width'] >= $image_info['height'])
				{
					
					//������� �������� ������ � ����������� � �������� ���������
					$i_height = (int)($width * $image_info['height']) / $image_info['width'];
					$i_width = $width;
					$height = $i_height;
					
				}
				//�������� �� ������� ������
				else
				{
					
					$i_width = (int)($height * $image_info['width']) / $image_info['height'];
					$i_height = $height;
					$width = $i_width;
					
				}
							
			}
			
			
		}
		//���� ��������, �� �������� �� ������� �������
		else
		{

			if ($image_info['width'] <= $image_info['height'])
			{
				
				$i_height = (int) ($image_info['width'] * $height) / $width;
				$src_y = (int) (($image_info['height'] - $i_height) / 2);
				$image_info['height'] = $i_height;
				$i_height = $height;
				$i_width = $width;
				
			}
			else
			{
				
				$i_width = (int) ($image_info['height'] * $width) / $height;
				$src_x = (int) ($image_info['width'] - $i_width) / 2;
				$image_info['width'] = $i_width;
				$i_width = $width;
				$i_height = $height;
				
			}

		}
		
		switch ($image_info['mime'])
		{
			
			case "image/jpeg":
				$source = imagecreatefromjpeg($image_origin);
				break;
			case "image/png":
				$source = imagecreatefrompng($image_origin);
				break;
			
		}
		
		//������� ����� ������ ��������
		$thumb = imagecreatetruecolor($width, $height);
		
		//������ ����� ��� � ��������
		//$white = imagecolorallocate($thumb, 255, 255, 255);
		$grey = imagecolorallocate($thumb, 242, 242, 242);
		
		//��������� �����
		imagefill($thumb, 0, 0, $grey);
		
		//����������� ��������� ��������
		imagecopyresampled($thumb, $source, $dst_x, $dst_y, $src_x, $src_y, $i_width, $i_height, $image_info['width'], $image_info['height']);
		
		imagejpeg($thumb, $image_destination, 90);
		
		@chmod($image_destination, 0777);
		
		imagedestroy($source);
		
		imagedestroy($thumb);
		
	}
	
	/**
	 * 
	 * ���� ��������-���������
	 * @param string $name - ����� ��������
	 */
	public static function is_image_preview($name)
	{
		
		$name = self::get_filename_by_src($name);
		
		if (preg_match("/\d{1,4}x\d{1,4}\.(gif|png|jpg|jpeg)$/", $name))
			return true;
		
		return false;
			
	}
	
}

?>