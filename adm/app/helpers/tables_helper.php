<?php 
/**
 * 
 * ����� ������ � ���������
 * @author ashmits
 *
 */
class Tables_Helper
{
	
	/**
	 * ������� ������ �� �����
	 * @param String $text
	 * @return String
	 * @author ashmits
	 * ������� ���� ������ � ������ � �������� �� �� �������
	 * ������ �����: {table id="(:num)"[ class="class_name"]}
	 */
	public static function get_tables_by_tags($text)
	{
		
		$CI =& get_instance();
		$CI->load->library('tables_views');
		if (preg_match_all('/{table\s{1,}id="([0-9]+)"(\s{1,}class="([a-zA-Z0-9_-\s]+)")?}/i', $text, $tags))
		{
			
			foreach ($tags[1] as $key => $table_id)
			{
				$tag = $tags[0][$key];
				
				$custom_class = null;
				if (!empty($tags[3][$key]))
				{
					$custom_class = (string)$tags[3][$key];
				}
				
				$table = $CI->tables_views->get_table_by_id($table_id, 1);
				$code = "";
				
				if (!empty($table))
				{
					
					if (!empty($table['structure']))
					{
						//���� ���� ��������� � �������
						if (!empty($table['table_preview_id']))
						{
							//������� ��������� � ����������� �� ������ �������, �������� ������ �� ������� �������
							$table_small = $CI->model_common->select_one("tables", array("table_id" => (int)$table['table_preview_id']));
							if (!empty($table_small))
							{
								$table_code = self::table_configure_by_limit((string)$table_small['structure'], (string)$table['description'], 11, $custom_class);
								$CI->smarty->assign('table', $table_small);
								$CI->smarty->assign('table_code', $table_code);
								$CI->smarty->assign('table_hash', Common_Helper::getInstance()->array_to_hash(array("id" => $table_id)));
								$code = $CI->smarty->fetch('front/tables/table_single');
							}
							
						}
						else
						{
							$table_code = self::table_configure_by_limit((string)$table['structure'], (string)$table['description'], 11, $custom_class);
							$CI->smarty->assign('table', $table);
							$CI->smarty->assign('table_code', $table_code);
							$CI->smarty->assign('table_hash', Common_Helper::getInstance()->array_to_hash(array("id" => $table_id)));
							$code = $CI->smarty->fetch('front/tables/table_single');
						}
						
					}
				}
				
				$text = str_replace($tag, $code, $text);
				
			}
			
		}
		
		$text = preg_replace('/{table\s{1,}id="([0-9]+)"}/i', "", $text);
		
		return (string)$text;
	}
	
	
	/**
	 * �������� ������� � ����������� ����
	 * @param String $table_structure
	 * @param String $description
	 * @return string
	 */
	public static function set_structure_table($table_structure, $description = null)
	{
		$config =& get_config();
		$tbl_config = $config['excel_to_table'];
		
		$rows = array();
		
		if (preg_match_all("'<tr.*?>.*?</tr>'si", $table_structure, $matches))
		{
			foreach ($matches[0] as $key=>$val)
			{
				if ($key == 0)
				{
					$val = preg_replace("'<tr.*?>'is", $tbl_config['heading_row_start'], $val);
					$val = preg_replace("'</tr>'is", $tbl_config['heading_row_end'], $val);
				}
				else
				{
					$val = preg_replace("'<tr.*?>'is", $tbl_config['row_start'], $val);
					$val = preg_replace("'</tr>'is", $tbl_config['row_end'], $val);
					
					if ($key%2 == 0)
					{
						$val = preg_replace("'<tr(.*?)>'is", "<tr$1 class='even'>", $val);
					}
					else
					{
						$val = preg_replace("'<tr(.*?)>'is", "<tr$1 class='odd'>", $val);
					}
					
				}
				
				$rows[] = $val;
				
			}
		}
		
		$rows = preg_replace("/\n/i", "", $rows);
		$rows = preg_replace("'<tr(.*?)>'is", "\n   <tr$1>", $rows);
		$rows = preg_replace("'</tr>'is", "\n   </tr>", $rows);
		$rows = preg_replace("'<t(d|h)(.*?)>'is", "\n           <t$1$2>", $rows);
		
		if (count($rows))
		{
			$table_structure = "";
			$table_structure = $tbl_config['table_open'] . implode("",$rows) . $tbl_config['table_close'];
			$table_structure = preg_replace("'</table>'is", "\n</table>", $table_structure);
		}

		return (string)$table_structure;
		
	}
	
	
	
	/**
	 * �������� �������, ��� ������
	 * @param String $table_structure
	 * @param String $description
	 * @param Int $count_rows
	 * @param String $custom_class - ���������������� ����� �������, ����������� � ���������
	 * @return NULL|string
	 */
	public static function table_configure_by_limit($table_structure = null, $description = null, $count_rows = 11, $custom_class=NULL)
	{
		if (empty($table_structure))
			return null;
		
		$config =& get_config();
		$tbl_config = $config['excel_to_table'];
		$max_cols = 0;
		
		if (preg_match_all("'<tr.*?>.*?</tr>'si", $table_structure, $matches))
		{
			if (!empty($matches[0]))
			{
				
				$rows = $matches[0];
				foreach ($rows as $key=>$val)
				{
					
					
					if ($key == 0)
					{
						$val = preg_replace("'<tr.*?>'is", $tbl_config['heading_row_start'], $val);
						$val = preg_replace("'</tr>'is", $tbl_config['heading_row_end'], $val);
					}
					else
					{
						
						if (preg_match_all("'<td.*?>.*?</td>'", $val, $m_cols))
						{
							if (!empty($m_cols[0]))
							{
								if ($max_cols < count($m_cols[0]))
									$max_cols = count($m_cols[0]);
							}
						}
						
						$val = preg_replace("'<tr.*?>'is", $tbl_config['row_start'], $val);
						$val = preg_replace("'</tr>'is", $tbl_config['row_end'], $val);
						
						if ($key%2 == 0)
						{
							$val = preg_replace("'<tr(.*?)>'is", "<tr$1 class='even'>", $val);
							//$val = preg_replace("'<tr class=(\"\')[^(\'|\")]*(\'|\")>'is", "<tr class='$1 even'>", $val);
						}
						else
						{
							$val = preg_replace("'<tr(.*?)>'is", "<tr$1 class='odd'>", $val);
							//$val = preg_replace("'<tr class=(\"\')[^(\'|\")]*(\'|\")>'is", "<tr class='$1 odd'>", $val);
						}
						
					}
					
					//��������� ������ �� �������� � ������� ������� ��������� �����-������� � ������� <td class="column1">
					if (preg_match_all("'<t(d|h)(.*?)>(.*?)</t(d|h)>'si", $val, $columns))
					{
						$tds = array();
						foreach ($columns[0] as $ckey=>$cval)
						{
							
							if (!preg_match("/class=(\"|\')column\d{1,2}(\"|\')/i", $columns[1][$ckey].$columns[2][$ckey]))
							{
								$tds[] = "<t" . $columns[1][$ckey].$columns[2][$ckey]." class=\"column$ckey\">".$columns[3][$ckey]."</t".$columns[4][$ckey].">";
							}
							else
							{ 
								$tds[] = "<t" . $columns[1][$ckey].$columns[2][$ckey].">".$columns[3][$ckey]."</t".$columns[4][$ckey].">";
							}
							
						}
						
						$val = preg_replace("'<tr(.*?)>(.*?)</tr>'is", "<tr$1>" . implode("\n", $tds) . "</tr>", $val);
						
					}
					
					$rows[$key] = $val;
				}
				
				if (!empty($count_rows))
				{
					$rows = array_slice($rows, 0, $count_rows);
				}
				
				
				$rows = preg_replace("/\n/i", "", $rows);
				$rows = preg_replace("'<tr(.*?)>'is", "\n   <tr$1>", $rows);
				$rows = preg_replace("'</tr>'is", "\n   </tr>", $rows);
				$rows = preg_replace("'<t(d|h)(.*?)>'is", "\n           <t$1$2>", $rows);
				
			}
			
			
			$cols = $max_cols;
			
			$config =& get_config();
			$table = $config['excel_to_table']['table_open'];
			
			if (!empty($custom_class))
			{
				
				$table = preg_replace("/class=\"(.*)\"/", sprintf('class="$1 %s"', $custom_class), $table);
				
			}
			
			if (!empty($cols) and trim($description) != "")
			{
				$row = $tbl_config['title_row_start'].$tbl_config['title_td_start'].$description.$tbl_config['title_td_end'].$tbl_config['title_row_end'];
				$row = preg_replace("'<t(d|h)(.*?)>'", "<t$1$2 colspan=\"$cols\">", $row);
				$table .= $row;
			}
			$table .= implode("\n",$rows);
			$table .= "</table>";
			
			return $table;
		}
		
		return null;
		
	}
	
}

?>