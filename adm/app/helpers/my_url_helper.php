<?php 
/**
 * ������ � ������
 * @author ashmits
 *
 */
final class My_Url_Helper
{
	
	public static function redirect($url)
	{
		
		header("Location: " . $url);
		die();
		
	}
	
	/**
	 * 
	 * ���� ������� �������� ������ �� ������
	 */
	public static function referer_is_hash_exists()
	{
		
		$CI =& get_instance();
		if (preg_match("/\?hash=([^\&]+)\&?$/", $CI->input->server('HTTP_REFERER'), $matches))
			return $matches[1];
			
		return null;
		
	}
	
	public static function get_current_url()
	{
		
		return 'http://'. $_SERVER["HTTP_HOST"] . $_SERVER['QUERY_STRING'];
		
	}
	
	public static function get_from_url()
	{
		if (preg_match("/from=(.*)$/i", $_SERVER['REQUEST_URI'], $matches))
		{
			if (!empty($matches[1]))
			{
				return $matches[1];
			}
		}
	}
	
	public static function replace_change_type_url($url, $id)
	{
		
		if (preg_match("/(\d{1,})\:(\d{1,})/i", $id, $match))
		{
			
			$parent_entity_id = (int)$match[1];
			$object_id = (int)$match[2];
			$url = str_replace("#PARENT_ENTITY_ID", $parent_entity_id, $url);
			$url = str_replace("#ID", intval($object_id), $url);
			return $url;
			
		}
		
		return str_replace("#ID", intval($id), $url);
		
	}
	
	
	public static function get_view_admin_url($config_url, $change_object)
	{
		
		if (!empty($change_object['info']))
		{
			foreach ($change_object['info'] as $key=>$val)
			{
				$config_url = str_replace("{#".$key."}", $val, $config_url);
			}
		}
		
		return $config_url;
		
	}
	
	
	public static function validate_url($url)
	{
		
		$match = "/^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-‌​\.\?\,\'\/\\\+&amp;%\$#_]*)?$/i";
		
		if (preg_match($match, $url))
			return true;
		
		return false;
		
	}
	
	
	/**
	 * 
	 * ������
	 */
	public static function get_remote_url()
	{
		
		if (preg_match("/\?url=(.*)/", $_SERVER['REQUEST_URI'], $matches))
		{
			
			$url = $matches[1];
			
			$post = $_POST;
			
			//���� �������� ����
			if (!empty($_FILES['file']) or !empty($_FILES['Filedata']))
			{
				
				$name = !empty($_FILES['file']) ? 'file' : 'Filedata';
				$data = array();
				$file_name = $_FILES[$name]['name'];
				if ($array = explode(".", $file_name))
				{
					$file_ext = $array[count($array)-1];
					//���������� �����
					$file_ext = strtolower($file_ext);
					//������ ��������� ��� �����
					$file_name = md5($file_name . time());
					//��������� ����� �������� ����� � ������ path
					$full_path = sprintf("%s/files/proxy_tmp/%s.%s", (string)ROOT, (string)$file_name, (string)$file_ext);
					//������ �� ������
					if ( @move_uploaded_file($_FILES[$name]['tmp_name'], $full_path) )
					{
						$post = array_merge($post, array($name => '@' . $full_path));
					}
				}
				
			}
			
			//�������������� ����
			//�������� POST �� https � ������������ �� ��������� �������
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, (string)$url);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST , false);
			curl_setopt($curl, CURLOPT_USERPWD, "psycho:wE-[28!");
			
			if (preg_match("/\.(gif|jpg|jpeg|png)$/i", $url, $type))
			{
				
				header('Content-type: image/' . $type[1]);
				curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
				
			}
			else
			{
				
				if (preg_match("/\.js(\?rnd=([0-9]+))?$/i", $url, $type))
				{
					header('Content-type: text/javascript; charset=utf-8');
				}
				elseif (preg_match("/\.css(\?rnd=([0-9]+))?$/i", $url, $type))
				{
					header('Content-type: text/css; charset=utf-8');
				}
				elseif (preg_match("/\.swf/i", $url, $type))
				{
					header("content-type: application/x-shockwave-flash" );
				}
				else
				{
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				}
				
			}
			
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
			curl_setopt($curl, CURLOPT_VERBOSE, 1);
			curl_setopt($curl, CURLOPT_COOKIE, "proxy_domain=". DOMAIN ."/admin/ajax/get_remote_url/?url=;");
			
			$tuData = curl_exec($curl);
			//�������� ����� �������
			
			if (preg_match("/adm\.zoom\.cnews\.ru\/gallery/i", $url))
				$tuData = @iconv('utf-8','windows-1251', $tuData);
			
			print $tuData;
			
			//���� ��� �������� ���� ��� ������, ������� �� �� ��������� ����������
			if (!empty($full_path))
			{
				@unlink($full_path);
			}
			
		}
		
		
	}
	
}