<?php

/**
 * ������ ����������� ���������
 * @author ashmits by 21.01.2013 18:52
 *
 */

class My_Entities_Helper
{
	
	private static $CI;
	protected static $instance;
	private function __construct(){}
	private function __wakeup(){}
	private function __clone(){}
	
	
	public static function getInstance()
	{
		
		if ( is_null(self::$instance) )
		{
			$classname = __CLASS__;
			self::$instance = new $classname;
		}
		
		return self::$instance;
		
	}
	
	
	/**
	 * ��������� ����� ��������-��������
	 * @param Int $entity_id ID ����������� ��������
	 * @param Int $entity_in_id ID �������� � ������� �����������
	 * @param String $entity_type ��� table, image ...
	 * @param String $entity_in_type ��� reviews, reviews_articles ...
	 */
	public static function set_entity($entity_id, $entity_in_id, $entity_type, $entity_in_type)
	{
		
		$CI =& get_instance();
		$CI->load->model(array('model_common'));
		$CI->model_common->insert('entities_includes', array(
			'entity_id' => intval($entity_id),
			'entity_in_id'	=> intval($entity_in_id),
			'entity_type' => (string)$entity_type,
			'entity_in_type' => (string)$entity_in_type
			));
		
	}
	
	/**
	 * ������� ����������� ��������
	 * @param Int $entity_id
	 * @param String $entity_type
	 */
	public static function delete_by_entity($entity_id, $entity_type)
	{
		
		$CI =& get_instance();
		$CI->load->model(array('model_common'));
		$CI->model_common->delete("entities_includes", array("entity_id" => intval($entity_id), "entity_type" => (string)$entity_type));
		
	}

	/**
	 * ������� �� ID � ����
	 * @param Int $entity_in_id
	 * @param String $entity_in_type
	 */
	public static function delete_by_entity_in($entity_in_id, $entity_in_type)
	{
		
		$CI =& get_instance();
		$CI->load->model(array('model_common'));
		$CI->model_common->delete("entities_includes", array("entity_in_id" => intval($entity_in_id), "entity_in_type" => (string)$entity_in_type));
		
	}
	
	
	/**
	 * ������� ��� ���� {table id="(:num)"[class="class_name"]}
	 * @param String $text
	 * @return Array|Null
	 * @author ashmits by 22.01.2013 10:40
	 */
	public static function search_tables_entities($text)
	{
		
		return preg_match_all('/{table\s{1,}id="([0-9]+)"(\s{1,}class="([a-zA-Z0-9_-\s]+)")?}/i', $text, $matches) ? $matches[1] : null;
		
	}

	/**
	 * 
	 * ������� ��� ���� {video id="([0-9]+)"}
	 * @param unknown_type $text
	 * @author ashmits by 02.08.2013 17:21
	 */
	public static function search_videos_entities($text)
	{
		
		return preg_match_all('/{video\s{1,}id="([0-9]+)"}/i', $text, $matches) ? $matches[1] : null;
		
	}
	
	/**
	 * 
	 * ������� ��� ����������� {photogallery id="123"}
	 * @param string $text
	 */
	public static function search_galleries_entities($text)
	{
		return preg_match_all('/\{photogallery.+?id=(\'|\")([0-9]+)(\'|\")\}/i', $text, $matches) ? $matches[2] : null;
	}
	
	
	/**
	 * ������ ����� �� ������� � ������
	 * @param String $text
	 * @param Int $entity_in_id
	 * @param Int $entity_in_type
	 * @author ashmits by 22.01.2013 10:40
	 */
	public static function set_entities($text, $entity_in_id, $entity_in_type)
	{

		//������ ������������ ��������
		self::delete_by_entity_in($entity_in_id, $entity_in_type);
		
		//����������� �������
		$CI =& get_instance();
		$CI->load->library(array('tables_views'));
		$tables_ids = self::search_tables_entities($text);
		
		if (count($tables_ids))
		{
			foreach ($tables_ids as $key=>$table_id)
			{
				if ($CI->tables_views->get_table_by_id($table_id))
				{
					if (!self::search_entity_single((int)$table_id, 'tables', (int)$entity_in_id, (string)$entity_in_type))
					{
						self::set_entity((int)$table_id, (int)$entity_in_id, 'tables', (string)$entity_in_type);
					}
				}
			}
		}
		
		
		//����������� �����
		$CI->load->library(array('videos_views'));
		$videos_ids = self::search_videos_entities($text);
		
		if (count($videos_ids))
		{
			foreach ($videos_ids as $key=>$video_id)
			{
				if ($CI->videos_views->get_video_by_id($video_id))
				{
					if (!self::search_entity_single((int)$video_id, 'videos', (int)$entity_in_id, (string)$entity_in_type))
					{
						self::set_entity((int)$video_id, (int)$entity_in_id, 'videos', (string)$entity_in_type);
					}
				}
			}
		}
		
		
		//����������� �����������
		$CI->load->library(array('gallery_views'));
		$galleries_ids = self::search_galleries_entities($text);
		
		if (count($galleries_ids))
		{
			foreach ($galleries_ids as $key => $gallery_id)
			{
				if ($CI->gallery_views->get_gallery_by_id($gallery_id))
				{
					if (!self::search_entity_single((int)$gallery_id, 'gallery', (int)$entity_in_id, (string)$entity_in_type))
					{
						self::set_entity((int)$gallery_id, (int)$entity_in_id, 'gallery', (string)$entity_in_type);
					}
				}
			}
		}
		
		
	}
	
	/**
	 * 
	 * ������� ��������
	 * @param ID $entity_id
	 * @param ��� $entity_type
	 */
	public static function get_entity($entity_id, $entity_type)
	{
		
		$CI =& get_instance();
		$CI->load->model('model_common');
		$entity = $CI->model_common->select_one("entities_includes", array("entity_id" => intval($entity_id), "entity_type" => (string)$entity_type));
		if (!empty($entity))
		{
			return self::set_structure_entities($entity);
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * ����� ����������� ���������
	 * @param unknown_type $entity_in_id
	 * @param unknown_type $entity_in_type
	 */
	public static function get_entities_by_entity_in($entity_in_id, $entity_in_type)
	{
		
		$CI =& get_instance();
		$CI->load->model('model_common');
		$entities = $CI->model_common->select("entities_includes", array("entity_in_id" => intval($entity_in_id), "entity_in_type" => (string)$entity_in_type));
		if (!empty($entities))
		{
			return self::set_structure_entities($entities);
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * ����� ��������� ��������� � ���� �������� (�������, �����, �������)
	 * @param unknown_type $entity_id - ID �������, �����, �������
	 * @param unknown_type $entity_type - ���: �������|�����|�������
	 */
	public static function get_entities_in_by_entity($entity_id, $entity_type)
	{
		
		$CI =& get_instance();
		$CI->load->model('model_common');
		$entities_in = $CI->model_common->select("entities_includes", array("entity_id" => intval($entity_id), "entity_type" => (string)$entity_type));
		if (!empty($entities_in))
		{
			return self::set_structure_entities($entities_in);
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * ��������� ��������� ������������ ���������
	 * @param array[] $entities_array
	 */
	public static function set_structure_entities($entities_array)
	{
		
		if (is_array($entities_array))
		{
			
			$config = get_config();
			$CI =& get_instance();
			$CI->load->helper('my_url');
			//������ ��������� �� �������
			$items = $config['change_objects'];
			foreach ($entities_array as $key=>$val)
			{
				
				if (isset($items[$val['entity_type']]))
				{
					
					$entities_array[$key]['entity'] = $items[$val['entity_type']];
					//������� ������ �� ������������ �������� (�������, �������, �����)
					if (isset($items[$val['entity_type']]['table']) and isset($items[$val['entity_type']]['title_column']) and isset($items[$val['entity_type']]['id_column']))
					{
						$entities_array[$key]['entity']['title'] = $CI->model_common->select_cell($items[$val['entity_type']]['table'],	array($items[$val['entity_type']]['id_column'] => $entities_array[$key]['entity_id']), is_array($items[$val['entity_type']]['title_column']) ? $items[$val['entity_type']]['title_column'][0] : $items[$val['entity_type']]['title_column']);
					}
					
					$entities_array[$key]['entity_in'] = $items[$val['entity_in_type']];
					//������� ������ �� �������� (�������, ������, �����, ...), � ������� ���������� �������� (�������, �������, �����)
					if (isset($items[$val['entity_in_type']]['table']) and isset($items[$val['entity_in_type']]['title_column']) and isset($items[$val['entity_in_type']]['id_column']))
					{
						$entities_array[$key]['entity_in']['title'] = $CI->model_common->select_cell($items[$val['entity_in_type']]['table'], array( $items[$val['entity_in_type']]['id_column'] => $entities_array[$key]['entity_in_id'] ), is_array($items[$val['entity_in_type']]['title_column']) ? $items[$val['entity_in_type']]['title_column'][0] : $items[$val['entity_in_type']]['title_column'] );
					}
					
				}
				
			}
			
		}
		
		return $entities_array;
		
	}
	
	/**
	 * 
	 * ������� ��������� ������������ ��������
	 * @param int $entity_id - ID �������, �����, �������...
	 * @param string $entity_type - ��� �������, �����...
	 * @param int $entity_in_id - �������� � �������� �������, ������, ������...
	 * @param string $entity_in_type - ��� �������, ������, �����...
	 */
	public static function search_entity_single($entity_id, $entity_type, $entity_in_id, $entity_in_type)
	{
		
		$CI =& get_instance();
		$CI->load->model('model_common');
		
		$entity = $CI->model_common->select_one("entities_includes", array(
			"entity_id" => intval($entity_id),
			"entity_type" => (string)$entity_type,
			"entity_in_id" => intval($entity_in_id),
			"entity_in_type" => (string)$entity_in_type));
		
		if (!empty($entity))
		{
			return $entity;
		}
		else
		{
			return null;
		}
		
	}
	
	//������� ���������
	public static function get_similars_by_content($content_id, $content_type)
	{
		
		$CI =& get_instance();
		$config = $CI->config->item('change_objects');
		if (empty($config[$content_type]))
		{
			Show_Errors::show_permission_error("����������� ��� �������: " . $content_type);
		}
		
		if (empty($config[$content_type]['id_column']) or empty($config[$content_type]['title_column']) or empty($config[$content_type]['table']))
		{
			Show_Errors::show_permission_error("�� ���������� ���� ID � Title ��� ������� � ��������� " . $content_type);
		}
		
		$content_table = $config[$content_type]['table'];
		$content_id_column = $config[$content_type]['id_column'];
		$content_title_column = is_array($config[$content_type]['title_column']) ? $config[$content_type]['title_column'][0] : $config[$content_type]['title_column'];
		
		$CI->load->library('validate');
		$CI->model_common->clear_joins();
		$CI->model_common->set_join($content_table, "similars_content.similar_id = {$content_table}.{$content_id_column}", "left");
		$result = $CI->model_common->select("similars_content", array("content_id" => $CI->validate->set_int($content_id), "content_type" => $CI->validate->set_text($content_type)), "similars_content.*, {$content_table}.{$content_id_column}, {$content_table}.{$content_title_column} as title");
		$CI->model_common->clear_joins();
		
		return $result;
		
	}
	
}
?>