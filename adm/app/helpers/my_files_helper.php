<?php 
/**
 * 
 * ����� ������ � ������� � �������
 * @author ashmits
 *
 */
class My_Files_Helper
{
	
	/**
	 * 
	 * ������� ��������� �����
	 * @param Array $paths
	 */
	
	public $files = array();
	
	/**
	 * 
	 * ��������� ���� � ������������
	 * @param unknown_type $file_full_path - ��������� path
	 * @param unknown_type $remote_server_folder - ��������� path
	 * @param unknown_type $curl
	 */
	public static function send_file_to_filearchive($file_full_path, $remote_server_folder, $curl)
	{
		
		if (file_exists($file_full_path))
		{
			$action = "https://adm.zoom.cnews.ru/index.php/file_uploader/";
			$post = array(
				"folder" => (string)strtolower($remote_server_folder),
				"file" => '@' . (string)$file_full_path
			);
			
			//$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, (string)$action);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST , false);
			curl_setopt($curl, CURLOPT_USERPWD, "psycho:wE-[28!");
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
			curl_setopt($curl, CURLOPT_VERBOSE, 1);
			curl_setopt($curl, CURLOPT_COOKIE, "proxy_domain=". DOMAIN ."/admin/ajax/get_remote_url/?url=;");
			$data = curl_exec($curl);
			//curl_close($curl);
		}
		
	}
	
	/**
	 * 
	 * �������� ����� �� ���� ���������
	 * @param string $local_path
	 * @param string $remote_path
	 */
	public static function upload_file_to_filearchive($local_path, $remote_path)
	{
		
		$CI =& get_instance();
		$CI->load->library("ftp");
		$CI->ftp->connect($CI->config->item('ftp'));
		$array = explode("/", $remote_path);
		unset($array[count($array)-1]);
		$folder = "/";
		foreach ($array as $item)
		{
			$folder .= $item . "/";
			if (!$CI->ftp->list_files($folder))
				$CI->ftp->mkdir($folder, 0777);
		}
		
		
		$CI->ftp->upload($local_path, $remote_path, 'auto', 0777);
		
		$CI->ftp->close();
	}
	
	public static function set_image_dirs($paths = array())
	{
		
		foreach ($paths as $path)
		{
			
			$dirs = explode("/", $path);
			if (!empty($dirs) and count($dirs) > 0)
			{
				
				$single_path = UPLOAD_IMAGE_ROOT;
				foreach ($dirs as $dir)
				{
					
					$single_path .= "/" . $dir;
					if (!is_dir($single_path))
					{
						@mkdir($single_path);
						@chmod($single_path, 0777);
					}
					
				}
				
			}
			
		}
		
	}
	
	
	public static function get_subdirs($dir, $iframe = null)
	{
		
		$CI =& get_instance();
		$CI->load->helper('common_helper');
		
		$subdirs = array();
		
		$pattern = sprintf("^%s$", (string)addslashes(UPLOAD_IMAGE_ROOT));
		$pattern = str_replace("/", "\/", $pattern);
		$pattern = str_replace(".", "\.", $pattern);
		$pattern = "/" . $pattern . "/i";
			
		if (!preg_match($pattern, $dir))
		{
			$floor = explode("/", $dir);
			unset($floor[count($floor)-1]);
			$subdirs[] = array("url" => Common_Helper::getInstance()->array_to_hash(array('current_dir' => implode("/", $floor), "iframe" => $iframe)), "name" => ".." );
		}
		
		
		if (is_dir($dir))
		{
		
			$handle = opendir($dir);
			while ( ($row = readdir($handle)) !== false )
			{

				if (is_dir($dir."/".$row) and $row != ".")
				{
					if (!in_array($row, array(".","..")))
					{
						$subdirs[] = array('url' => Common_Helper::getInstance()->array_to_hash(array('current_dir' => $dir . "/" .$row, "iframe" => $iframe)), 'name' => $row);
					}
				}

			}
			
		}
		
		$remote_subdirs = self::get_subdirs_remote($dir, $subdirs, $iframe);

		if (!empty($remote_subdirs))
		{
			$subdirs = array_merge($subdirs, $remote_subdirs);
		}
		
		
		//���� �������� ����������
		if (preg_match($pattern, $dir))
		{
			
			foreach ($subdirs as $key=>$val)
			{
				
				if (!in_array($subdirs[$key]['name'], $CI->config->item('upload_image_dirs')))
				{
					unset($subdirs[$key]);
				}
				
			}
			
		}
		
		$subdirs = self::sort_folders($subdirs);
		
		return $subdirs;
		
	}
	
	//��������� ������ �����
	public static function sort_folders($folders)
	{
		
		$keys = array();
		$values = array();
		
		foreach($folders as $value)
		{
			$keys[] = $value['name'];
			$values[$value['name']] = $value;
		}
		
		sort($keys);
		
		$folders = array();
		foreach ($keys as $value)
		{
			$folders[] = $values[$value];
		}
		
		return (array)$folders;
		
	}
	
	
	public static function get_subdirs_remote($dir, $local_paths, $iframe)
	{
		$folders = array();
		$rdir = str_replace(UPLOAD_IMAGE_ROOT, "img", $dir);
		$CI =& get_instance();
		$CI->load->library("ftp");
		//$CI->load->helper('')
		$CI->ftp->connect($CI->config->item('ftp'));
		$files = $CI->ftp->list_files($rdir);

		if ($files)
		{
			foreach ($files as $key=>$val)
			{
				if ($info = @pathinfo($val))
				{
					if (is_array($info) and empty($info['extension']) and !empty($info['basename']))
					{
						if (!in_array(trim($info['basename']), array(".", "..")))
						{
							//$folders[] = (string)trim($info['basename']);
							$hash = Common_Helper::getInstance()->array_to_hash(array('current_dir' => $dir . "/" .$info['basename'], "iframe" => $iframe));
							
							if (!self::is_path_exists($local_paths, $hash))
							{
								$folders[] = array('url' => $hash, 'name' => $info['basename']);
							}
						}
					}
				}
			}
		}
		
		return (array)$folders;
	}
	
	
	public static function is_path_exists($path_array1, $hash)
	{
		foreach ($path_array1 as $item)
		{
			if ($item['url'] == $hash)
				return true;
		}
		
		return false;
		
	}
	
	/**
	 * 
	 * ������� ����� � ����������
	 * @param full path - ������� ����������
	 * @param bool $origin - �������� ������ ��������� (��� ���������)
	 */
	public static function get_files($dir, $origin = true)
	{
		
		$CI =& get_instance();
		$CI->load->helper('image');
		
		$CI->load->library('ftp');
		$files = array();
		
		if (is_dir($dir))
		{
		
			$handle = opendir($dir);
			while( ($file = readdir($handle) ) !== false )
			{
				if (!is_dir($dir . "/" . $file))
				{
					if (is_file($dir . "/" . $file))
					{
						//���� ��������� � ������� ������ ��������� - ����������
						if ($origin and Image_Helper::getInstance()->is_image_preview($file))
							continue;
						
						$files[] = array(
							"name" => basename($dir . "/" . $file),
							"filemtime" => filemtime($dir . "/" . $file),
							"url" => str_replace(UPLOAD_IMAGE_ROOT, DOMAIN . UPLOAD_IMAGE_DIR, $dir . "/" . $file));
						
					}
				}
			}
			
			//��������� ����������
			closedir($handle);
			
		}

		$CI->ftp->connect($CI->config->item('ftp'));
		$ftp_files = $CI->ftp->list_files('/img/' . str_replace(UPLOAD_IMAGE_ROOT, "", $dir));
		
		if (!empty($ftp_files))
		{
			
			foreach ($ftp_files as $file)
			{
				
				if (preg_match("/\.(png|jpeg|jpg)$/i", $file))
				{
					
					if ($origin and Image_Helper::getInstance()->is_image_preview($file))
						continue;

					$files[] = array(
						"remote" => true,
						"name" => basename($file),
						"filemtime" => ftp_mdtm($CI->ftp->conn_id, $file),
						"url" => str_replace("/img/", "", REMOTE_FILEARCHIVE_DOMAIN) . $file);
						
					
				}
				
			}
			
		}
		
		$CI->ftp->close();
		
		/*
		//���������� ������ � �����
		$class = __CLASS__;
		$obj = new $class;
		if (!empty($files))
		{
			$obj->sort_files($files);
		}
		*/
		//return (array)$obj->files;
		
		return (array)$files;
		
	}
	
	private function sort_files($files)
	{
		
		$filemtime = 0;
		$position = 0;
		foreach ($files as $key=>$val)
		{
			if ($files[$key]['filemtime'] > $filemtime)
			{
				$filemtime = (int)$files[$key]['filemtime'];
				$position = (int)$key;
			}
		}
		
		$this->files[] = $files[$position];
		unset($files[$position]);
		
		if (count($files) > 0)
		{
			$this->sort_files($files);
		}
		
	}
	
	
	public static function get_path_to_array($path, $iframe = null)
	{
		
		$CI =& get_instance();
		$CI->load->helper('common');
		
		$array = explode("/", str_replace(UPLOAD_IMAGE_ROOT, "", $path));
		
		$path = "";
		foreach ($array as $k => $v)
		{
			if (trim($v) == "")
			{
				unset($array[$k]);
				continue;
			}
			
			
			$path .= "/" . $v;
			$array[$k] = array(
				
				"name" => $v,
				"url" => Common_Helper::getInstance()->array_to_hash(array('current_dir' => UPLOAD_IMAGE_ROOT . $path, 'iframe' => $iframe))
			
			);
			
		}
		
		return (array) $array;
		
	}
	
	public static function set_table_dirs($date, $path)
	{
		
		$year = date("Y", strtotime($date));
		$month = date("m", strtotime($date));
		$day = date("d", strtotime($date));
		
		if (!is_dir($path.$year))
		{
			@mkdir($path.$year);
			@chmod($path.$year, 0777);
		}
		
		$path .= $year."/";
		
		if (!is_dir($path.$month))
		{
			@mkdir($path.$month);
			@chmod($path.$month, 0777);
		}
		
		$path .= $month."/";
		if (!is_dir($path.$day))
		{
			@mkdir($path.$day);
			@chmod($path.$day, 0777);
		}
		
		$path .= $day."/";
		
		return (string)$path;
		
	}
	
	/**
	 * 
	 * ������� ��������� ����� �� �������
	 * @param unknown_type $project_id
	 * @param unknown_type $task_id
	 */
	public static function set_tracker_dir($project_id, $task_id = null)
	{
		
		$path = TRACKER_FILE_PATH . $project_id . '/';
		$year = date("Y");
		$month = date("m");
		$day = date("d");

		if (!is_dir($path))
		{
			@mkdir($path);
			@chmod($path, 0777);
		}
		
		if (!is_dir($path.$year))
		{
			@mkdir($path.$year);
			@chmod($path.$year, 0777);
		}
		
		$path .= $year."/";
		
		if (!is_dir($path.$month))
		{
			@mkdir($path.$month);
			@chmod($path.$month, 0777);
		}
		
		$path .= $month."/";
		if (!is_dir($path.$day))
		{
			@mkdir($path.$day);
			@chmod($path.$day, 0777);
		}
		
		$path .= $day."/";

		if (!empty($task_id))
		{
			if (!is_dir($path.$task_id))
			{
				@mkdir($path.$task_id);
				@chmod($path.$task_id, 0777);
			}
			
			$path .= $task_id."/";
		}
		
		return (string)$path;
		
	}
	
	
	
	public static function get_image_upload_dir($date)
	{

		$path = GALLERY_IMAGE_UPLOAD_PATH;
		
		$year = date("Y", strtotime($date));
		$month = date("m", strtotime($date));
		$day = date("d", strtotime($date));
		
		if (!is_dir($path.$year))
		{
			@mkdir($path.$year);
			@chmod($path.$year, 0777);
		}
		
		$path .= $year."/";
		
		if (!is_dir($path.$month))
		{
			@mkdir($path.$month);
			@chmod($path.$month, 0777);
		}
		
		$path .= $month."/";
		if (!is_dir($path.$day))
		{
			@mkdir($path.$day);
			@chmod($path.$day, 0777);
		}
		
		$path .= $day."/";
		
		return (string)$path;
		
		
	}
	
	public static function get_image_url_by_date($date)
	{
		
		$year = date("Y", strtotime($date));
		$month = date("m", strtotime($date));
		$day = date("d", strtotime($date));
		
		return "/" . $year . "/" . $month . "/" . $day;
	}
	
	
	public static function validate_image($file)
	{
		
		//���� ��������� ������
		if (is_array($file))
		{
			$name = $file['name'];
			$type = $file['type'];
		}
		
		if (preg_match("/\.(png|jpeg|jpg)$/i", $name))
		{
			//swfupload �������� ��� ����� � �����������, ������� �������� ������.
			$name = preg_replace("/(.*)?(jpg|png|jpeg)\.(png|jpeg|jpg)$/i", "$1.$2", $name);
			return (string)$name;
		}
		
		Show_Errors::show_permission_error("�������� ������ �����������");
		
	}
	
	public static function generate_image_name($image)
	{
		
		if (preg_match("/\.(png|jpg|jpeg)$/i", $image, $matches))
		{
			$ext = $matches[1];
			return sha1($image . rand(0, 1000)) . "." . $ext;
		}
		 
	}

	public static function generate_image_name_translit($image)
	{
		
		if (preg_match("/(.*)?\.(png|jpg|jpeg)$/i", $image, $matches))
		{
			
			$name = strtolower($matches[1]);
			$ext = strtolower($matches[2]);
			$name = Common_Helper::getInstance()->rus_to_translit((string)$name);
			
			return $name . "." . $ext;
			
		}
		 
	}
	
	
	public static function set_image_name_in_dir($image_name, $dir, $iterator = 1)
	{

		if (file_exists($dir . "/" . $image_name))
		{
			
			$image_name = ($iterator == 1) ? preg_replace("/\.(gif|jpg|jpeg|png)$/i", "_{$iterator}.$1", $image_name) : preg_replace("/_\d{1,}\.(gif|png|jpg|jpeg)$/i", "_{$iterator}.$1", $image_name);
			return self::set_image_name_in_dir($image_name, $dir, ++$iterator);
			
		}
		
		return $image_name;
		
	}

	
	public static function set_image_name_in_remote_dir($image_name, $remote_dir, $iterator = 1)
	{
		
		$CI =& get_instance();
		$CI->load->library('ftp');
		$CI->ftp->connect($CI->config->item('ftp'));
		$files = $CI->ftp->list_files($remote_dir);
		
		if (!empty($files))
		{
			//������� ����� ������� � � �����
			$image_name = preg_replace("/^\//i", "", $image_name);
			$remote_dir = preg_replace("/\/$/i", "", $remote_dir);
			
			if (in_array($remote_dir . "/" . $image_name, array_values($files)))
			{
				$image_name = ($iterator == 1) ? preg_replace("/\.(gif|jpg|jpeg|png)$/i", "_{$iterator}.$1", $image_name) : preg_replace("/_\d{1,}\.(gif|png|jpg|jpeg)$/i", "_{$iterator}.$1", $image_name);
				return self::set_image_name_in_remote_dir($image_name, $remote_dir, ++$iterator);
			}
		}
		
		$CI->ftp->close();
		return $image_name;
		
	}
	
	
	public static function get_image_home_dir($path)
	{
		
		$path = str_replace(UPLOAD_IMAGE_ROOT, "", $path);
		$array = explode("/", $path);
		
		$CI =& get_instance();
		
		foreach ($array as $item)
		{
			if (in_array($item, $CI->config->item('upload_image_dirs')))
			{
				return (string)$item;
			}
		}
		
		return null;
		
	}
	
}

?>