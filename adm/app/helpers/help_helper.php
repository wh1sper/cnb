<?php
	
	//======================================================
	// ������� ������ ��� ������ �����
	//======================================================
	// ������� ��������� ������� ������� ����� �� �����
	function split_text($s, $len = 10) 
	{ 
		$marker = "\x01";
		$hyp = " ";
		
		$wordmaxlen = $len;
		
		// ��������� ��� ���� ����� ������� �� �� ��������
		preg_match_all('/(<.*?\>)/si',$s,$tags);
		
		// �������� ��� ���� �� �������
		$s =  preg_replace('/(<.*?\>)/si', $marker, $s);
		
		// ��������� ����� �� �����
		$words = split(' ',$s);
		
		for ($i=0; $i<count($words); $i++)
		{
			// ������ ����� >= $wordmaxlen ���������
		  if (strlen($words[$i])>=$wordmaxlen)
		    $words[$i] = chunk_split($words[$i],$wordmaxlen,$hyp);
		    
		}
		
		// �������� ������ �� ��� �������� �� ����� ����
		$s = implode(' ',$words);
		
		// ��������������� ����, ����� ������� ���� �������� ���������
		for ($i=0; $i<count($tags[1]); $i++)
			$s =  preg_replace("/$marker/si", $tags[1][$i], $s, 1);
		
		return $s;
	}

	function text_format($word, $fl) 
	{
		$up_letter   = '�����Ũ��������������������������';
		$down_letter = '��������������������������������';
		if($fl == '1') return trim(strtr($word, $down_letter, $up_letter));
	 		return trim(strtr($word, $up_letter, $down_letter));
	}	
	
	function text_validation($text = "")
	{	
		$text[0] = strtoupper($this->_text_format($text[0], 1));
		
		$text = trim($text);
		foreach (array(";", ":", ",", ".", "!", "?") as $val)
		{
			$text = str_replace(" $val ", "$val ", $text);
		}

		foreach (array(",", ".") as $val)
		{
			$text = str_replace(" $val", "$val ", $text);
		}		
		
		// �������� ' �� `
		$text = str_replace("'", "`", $text);
		
		// ������ ���� ����� �� ���������
		$text = str_replace("..", "...", $text);
		
		// �������� ��� ������� ������������� ����� 3� ��� �� 3� ����������
		preg_match_all("/[?!\.,]{3,}/is", $text, $match);
		
		foreach ($match[0] as $key => $val)
		{
			$text = str_replace($val, substr($val, 0, 3), $text);
		}
		
		// ���������� ������� ����� ���� ������ ����������
		// ���� ��� �� , : ; �� ��������� ����������� ����� � ������� ������� 
		$match = "";
		preg_match_all("/([�-���-�])(\.|!|\?|,|:|;|\.\.\.)([�-���-�])/is", $text, $match);
		if (!empty($match))
		{
			foreach ($match[0] as $key => $val)
			{
				$flatter = $match[1][$key];
				$sumb = $match[2][$key];
				$slatter = $match[3][$key];
				
				if ($match[0][$key] == "�.�" or $match[0][$key] == "�.�") continue;
				
				if ($sumb != "," and $sumb != ":" and $sumb != ";")
				{
					$slatter = strtoupper($this->_text_format($slatter, 1));
				}
				
				$text = str_replace($match[0][$key], $flatter.$sumb." ".$slatter, $text);
			}
		}
		
		// ����������� ����
		// �������� �������� ���� (�)
		$text = str_replace("�", "�", $text);
		// �������� �����, ��������� ��������� ( - ); 
		$text = str_replace(" - ", " � ", $text);
		$text = str_replace(" -", " � ", $text);
		$text = str_replace("--", "�", $text);
		// ����� � ������ ������ (������ ����); 
		if ($text[0] == "-") $text[0] = "�";
		$text = str_replace("\n- ", "\n�", $text);
		$text = str_replace("\n - ", "\n�", $text);
		$text = str_replace("\n\t- ", "\n\t�", $text);
			
		// ������ ���������� ������ ��� ��������� �� ������� ���������
		return  $text;
	}	
	
	//�������������� ������� ���� � �����������
	function rus_to_translit($s){
		$s=trim(strtolower($s));  
		$s = preg_replace("/[^�-��-�a-zA-Z0-9 +]/ies", '', $s);
		$s=str_replace("�", "a",$s); $s=str_replace("�", "b",$s);
		$s=str_replace("�", "v",$s); $s=str_replace("�", "g",$s);
		$s=str_replace("�", "d",$s); $s=str_replace("�", "e",$s);
		$s=str_replace("�", "yo",$s); $s=str_replace("�", "zh",$s);
		$s=str_replace("�", "z",$s); $s=str_replace("�", "i",$s);
		$s=str_replace("�", "j",$s); $s=str_replace("�", "k",$s);
		$s=str_replace("�", "l",$s); $s=str_replace("�", "m",$s);
		$s=str_replace("�", "n",$s); $s=str_replace("�", "o",$s); 
		$s=str_replace("�", "p",$s); $s=str_replace("�", "r",$s);
		$s=str_replace("�", "s",$s); $s=str_replace("�", "t",$s);
		$s=str_replace("�", "u",$s); $s=str_replace("�", "f",$s);
		$s=str_replace("�", "h",$s); $s=str_replace("�", "ts",$s);
		$s=str_replace("�", "ch",$s); $s=str_replace("�", "sh",$s); 
		$s=str_replace("�", "shch",$s); $s=str_replace("�", "",$s); 
		$s=str_replace("�", "y",$s); $s=str_replace("�", "",$s); 
		$s=str_replace("�", "e",$s); $s=str_replace("�", "yu",$s); 
		$s=str_replace("�", "ya",$s); $s=str_replace(" ", "_",$s); 
		
		$s=str_replace("�", "a",$s); $s=str_replace("�", "b",$s);
		$s=str_replace("�", "v",$s); $s=str_replace("�", "g",$s);
		$s=str_replace("�", "d",$s); $s=str_replace("�", "e",$s);
		$s=str_replace("�", "yo",$s); $s=str_replace("�", "zh",$s);
		$s=str_replace("�", "z",$s); $s=str_replace("�", "i",$s);
		$s=str_replace("�", "j",$s); $s=str_replace("�", "k",$s);
		$s=str_replace("�", "l",$s); $s=str_replace("�", "m",$s);
		$s=str_replace("�", "n",$s); $s=str_replace("�", "o",$s); 
		$s=str_replace("�", "p",$s); $s=str_replace("�", "r",$s);
		$s=str_replace("�", "s",$s); $s=str_replace("�", "t",$s);
		$s=str_replace("�", "u",$s); $s=str_replace("�", "f",$s);
		$s=str_replace("�", "h",$s); $s=str_replace("�", "ts",$s);
		$s=str_replace("�", "ch",$s); $s=str_replace("�", "sh",$s); 
		$s=str_replace("�", "shch",$s); $s=str_replace("�", "",$s); 
		$s=str_replace("�", "y",$s); $s=str_replace("�", "",$s); 
		$s=str_replace("�", "e",$s); $s=str_replace("�", "yu",$s); 
		$s=str_replace("�", "ya",$s);
		
		return $s;
	}
	
	// ������� ��������� ������ ���������� ������ �����������
	// �� ��������: << -1- -2- >>
	// $n_this_page - ����� ������� ��������
	// $n_pages - ����� �������
	// $controller_name - ��� ����������� (��� �������� �����������)
	function drop_to_page($n_this_page, $n_pages, $controller_name, $page_name = '')
	{
		if ($n_pages <= 1) return 0;
	
		$d = 5; // �������� ������
		$start = 1;
		$stop = 2*$d;
	
		if (($n_this_page - $d) > 1)
		{
			$start = $n_this_page - $d;
			$stop = $n_this_page + $d;
		}
	
		if ($stop > $n_pages)
		{
			$stop = $n_pages;
			$start = $n_pages - 2*$d;
		}
	
		if ($start < 1) $start = 1;
	
		//$array_link[] = '��������: ';

		// ������� �� ������ ��������
		if ($n_this_page > 1)
		{
			$path = "/".$controller_name."1".$page_name;
			$array_link[] = "<a href='$path' class='rew'>&lt;&lt;</a> ";
		}


		// ����� ������ ������� �� $start � ���������� $stop
		for ($i = $start; $i <= $stop; $i++)
		{
				$path = "/".$controller_name.$i.$page_name;
				if ($i != $n_this_page)
				{
					$array_link[] = "<a href='$path' > $i </a>";
				}	
			 	else $array_link[] = "<span > $i </span>";
		}
		

		// ������� �� ��������� ��������
		if ($n_this_page < $n_pages)
		{
			$path = "/".$controller_name.$n_pages.$page_name;
			$array_link[] = " <a href='$path' class='ff'>&gt;&gt;</a>";
		}	

	
		return $array_link;
	}
	
	/*
	* ������� �������� ������� ������� ��������� � ������� ��� �������
	* ������� �������� ��������� "..."
	* ���������:
	* $annotation - ���������, ������� ��������� ��������
	* $len - ����� ���������, ������� ��������� ��������
	* ������������ �������� - ����������� ���������.
	*/
	function cut_long_annotation($annotation, $len)
	{
		$annotation = strip_tags($annotation);
		//�������� ������� ������� ��������
		if (strlen($annotation)>$len and !empty($annotation))
		{		
			// ������ ����� ���������� �������, � �������� ����� ����������
			// len_description. ����� �� �������� ����� �� ��������.
			$annotation=stripslashes($annotation);

			$pos = strpos($annotation, ' ', $len);

			// ������ ��� � ����������� ����� ������� ���� ������� ������� � $len
			// ��� ���� ������. �� �������� ����� $len ���. ����� $pos ����� ��������� �������.
			// �������� ��������:
			if (empty($pos) and $len > 10) $pos = strpos($annotation, ' ', $len - 10);

			if (empty($pos)) return $annotation;

			$annotation = substr_replace($annotation, '', $pos)."...";		

			//�� ������, ���� � ����� ������ ������� ��� ����� ('���,...' ��� '���....')
			$annotation = str_replace(',...', '...', $annotation);
			$annotation = str_replace('......', '...', $annotation);
		}
		return $annotation;
	}
	
	function get_site_description($text = "")
	{
		$text = str_replace(array("\n", "</p>", "<br>"), " ", $text);
		$text = str_replace(array('"', "'", "\t"), "", $text);
		$text = str_replace(array("&nbsp;"), " ", $text);
		$text = strip_tags($text);
		$text = cut_long_annotation($text, 200);
		$text = str_replace(array('   '), " ", $text);
		$text = str_replace(array('  '), " ", $text);

		return $text;
	}

	// ������� ���������� ����� ���������� ����� � ������, ������������ ��� �������� keywords ����
	function get_keywords($text = "", $count_words = 12)
	{
		$text = str_replace(array("\n", "<p>", "<p />", "<br />", "<br>", ".", ",", ":", ";", "-"), " ", $text);
		$text = str_replace(array("&nbsp"), " ", $text);
		$text = text_format(strip_tags($text), 0);
		$text = preg_replace("/[^�-��-�a-zA-Z ]/ies", ' ', $text);

		$array = explode(" ", $text);

		foreach ($array as $key => $val)
		{
			if (strlen($val) < 5) unset($array[$key]);
		}

		usort($array, "sort_max_word_len");

		$array_result = array();

		$array_result[] = array('word' => $array[0], "count" => "1");
		unset($array[0]);

		foreach ($array as $key => $word)
		{
			$i = 0;
			foreach ($array_result as $r_key => $r)
			{
				if (substr_count($r['word'], $word))
				{
					$array_result[$r_key]['count']++;
					$i = 1;
					break;
				}
			}

			if ($i == 0) $array_result[] = array('word' => $word, "count" => "1");

		}

		usort($array_result, "sort_max_word_count");

		$array_result = array_slice($array_result, 0, $count_words);

		$result_str = "";
		foreach ($array_result as $a)
		{
			if (!empty($result_str)) $result_str .= ", ";
			$result_str .= $a['word'];
		}

		return $result_str;
	}

	function sort_max_word_len($a, $b)
	{
		if (strlen($a) == strlen($b)) {
			return 0;
		}
		return (strlen($a) < strlen($b)) ? 1 : -1;
	}
	
	function sort_max_word_count($a, $b)
	{
		if ($a['count'] == $b['count']) {
			return 0;
		}
		return ($a['count'] < $b['count']) ? 1 : -1;
	}
	

	function declension($n,$string=array('����','���','����'))
	{
	    $n = abs($n) % 100;
	    $n1 = $n % 10;
	    if ($n > 10 && $n < 20) return $string[2];
	    if ($n1 > 1 && $n1 < 5) return $string[1];
	    if ($n1 == 1) return $string[0];
	    return $string[2];
	}
	
	// ������� ���������� ip ������������
	function get_real_ip()
	{
		 if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
		 {
		   $ip=$_SERVER['HTTP_CLIENT_IP'];
		 }
		 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		 {
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		 }
		 else
		 {
		   $ip=$_SERVER['REMOTE_ADDR'];
		 }
		 return $ip;
	}
	
?>