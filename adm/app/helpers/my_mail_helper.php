<?php 

/**
 * 
 * �������� �����
 * @author ashmits
 *
 */
class My_Mail_Helper
{
	
	/**
	 * 
	 * �������� ����� ��� ���������/���������� ������
	 * @param array $task
	 */
	public static function send_task_users($task)
	{
		
		$emails = array();
		$emails[] = $task['create_email'];
		
		if (!in_array($task['change_email'], $emails))
			$emails[] = $task['change_email'];
		if (!in_array($task['producer_email'], $emails))
			$emails[] = $task['producer_email'];
		if (!in_array($task['performer_email'], $emails))
			$emails[] = $task['performer_email'];
		
		$CI =& get_instance();
		$CI->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'windows-1251';
		$config['wordwrap'] = TRUE;
		
		$priority = $CI->config->item('task_priority');
		$task['priority'] = $priority[$task['task_priority']];
		
		$status = $CI->config->item('task_status');
		$task['status'] = $status[$task['task_status']];
		
		$CI->email->initialize($config);			
		$CI->email->from('noreply@' . $_SERVER['SERVER_NAME'], "������ " . $_SERVER['SERVER_NAME']);
		$CI->email->to(implode(",", $emails));
		$CI->email->subject("������: " . $task['task_title']);
		$CI->smarty->assign('task', $task);
		$CI->email->message($CI->smarty->fetch("admin/tracker/task_email.tpl"));
		$CI->email->send();
		
	}
	
	/**
	 * 
	 * �������� ����� ��� ���������� ����������� � ������
	 * @param array $task
	 * @param array $comment
	 */
	public static function send_task_comment($task, $comment)
	{
		
		$emails = array();
		$emails[] = $task['create_email'];
		
		if (!in_array($task['change_email'], $emails))
			$emails[] = $task['change_email'];
		if (!in_array($task['producer_email'], $emails))
			$emails[] = $task['producer_email'];
		if (!in_array($task['performer_email'], $emails))
			$emails[] = $task['performer_email'];
		if (!in_array($comment['create_user_email'], $emails))
			$emails[] = $comment['create_user_email'];
		
		$CI =& get_instance();
		$CI->load->library('email');
		
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'windows-1251';
		$config['wordwrap'] = TRUE;
		
		$CI->email->initialize($config);
		$CI->email->from('noreply@' . $_SERVER['SERVER_NAME'], "������ " . $_SERVER['SERVER_NAME']);
		$CI->email->to(implode(",", $emails));
		$CI->email->subject("����� ����������� � ������: " . $task['task_title']);
		$CI->smarty->assign('task', $task);
		$CI->smarty->assign('comment', $comment);
		$CI->email->message($CI->smarty->fetch("admin/tracker/task_comment_email.tpl"));
		$CI->email->send();
		
	}
	
}

?>