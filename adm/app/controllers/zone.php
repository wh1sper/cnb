<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Zone extends Starter
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('model_zone');

        $this->admin_menu->set_active_section('site');
        $this->admin_menu->set();

    }

    function index($id_site)
    {
        if(!is_numeric($id_site))
        {
            show_404("Error");
        }

        $site = $this->model_common->select_one("site",array('id'=>$id_site));
        if(is_null($site))
        {
            show_404("������ ����� ���");
        }

        $this->header_block->set_title('����');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�����', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add($site['name'], DOMAIN.'/adm/site/edit/'.$site['id']);
        $this->breadcrumbs_block->add('����','');

        $this->breadcrumbs_block->set();

        // ������ ������ ��� �� ������ ��� ����� �����
        $list = $this->model_zone->select_by_site($id_site);

        $this->smarty->assign('site',$site);
        $this->smarty->assign("zone_list",$list);
        $this->_display("zone/zone_index.tpl");
    }

    // ������� ��� �������, ����������� � ���� ����
    function banner($id_zone)
    {
        if(!is_numeric($id_zone))
        {
            show_404("������������ �������� ��������");
        }

        $this->load->model('model_zone');

        $zone = $this->model_zone->select_with_banners($id_zone);
        $this->smarty->assign('zone',$zone[0]);

        if(is_null($zone))
        {
            show_404("����� ���� ���");
        }

        $this->load->model("model_client");
        $clients = $this->model_client->select_filled();
        $this->smarty->assign('client_list',$clients);

        $this->header_block->set_title('������� � ���� '.$zone[0]['name']);
        $this->header_block->set();

        $this->breadcrumbs_block->add('�����',DOMAIN.'/adm/site');
        $this->breadcrumbs_block->add($zone[0]['sitename'],DOMAIN.'/adm/site/edit/'.$zone[0]['siteid']);
        $this->breadcrumbs_block->add('����',DOMAIN.'/adm/zone/index/'.$zone[0]['siteid']);
        $this->breadcrumbs_block->add($zone[0]['name'],DOMAIN.'/adm/zone/edit/'.$zone[0]['id']);
        $this->breadcrumbs_block->add('�������','');
        $this->breadcrumbs_block->set();

        $this->_display('zone/zone_banner.tpl');
    }

    // ��� ���� �� ��������, ������� ���������� ��������������� ���������, � ����� �����������
    function add($id_site)
    {
        if(!is_numeric($id_site))
        {
            show_404("������������ �������� ��������");
        }

        $site = $this->model_common->select_one("site",array('id'=>$id_site));
        if(is_null($site))
        {
            show_404("������ ����� ���");
        }

        $this->header_block->set_title('���������� ����');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�����', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add($site['name'],$site['url']);
        $this->breadcrumbs_block->add('����', DOMAIN .'/adm/zone/site/'.$id_site);
        $this->breadcrumbs_block->add('����������','');
        $this->breadcrumbs_block->set();

        $this->smarty->assign('zone',0);
        $this->smarty->assign('site',$site);

        $this->_display('zone/zone_add.tpl');

    }


    function edit($zone_id)
    {
        if(!is_numeric($zone_id))
        {
            show_404("Wrong id");
        }

        $zone = $this->model_zone->select_with_banners($zone_id);
        $zone = $zone[0];
        $site = $this->model_common->select_one("site",array("id"=>$zone['id_site']));

        if(is_null($zone))
        {
            show_404("����� ���� ���");
        }

        $sites = $this->model_common->select("site",null,null,"id");
        $this->smarty->assign('site_list',$sites);

        $redirect = 0;
        // ������ ��� ���������
        foreach($zone['banners'] as $v) {
            if($v['type']=="redirect") {
                $redirect = $v;
            }
        }

        $this->smarty->assign("redirect",$redirect);

        $this->header_block->set_title('����');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�����', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add($site['name'], DOMAIN.'/adm/site/edit/'.$site['id']);
        $this->breadcrumbs_block->add('����',DOMAIN.'/adm/zone/index/'.$site['id']);
        $this->breadcrumbs_block->add($zone['name'],'');

        $this->breadcrumbs_block->set();


        $this->smarty->assign("zone",$zone);

        $this->_display('zone/zone_add.tpl');

    }

    function save()
    {
        $data = $this->collector->zone();
        // ������
        if(isset($data['id']))
        {
            $this->model_common->update("zone", $data, array("id" => $data['id']));
        }
        // ����������
        else
        {
            $zone_id = $this->model_common->insert("zone",$data);
        }

        My_Url_Helper::redirect(DOMAIN . '/adm/site');
    }

    function delete($id)
    {
        if(!is_numeric($id))
        {
            show_404("Wrong id");
        }

        $zone = $this->model_common->select_one("zone",array("id"=>$id));

        if(is_null($zone))
        {
            show_404("����� ���� ���");
        }

        $this->model_zone->delete($id);

        My_Url_Helper::redirect(DOMAIN.'/adm/site');
    }

    // �������� ����, ������ json ���
    function ajax_list($id_site)
    {
        $list = $this->model_zone->select_by_site($id_site);
        foreach($list as &$v)
        {
            foreach($v as &$vv)
                $vv = iconv('windows-1251','utf-8',$vv);
        }
        echo json_encode($list);
    }


}

?>