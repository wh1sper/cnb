<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Site extends Starter
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('model_site');

        $this->admin_menu->set_active_section('site');
        $this->admin_menu->set();

    }

    function index()
    {
        $this->header_block->set_title('�����');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�����', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->set();

        // ������ ������ ������ �� ���� ������ - ���������� ����, ��������, ������, �������
        $list = $this->model_site->select_full_list();

        $this->smarty->assign("site_list",$list);
        $this->_display("site/site_index.tpl");
    }

    function add()
    {
        $this->header_block->set_title('���������� �����');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�����', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add('����������', DOMAIN.'/adm/site/add');
        $this->breadcrumbs_block->set();

        $this->smarty->assign('site',0);

        $this->_display('site/site_add.tpl');

    }


    function edit($id_site)
    {
        if(!is_numeric($id_site))
        {
            show_404("������������ �������� ��������");
        }

        $site = $this->model_common->select_one("site",array('id'=>$id_site));

        if(is_null($site))
        {
            show_404("������ ����� ���");
        }

        $this->header_block->set_title('�������������� ����� '.$site['name']);
        $this->header_block->set();

        $this->breadcrumbs_block->add('�����', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add($site['name'],'');
        $this->breadcrumbs_block->set();


        $this->smarty->assign("site",$site);

        $this->_display('site/site_add.tpl');

    }

    function save()
    {
        $data = $this->collector->site();
        // ������
        if(isset($data['id']))
        {
            $this->model_common->update("site", $data, array("id" => $data['id']));
        }
        // ����������
        else
        {
            $site_id = $this->model_common->insert("site",$data);
        }

        My_Url_Helper::redirect(DOMAIN.'/adm/site');

    }

    function delete($id)
    {
        // �������� ����� �������� �������� ��� ��� � ������ �������� � ����� ������
        $this->model_site->delete($id);
        My_Url_Helper::redirect($this->input->server('HTTP_REFERER'));

    }

}

?>