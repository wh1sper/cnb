<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Stat extends Starter
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('model_stat');

        $this->admin_menu->set_active_section('stat');
        $this->admin_menu->set();

    }

    function index()
    {
        $this->header_block->set_title('����������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('����������', DOMAIN.'/adm/stat/');
        $this->breadcrumbs_block->set();

        $sec_from = time() - 604800;
        $data['date_from'] = date("Y-m-d",$sec_from);
        $data['date_to'] = date("Y-m-d");
        $data['period'] = "week";

        if(isset($_GET['filter']))
        {
            $data = $this->collector->stat();
            $getstr[] = "filter=1";

            if(isset($data['client']))
            {
                $campaigns = $this->model_common->select("campaign",array("id_client"=>$data['client']),"id,name","id");
                $this->smarty->assign("campaigns",$campaigns);
                $getstr[] = 'client='.$data['client'];
            }

            if(isset($data['campaign']))
            {
                $banners = $this->model_common->select("banner",array("id_campaign"=>$data['campaign']),"id,name","id");
                $this->smarty->assign("banners",$banners);
                $getstr[] = 'campaign='.$data['campaign'];
            }

            if(isset($data['site']))
            {
                $zones = $this->model_common->select("zone",array("id_site"=>$data['site']),"id,name","id");
                $this->smarty->assign("zones",$zones);
                $getstr[] = 'site='.$data['site'];
            }

            if(isset($data['zone'])) $getstr[] = 'zone='.$data['zone'];
            if(isset($data['banner'])) $getstr[] = 'banner='.$data['banner'];

            $getstr[] = 'date_from='.$data['date_from'];
            $getstr[] = 'date_to='.$data['date_to'];
            $getstr[] = 'period='.$data['period'];

            $textdata = $this->getFullData($data);
            if(sizeof($textdata)) $this->smarty->assign('textdata',$textdata);

            $this->smarty->assign("getstr",join("&",$getstr));

        }

        foreach($data as $k=>$v)
        {
            $this->smarty->assign($k,$v);
        }

        $dates = $this->model_stat->get_stat_by_interval($data);

        $this->smarty->assign('total_imp',$this->model_stat->total_imp);
        $this->smarty->assign('total_clicks',$this->model_stat->total_clicks);

        $this->smarty->assign('total_ctr',round((($this->model_stat->total_clicks / $this->model_stat->total_imp) * 100),2));

        $this->smarty->assign("today",date("Y-m-d"));
        $this->smarty->assign("dates",$dates);

        $sites = $this->model_common->select("site",null,"id,name","id");
        $this->smarty->assign("sites",$sites);

        $this->load->model("model_client");
        $clients = $this->model_client->select_filled();
        $this->smarty->assign("clients",$clients);

        $this->_display("stat/stat_index.tpl");
    }

    function day($date)
    {
        $this->header_block->set_title('����������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('����������', DOMAIN.'/adm/stat/');
        $this->breadcrumbs_block->add($date,'');
        $this->breadcrumbs_block->set();

        $this->smarty->assign('date',$date);

        $data = array();

        if(isset($_GET['filter']))
        {
            $data = $this->collector->stat();

            if(isset($data['client']))
            {
                $campaigns = $this->model_common->select("campaign",array("id_client"=>$data['client']),"id,name","id");
                $this->smarty->assign("campaigns",$campaigns);
            }

            if(isset($data['campaign']))
            {
                $banners = $this->model_common->select("banner",array("id_campaign"=>$data['campaign']),"id,name","id");
                $this->smarty->assign("banners",$banners);
            }

            if(isset($data['site']))
            {
                $zones = $this->model_common->select("zone",array("id_site"=>$data['site']),"id,name","id");
                $this->smarty->assign("zones",$zones);
            }

            foreach($data as $k=>$v)
            {
                $this->smarty->assign($k,$v);
            }

        }

        $stat = $this->model_stat->get_stat_by_date($date,$data);

        $total['imp'] = $total['clk'] = $total['ctr'] = 0;


        for($i=0;$i<24;$i++)
        {
            if(isset($stat[$i])) {
                $hours[$i] = $stat[$i];
            }
            else {
                $hours[$i]['clk'] = $hours[$i]['imp'] = 0;
            }

            $total['imp'] += $hours[$i]['imp'];
            $total['clk'] += $hours[$i]['clk'];

        }

        $total['ctr'] = round((($total['clk'] / $total['imp']) * 100),2);
        $this->smarty->assign('total',$total);

        $this->smarty->assign("hours",$hours);
        $this->smarty->assign("hour",date("H"));

        $sites = $this->model_common->select("site",null,"id,name","id");
        $this->smarty->assign("sites",$sites);

        $this->load->model("model_client");
        $clients = $this->model_client->select_filled();
        $this->smarty->assign("clients",$clients);

        $this->_display("stat/stat_day.tpl");
    }

    function getFullData($data){
        $res = array();
        foreach($data as $k=>$v)
        {
            if($k!="date_to" && $k!="date_from" && $k!="filter" && $k!="period")
                $res[$k] =  $this->model_common->select_one($k,array("id"=>$v));
        }

        return $res;
    }

    function export($type)
    {

        ob_clean();
        header("Content-type: text/csv");
        header('Content-disposition: attachment; filename=stat.csv');

        $sec_from = time() - 604800;

        if(isset($_GET['filter']))
        {
            $data = $this->collector->stat();
        }

        if(!isset($data['date_from']))
        {
            $data['date_from'] = date("Y-m-d",$sec_from);
            $data['date_to'] = date("Y-m-d");
            $data['period'] = "week";
        }

        foreach($data as $k=>$v)
        {
            $this->smarty->assign($k,$v);
        }

        $textdata = $this->getFullData($data);

        echo "����: ".$data['date_from']." - ".$data['date_to']."\n";
        if(isset($textdata['client'])) echo "������: ".$textdata['client']['name']."\n";
        if(isset($textdata['campaign'])) echo "��������: ".$textdata['campaign']['name']."\n";
        if(isset($textdata['site'])) echo "����: ".$textdata['site']['name']."\n";
        if(isset($textdata['zone'])) echo "����: ".$textdata['zone']['name']."\n";
        if(isset($textdata['banner'])) echo "������: ".$textdata['banner']['name']."\n";

        echo "\n";

        $dates = $this->model_stat->get_stat_by_interval($data);

        $imp = $clk = 0;

        echo "����;������;�����;CTR"."\n";
        foreach($dates as $date)
        {
            echo $date['date'].";".$date['imp'].";".$date['clk'].";".$date['ctr']."\n";
            $imp += $date['imp'];
            $clk += $date['clk'];
        }

        $ctr = round((($clk / $imp) * 100),2);
        echo "�����:;".$imp.";".$clk.";".$ctr;
    }


}

?>