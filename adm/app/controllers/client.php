<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Client extends Starter
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('model_client');

        $this->admin_menu->set_active_section('client');
        $this->admin_menu->set();

    }

    function index()
    {
        $this->header_block->set_title('�������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->set();

        $data = array();
        if(isset($_GET['filter']))
        {
            $data = $this->collector->banner_filter();
            if(isset($data['name']))
            {
                $this->smarty->assign('name',$data['name']);
            }
        }

        // ������ ������ ��������
        $list = $this->model_client->select_full_list($data);

        $this->smarty->assign("client_list",$list);
        $this->_display("client/client_index.tpl");
    }

    function add()
    {
        $this->header_block->set_title('���������� �������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add('����������', DOMAIN.'/adm/site/add');
        $this->breadcrumbs_block->set();

        $this->smarty->assign('client',0);

        $this->_display('client/client_add.tpl');

    }


    function edit($id_client)
    {
        if(!is_numeric($id_client))
        {
            show_404("������������ �������� ��������");
        }

        $client = $this->model_common->select_one("client",array('id'=>$id_client));

        if(is_null($client))
        {
            show_404("������ ����� ���");
        }

        $this->header_block->set_title('�������������� ������� '.$client['name']);
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������', DOMAIN.'/adm/client/');
        $this->breadcrumbs_block->add($client['name'],'');
        $this->breadcrumbs_block->set();

        $this->smarty->assign("client",$client);

        $this->_display('client/client_add.tpl');

    }

    function save()
    {
        $data = $this->collector->client();
        // ������
        if(isset($data['id']))
        {
            $this->model_common->update("client", $data, array("id" => $data['id']));
        }
        // ����������
        else
        {
            $site_id = $this->model_common->insert("client",$data);
        }

        My_Url_Helper::redirect(DOMAIN . '/adm/client');
    }

    function delete($id)
    {
        // �������� ����� �������� �������� ��� ��� � ������ �������� � ����� ������
        $this->model_client->delete($id);
        My_Url_Helper::redirect($this->input->server('HTTP_REFERER'));

    }

}

?>