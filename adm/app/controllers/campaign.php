<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Campaign extends Starter
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_campaign');

        $this->admin_menu->set_active_section('campaign');
        $this->admin_menu->set();

    }

    function index($page=1)
    {

        $data = array();

        if(isset($_GET['filter']))
        {
            $data = $this->collector->campaign_filter();

            if(isset($data['client']))
            {
                $this->smarty->assign('client',$this->model_common->select_one("client",array("id"=>$data['client'])));
            }

            if(isset($data['name']))
            {
                $this->smarty->assign('name',$data['name']);
            }
        }

        $offset = $page > 1 ? (($page - 1) * 10) : 0;
        $this->smarty->assign('offset',$offset);

        $this->load->model("model_client");
        $clients = $this->model_client->select_filled();
        $this->smarty->assign("clients",$clients);

        $this->header_block->set_title('��������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('��������', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->set();

        // ������ ������ ��� �� ������ ��� ����� �����

        $res = $this->model_campaign->select_by_page($page,$data);

        $list = $res['data'];
     //   $list = $this->model_campaign->select_full_list($data);

        $array_link = $this->drop_to_page($page, ceil($res['count'] / 10), "campaign", $data);

        $this->smarty->assign("array_link", $array_link);
        $this->smarty->assign("campaign_list",$list);

        $this->_display("campaign/campaign_index.tpl");
    }

    // ������� ��� �������� �������, ����� � ������� �������
    function addraw()
    {
        $this->header_block->set_title('���������� ��������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('��������', DOMAIN.'/adm/campaign/');
        $this->breadcrumbs_block->add('����������','');
        $this->breadcrumbs_block->set();

        $this->load->model("model_client");
        $client_list = $this->model_client->select_full_list();
        $this->smarty->assign('client_list',$client_list);

        $this->smarty->assign('campaign',0);
        $this->_display('campaign/campaign_add.tpl');
    }

    // ������� � ������� ��������� ��������
    function add($id_client)
    {
        if(!is_numeric($id_client))
        {
            show_error("������������ �������� ��������");
        }

        $client = $this->model_common->select_one("client",array('id'=>$id_client));
        if(is_null($client))
        {
            show_error("������ ������� ���");
        }

        $this->header_block->set_title('���������� ��������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add($client['name'],'/adm/client/view/'.$id_client);
        $this->breadcrumbs_block->add('��������', DOMAIN .'/adm/campaign/client/'.$id_client);
        $this->breadcrumbs_block->add('����������','');
        $this->breadcrumbs_block->set();

        $this->load->model("model_channel");
        $this->smarty->assign("channels",$this->model_channel->select());

        $this->smarty->assign('campaign',0);
        $this->smarty->assign('client',$client);

        $this->load->model("model_channel");
        $this->smarty->assign("channel_list",$this->model_channel->select());

        $this->_display('campaign/campaign_add.tpl');

    }


    function edit($campaign_id)
    {
        if(!is_numeric($campaign_id))
        {
            show_error("Wrong id");
        }

        $campaign = $this->model_campaign->select_full_single($campaign_id);

        if(is_null($campaign))
        {
            show_error("����� �������� ���");
        }

        if(isset($campaign['settings']))
        {
            foreach($campaign['settings'] as $set)
            {
                if($set['id']=='limit_geo') $this->smarty->assign("geo_array",$this->model_channel->select_cities("city",0));
                if($set['id']=='limit_geo_state')   $this->smarty->assign("geo_array",$this->model_channel->select_cities("state",0));
                if($set['id']=='limit_geo_region')  $this->smarty->assign("geo_array",$this->model_channel->select_cities("region",0));
            }
        }

        $client = $this->model_common->select_one("client",array('id'=>$campaign['id_client']));

        $this->header_block->set_title('�������������� �������� '.$campaign['name']);
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->add($client['name'],'/adm/campaign/index/'.$client['id']);
        $this->breadcrumbs_block->add('��������', DOMAIN .'/adm/campaign/client/'.$client['id']);
        $this->breadcrumbs_block->add($campaign['name'],'');
        $this->breadcrumbs_block->set();

        $this->smarty->assign("campaign",$campaign);

        $this->load->model("model_client");
        $this->smarty->assign('client_list',$this->model_client->select_full_list());

        $this->load->model("model_channel");
        $this->smarty->assign("channel_list",$this->model_channel->select());
        $this->smarty->assign("setting_list",$this->model_channel->select_settings());

        $this->_display('campaign/campaign_add.tpl');

    }

    function save()
    {
        $data = $this->collector->campaign();

        if(isset($data['channels']))
        {
            $channels = $data['channels'];
            unset($data['channels']);
        }

        if(isset($data['settings']))
        {
            $settings = $data['settings'];
            unset($data['settings']);
        }

        // ������
        if(isset($data['id']))
        {
            $this->model_common->update("campaign", $data, array("id" => $data['id']));
            $id = $data['id'];

            // ��������� ������� - ����� ��������
            $campaign_object = $this->model_campaign->select_full_single($id);

            if($campaign_object['weight']!=$data['weight'])
            {
                // ������������� ����������� ���� �������� �������� �� ���� �����, ��� ��� ���������
                $this->load->model("model_zone");
                $this->load->model("model_banner");

                foreach($campaign_object['banners'] as $bannerzone)
                {
                    $this->model_banner->calculate_weight($bannerzone['id_banner'],$bannerzone['id_zone']);
                    // �� � ��� �� ���� ��������� �������� � ����
                    $zone_object = $this->model_zone->select_with_banners($bannerzone['id_zone'],1);
                    foreach($zone_object[0]['banners'] as $zonebanner)
                    {
                       $this->model_banner->calculate_weight($zonebanner['id_banner'],$bannerzone['id_zone']);
                    }
                }
            }
        }
        // ����������
        else
        {
            $id = $this->model_common->insert("campaign",$data);
            $data = array("code"=>md5(time().$id));
            $this->model_common->update("campaign", $data, array("id" => $id));
        }

        $this->load->model("model_channel");

        $this->model_channel->clear_public_channels($id,"campaign");

        if(isset($channels))
        {
            foreach($channels as $ch)
                $this->model_channel->add_channel_to_object($ch,$id,"campaign");
        }

        $this->model_channel->clear_personal_channel_settings($id,"campaign");

        if(isset($settings))
        {
            // ������ ���������, ����� ���������, ������ �� ��� �������� ����� � share = 0 , ����� ������� � ��������� � ����
            $this->model_channel->add_personal_channel($id,"campaign",$settings);
        }

        // ��������� ���
     //   $this->make_zones_cache();

        My_Url_Helper::redirect(DOMAIN .'/adm/campaign');
    }


    function view($id)
    {
        if(!is_numeric($id))
        {
            show_404("������������ �������� ��������");
        }

        $campaign = $this->model_campaign->select_full_single($id);

        if(is_null($campaign))
        {
            show_404("����� �������� ���");
        }

        $this->smarty->assign('campaign',$campaign);

        $this->header_block->set_title('�������� '.$campaign['name']);
        $this->header_block->set();

        $this->breadcrumbs_block->add('��������', DOMAIN .'/adm/campaign/');
        $this->breadcrumbs_block->add($campaign['name'],'');
        $this->breadcrumbs_block->set();

        $this->_display('campaign/campaign_item.tpl');

    }

    // �������� �������� ������������� �������� �������� � ����������� � ����� id_banner=0
    function delete($id)
    {
        $this->model_campaign->delete($id);
        My_Url_Helper::redirect($this->input->server('HTTP_REFERER'));
    }

    // �������� �������, ������ json ��������
    function ajax_list($id_client)
    {
     //   $list = $this->model_campaign->select_filled(array("c.id_client"=>$id_client));
        $list = $this->model_campaign->select_filled("c.id_client = ".$id_client." AND c.date_start <= CURDATE() AND (c.date_end > CURDATE() OR c.date_end IS NULL)");
        foreach($list as &$v)
        {
            foreach($v as &$vv)
                $vv = iconv('windows-1251','utf-8',$vv);
        }
        echo json_encode($list);
    }

}

?>