<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Banner extends Starter
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('model_banner');
        $this->admin_menu->set_active_section('banner');
        $this->admin_menu->set();

    }

    function index($page=1)
    {

        $data = array();

        if(isset($_GET['filter']))
        {
            $data = $this->collector->banner_filter();

            if(isset($data['client']))
            {
                $campaigns = $this->model_common->select("campaign",array("id_client"=>$data['client']),"id,name","id");
                $this->smarty->assign("campaigns",$campaigns);
                $this->smarty->assign('client',$this->model_common->select_one("client",array("id"=>$data['client'])));
            }

            if(isset($data['campaign']))
            {
                $banners = $this->model_common->select("banner",array("id_campaign"=>$data['campaign']),"id,name","id");
                $this->smarty->assign("banners",$banners);
                $this->smarty->assign('campaign',$this->model_common->select_one("campaign",array("id"=>$data['campaign'])));
            }

            if(isset($data['name']))
            {
                $this->smarty->assign('name',$data['name']);
            }

        }

        $offset = $page > 1 ? (($page - 1) * 10) : 0;
        $this->smarty->assign('offset',$offset);

        $res = $this->model_banner->select_by_page($page,$data);
        $banners = $res['data'];
        $this->smarty->assign('banner_list',$banners);

        $this->header_block->set_title('�������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������','');
        $this->breadcrumbs_block->set();

        $this->load->model("model_client");
        $clients = $this->model_client->select_filled();
        $this->smarty->assign("clients",$clients);

        $array_link = $this->drop_to_page($page, ceil($res['count'] / 10), "banner", $data);
        $this->smarty->assign("array_link", $array_link);

        $this->_display('banner/banner_index.tpl');
    }


    //  ������ ��������� ������� ���/����
    function power($id_banner)
    {
        if(!is_numeric($id_banner))
        {
            show_404("Error");
        }

        $banner = $this->model_banner->select_full_single($id_banner);

        if(is_null($banner))
        {
            show_404("������ ������� ���");
        }

        // ���� �������, ���������.
        if($banner['status'] == 1)
        {
            $data['status'] = 0;
            $this->model_common->update("banner",$data,array("id"=>$id_banner));

            // �� ���� �����, ��� ���� ������, �������� �� ����������� � ����������� ���������
            foreach($banner['zones'] as $zone)
            {
                // ������ ����
                $this->model_banner->set_zero_weight($banner['id'],$zone['id']);

                $zone_object = $this->model_zone->select_with_banners($zone['id']);

                if(isset($zone_object[0]['banners']))
                {
                    foreach($zone_object[0]['banners'] as $zonebanner)
                    {
                        if($zonebanner['id']!=$banner['id']) $this->model_banner->calculate_weight($zonebanner['id'],$zone['id']);
                    }
                }
            }

        }
        else
        {
            $data['status'] = 1;
            $this->model_common->update("banner",$data,array("id"=>$id_banner));

            // �� ���� �����, ��� ���� ������, ������������� �����������
            foreach($banner['zones'] as $zone)
            {
                // ������ ����
                $this->model_banner->calculate_weight($banner['id'],$zone['id']);

                $zone_object = $this->model_zone->select_with_banners($zone['id']);
                if(isset($zone_object[0]['banners']))
                {
                    foreach($zone_object[0]['banners'] as $zonebanner)
                    {
                        if($zonebanner['id']!=$banner['id']) $this->model_banner->calculate_weight($zonebanner['id'],$zone['id']);
                    }
                }
            }

        }

        My_Url_Helper::redirect($this->input->server('HTTP_REFERER'));

    }

    // ������� ������ � ��� ���� � ����������� ���������� ������
    function zone($id_banner)
    {
        if(!is_numeric($id_banner))
        {
            show_404("Error");
        }

        $this->load->model("model_site");
        $this->load->model("model_banner");

        $sites = $this->model_site->select_with_zones();

        $banner = $this->model_banner->select_full_single($id_banner);
        $this->smarty->assign("banner",$banner);

        $zones = array();

        foreach($banner['zones'] as $zone)
        {
            $zones[] = $zone['id'];
        }

        $this->smarty->assign('zones',$zones);

        // ��������-������ ����� �������� ������ � ����� ����
        $full = ($banner['type'] == "redirect" && sizeof($zones)) ? 1 : 0;
        $this->smarty->assign("full",$full);

        $this->header_block->set_title('������ '.$banner['name'].' - ����');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������',DOMAIN.'/adm/client');
        $this->breadcrumbs_block->add($banner['clientname'],DOMAIN.'/adm/client'.$banner['clientid']);
        $this->breadcrumbs_block->add('��������',DOMAIN.'/adm/campaign/index'.$banner['clientid']);
        $this->breadcrumbs_block->add($banner['campaignname'],'/adm/campaign/edit/'.$banner['campaignid']);
        $this->breadcrumbs_block->add('�������', DOMAIN.'/adm/banner/index/?filter&campaign='.$banner['campaignid']);
        $this->breadcrumbs_block->add($banner['name'], DOMAIN.'/adm/banner/edit/'.$banner['id']);
        $this->breadcrumbs_block->add('�������� � �����','');
        $this->breadcrumbs_block->set();

        $this->smarty->assign("site_list",$sites);

        $this->_display("banner/banner_zone.tpl");
    }

    // ������� ������� � ��������
    function add($id_campaign)
    {
        if(!is_numeric($id_campaign))
        {
            show_404("������������ �������� ��������");
        }

        $this->load->model("model_campaign");
        $campaign = $this->model_campaign->select_full_single($id_campaign);

        if(is_null($campaign))
        {
            show_404("����� �������� ���");
        }

        $this->header_block->set_title('���������� �������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������', DOMAIN.'/adm/client/');
        $this->breadcrumbs_block->add('��������', DOMAIN .'/adm/campaign/view/'.$id_campaign);
        $this->breadcrumbs_block->add($campaign['name'], DOMAIN .'/adm/campaign/view/'.$id_campaign);
        $this->breadcrumbs_block->add('���������� �������','');
        $this->breadcrumbs_block->set();

        $this->smarty->assign('banner',0);
        $this->smarty->assign('campaign',$campaign);

        $this->load->model("model_channel");
        $this->smarty->assign("channel_list",$this->model_channel->select());
        $this->smarty->assign("setting_list",$this->model_channel->select_settings());
        $this->_display('banner/banner_add.tpl');

    }

    // ����������� ����� ���������� �������
    function addraw()
    {
        $this->load->model("model_campaign");

        $this->header_block->set_title('���������� �������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('���������� �������','');
        $this->breadcrumbs_block->set();

        $this->smarty->assign('banner',0);
        $this->smarty->assign('campaign',0);

        $this->smarty->assign('campaign_list',$this->model_campaign->select_short_list());

        $this->load->model("model_channel");
        $this->smarty->assign("channel_list",$this->model_channel->select());
        $this->smarty->assign("setting_list",$this->model_channel->select_settings());

        $this->_display('banner/banner_add.tpl');

    }


    function edit($id)
    {
        if(!is_numeric($id))
        {
            show_404("Wrong id");
        }

        $banner = $this->model_banner->select_full_single($id);

        if(is_null($banner))
        {
            show_404("������ ������� ���");
        }

        $this->smarty->assign("banner",$banner);

        if(isset($banner['settings']))
        {
            foreach($banner['settings'] as $set)
            {
                if($set['id']=='limit_geo') $this->smarty->assign("geo_array",$this->model_channel->select_cities("city",0));
                if($set['id']=='limit_geo_state')   $this->smarty->assign("geo_array",$this->model_channel->select_cities("state",0));
                if($set['id']=='limit_geo_region')  $this->smarty->assign("geo_array",$this->model_channel->select_cities("region",0));
            }
        }

        $this->header_block->set_title('�������������� ������� '.$banner['name']);
        $this->header_block->set();

        $this->breadcrumbs_block->add('�������',DOMAIN."/adm/banner");
        $this->breadcrumbs_block->add($banner['name'],'');
        $this->breadcrumbs_block->set();

        $this->load->model("model_campaign");

        $campaign = $this->model_campaign->select_full_single($banner['id_campaign']);

        $this->smarty->assign('campaign',$campaign);
        $this->smarty->assign('campaign_list',$this->model_campaign->select_short_list());

        $this->load->model("model_channel");
        $this->smarty->assign("channel_list",$this->model_channel->select());
        $this->smarty->assign("setting_list",$this->model_channel->select_settings());

        $this->_display('banner/banner_add.tpl');

    }

    function save()
    {
        $data = $this->collector->banner();

        if(isset($data['channels']))
        {
            $channels = $data['channels'];
            unset($data['channels']);
        }

        if(isset($data['settings']))
        {
            $settings = $data['settings'];
            unset($data['settings']);
        }

        // ������
        if(isset($data['id']))
        {
            $this->model_common->update("banner", $data, array("id" => $data['id']));
            $id = $data['id'];
            $this->load->model("model_zone");
            // ��������� ����������� � ����������� �����
            $banner = $this->model_banner->select_full_single($data['id']);
            if(isset($banner['zones']))
            {
                foreach($banner['zones'] as $zone)
                {
                    // ������ ������
                    $this->model_banner->calculate_weight($data['id'],$zone['id']);
                    // ���� ��������� �������� �� ���� ��������� ����� ����������� ���� ��������
                    $zone_object = $this->model_zone->select_with_banners($zone['id']);

                    if(isset($zone_object[0]['banners']))
                    {
                        foreach($zone_object[0]['banners'] as $zonebanner)
                        {
                             $this->model_banner->calculate_weight($zonebanner['id'],$zone['id']);
                        }
                    }
                }
            }
        }

        // ����������
        else
        {
            $id = $this->model_common->insert("banner",$data);
        }

        $this->load->model("model_channel");

        $this->model_channel->clear_public_channels($id,"banner");

        if(isset($channels))
        {
            foreach($channels as $ch)
                $this->model_channel->add_channel_to_object($ch,$id,"banner");
        }

        $this->model_channel->clear_personal_channel_settings($id,"banner");

        if(isset($settings))
        {
            // ������ ���������, ����� ���������, ������ �� ��� �������� ����� � share = 0 , ����� ������� � ��������� � ����
            $this->model_channel->add_personal_channel($id,"banner",$settings);
        }

        // ��������� ���
   //     $this->make_zones_cache();

        My_Url_Helper::redirect(DOMAIN.'/adm/banner');
    }

   function copy($id)
   {
       $banner = $this->model_common->select_one("banner",array("id"=>$id));
       unset($banner['id']);
       $data = $banner;
       $this->model_common->insert('banner',$data);
       My_Url_Helper::redirect(DOMAIN.'/adm/banner');
   }

    function delete($id)
    {
        $this->model_banner->delete($id);
        My_Url_Helper::redirect($this->input->server('HTTP_REFERER'));
    }

    // �������� ����� ���� ��������� - ����, ������, ��� �������� - ������� ��� ������� ��������
   public function ajax_setzone()
   {
       $post = $this->input->post();
       if(!is_numeric($post['type']) || !is_numeric($post['banner']) || !is_numeric($post['zone'])) {
           show_404("�� ���������� ������");
       }
       if($this->model_banner->setzone($post['type'],$post['zone'],$post['banner'])) echo "ok";
   }

    // �������� ��������, ������ json ��������
    public function ajax_list($id_campaign)
    {
        // ���� ������ ������ ��, ������� ��� �� ��������� � ���� ����

        $list = $this->model_common->select("banner",array("id_campaign"=>$id_campaign),'id,name',"name");
        foreach($list as &$v)
        {
            foreach($v as &$vv)
                $vv = iconv('windows-1251','utf-8',$vv);
        }
        echo json_encode($list);
    }


}

?>