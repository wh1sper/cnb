<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Dashboard extends Starter
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('model_site');

        $this->admin_menu->set_active_section('dashboard');
        $this->admin_menu->set();

    }

    function index()
    {
        $this->header_block->set_title('������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('������', DOMAIN.'/adm/site/');
        $this->breadcrumbs_block->set();

        // ������ ������ ������ �� ���� ������ - ���������� ����, ��������, ������, �������
        $list = $this->model_site->select_full_list(1);

        $this->smarty->assign("site_list",$list);
        $this->_display("dashboard/dashboard_index.tpl");
    }



}

?>