<?php

class Starter extends CI_Controller
{

	var $section_not_show_id = "'0'"; // ����� ������� �������

    function __construct()
    {
        parent::__construct();

        $this->smarty = &$this->smarty_tpl;
        $this->smarty->assign("DOMAIN",DOMAIN);

        $this->db = $this->load->database('default', TRUE);

        $this->load->model("model_common");
        $this->load->library(array('collector','header_block','breadcrumbs_block','admin_menu'));
        $this->load->helper('my_url_helper');

        //��������� ��������� - ��� ������ �����������.
        Header("Expires: Thu, 19 Feb 1998 13:24:18 GMT");
        Header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        Header("Cache-Control: no-cache, must-revalidate");
        Header("Cache-Control: post-check=0,pre-check=0");
        Header("Cache-Control: max-age=1");
        Header("Pragma: no-cache");

        ini_set('session.gc_maxlifetime', 1209600);
        ini_set('session.cookie_lifetime', 1209600);
        //ini_set('session.cookie_domain','.cnews.ru');

        session_start();

		//$this->output->enable_profiler(TRUE);

        // ��������� ������ ������� ������
        foreach ($_REQUEST as &$val)
        {
            if (!is_array($val))
            {
                $val = htmlspecialchars($val, ENT_QUOTES);
                $val = str_replace('\\', '\\\\', $val);
            }
            else
            {
                foreach ($val as &$val2)
                {
                    if (!is_array($val2) and substr_count($val2, "<xjxquery>") == 0)
                    {
                        $val2 = htmlspecialchars($val2, ENT_QUOTES);
                        $val2 = str_replace('\\', '\\\\', $val2);
                    }
                    // xajax �������� ������ � �������� ��������, ������� ��� ������ ��������
                    // htmlspecialchars ������. ����� ��� ��������. �� �� ����� ���������� ��� ������
                    // �������� xajax � �������� �� addslashes
                    elseif (!is_array($val2) and substr_count($val2, "<xjxquery>"))
                    {
                        $val2 = addslashes($val2);
                    }
                }
            }
        }

    }

    // ��� ������� ���������� �� �� ������ ����� ������, � ����� ��� �������
    // ��� �������� ������� ����� �������� ����� �����. ��������, ��� ������ ������.
    function _display($tpl, $a_assign = array())
    {
        if (empty($tpl))   show_404('page');

        if (!empty($a_assign))
        {
            foreach ($a_assign as $key => $val)
            {
                $this->smarty->assign($key, $val);
            }
        }

        $this->smarty->display($tpl);

		$this->output->_display(); // ����� �� codeigniter

		exit;
    }


    // ������� ��������� ������ ���������� ������ �����������
    // �� ��������: << -1- -2- >>
    // $n_this_page - ����� ������� ��������
    // $n_pages - ����� �������
    // $controller_name - ��� ����������� (��� �������� �����������)
    // $getdata - ������ �� get ��� ������������ ������
    // � ����� ������� ���� ������������. ����  $set_metod = false, �� ������������
    // ����� �� ��������� index().
    function drop_to_page($n_this_page, $n_pages, $controller_name, $getdata)
    {

        $controller_name = $controller_name."/index";

        if ($n_pages <= 1) return 0;

        $d = 5; // �������� ������
        $start = 1;
        $stop = 2*$d;

        if (($n_this_page - $d) > 1)
        {
            $start = $n_this_page - $d;
            $stop = $n_this_page + $d;
        }

        if ($stop > $n_pages)
        {
            $stop = $n_pages;
            $start = $n_pages - 2*$d;
        }

        if ($start < 1) $start = 1;

        $getstr = array("?filter=1");
        foreach($getdata as $k=>$v)
        {
            $getstr[] = $k."=".$v;
        }
        $getstr = join("&",$getstr);

        // ������� �� ������ ��������
        if ($n_this_page > 1)
        {
            $array_link[] = "<A HREF='/adm/$controller_name/1/$getstr'> << </A>";
        }
        else $array_link[] = " << ";

        // ����� ������ ������� �� $start � ���������� $stop
        for ($i = $start; $i <= $stop; $i++)
        {
            if ($i != $n_this_page)
            {
                $array_link[] = "<A HREF='/adm/$controller_name/$i/$getstr'> $i </A>";
            } else $array_link[] = " $i ";
        }

        // ������� �� ��������� ��������
        if ($n_this_page < $n_pages)
        {
            $array_link[] = "<A HREF='/adm/$controller_name/$n_pages/$getstr'> >> </A>";
        }
        else $array_link[] = " >> ";

        return $array_link;
    }

    // ��������/���������� ���� �������� ��� � �����
    public function make_zones_cache()
    {
        $zones = $this->db->select("id_zone")->from("banner_zone")->distinct()->get()->result_array();
        if(!sizeof($zones)) exit;

        foreach($zones as $zn)
        {

            $zone = $zn['id_zone'];

            // 1. ������� ������������ ������� � �� ��������� � ��������
            $banners_total['exclusive'] = $this->model_banner->select_by_zone($zone,"exclusive");
            $banners_total['regular'] = $this->model_banner->select_by_zone($zone,"regular");

            $array = array("exclusive"=>array(),"regular"=>array());

            foreach($banners_total as $type=>&$banners)
            {
                foreach($banners as &$v)
                {

                    // �������� ��� ���-���
                    if(!is_null($v['width']) && $v['width'])
                    {
                        $v['margin']['left'] = $v['width'] / 2;
                        $v['margin']['top'] = $v['height'] / 2;
                    }

                    // ��� swf ������� ���������� flashvars
                    if($v['type']=='swf' || $v['type']=='popup')
                    {
                        $link = urlencode("http://cnb.cnews.ru/click.php?zone=".$zone."&banner=".$v['id']);
                        $v['flashvars'] = str_replace("#link",$link,$v['flashvars']);
                    }

                }
            }

            file_put_contents("../cache/cache_zones/".$zone.".txt",base64_encode(serialize($banners_total)));

        }
    }


}