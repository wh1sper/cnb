<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:06
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Channel extends Starter
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('model_channel');

        $this->admin_menu->set_active_section('channel');
        $this->admin_menu->set();

    }

    function index()
    {

        $this->header_block->set_title('������ ����������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('������ ����������', DOMAIN.'/adm/channel/');
        $this->breadcrumbs_block->set();

        $data = $this->model_channel->select();
        $this->smarty->assign("channel_list",$data);

        $this->_display("channel/channel_index.tpl");
    }

    // ������� ��� �������� �������, ����� � ������� �������
    function addraw()
    {
        $this->header_block->set_title('�������� ����� ����������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('������ ����������', DOMAIN.'/adm/channel/');
        $this->breadcrumbs_block->add('�������� ����� ����������','');

        $this->breadcrumbs_block->set();

        $this->smarty->assign('cities',$this->model_channel->select_cities());

        $setting_list = $this->model_channel->select_settings();
        $this->smarty->assign("setting_list",$setting_list);

        $this->_display('channel/channel_add.tpl');
    }

    // ������� � ������� ��������� ��������
    function add()
    {

        $this->_display('campaign/campaign_add.tpl');

    }


    function edit($channel_id)
    {
        if(!is_numeric($channel_id))
        {
            show_erorr("Wrong id");
        }

        $channel = $this->model_channel->select($channel_id);

        if(!sizeof($channel))
        {
            show_error("������ ������ ���");
        }

        $channel = $channel[0];

        if(isset($channel['settings']))
        {
            foreach($channel['settings'] as $set)
            {
                if($set['id']=='limit_geo') $this->smarty->assign("geo_array",$this->model_channel->select_cities("city",0));
                if($set['id']=='limit_geo_state')   $this->smarty->assign("geo_array",$this->model_channel->select_cities("state",0));
                if($set['id']=='limit_geo_region')  $this->smarty->assign("geo_array",$this->model_channel->select_cities("region",0));
            }
        }

        $this->header_block->set_title('������������� ����� ����������');
        $this->header_block->set();

        $this->breadcrumbs_block->add('������ ����������', DOMAIN.'/adm/channel/');
        $this->breadcrumbs_block->add($channel['name'],'');

        $this->breadcrumbs_block->set();

        $this->smarty->assign('channel',$channel);
        $this->smarty->assign('cities',$this->model_channel->select_cities());

        $setting_list = $this->model_channel->select_settings();
        $this->smarty->assign("setting_list",$setting_list);

        $this->_display('channel/channel_add.tpl');

    }

    function save()
    {
        $data = $this->collector->channel();

        if(sizeof($data['settings']))
        {
            $settings = $data['settings'];
            unset($data['settings']);
        }

        // ������
        if(isset($data['id']))
        {
            $this->model_common->update("channel",$data,array("id"=>$data['id']));
            $this->model_channel->clear_settings($data['id']);
            $id = $data['id'];
        }
        else
        {
            $id = $this->model_common->insert("channel",$data);

        }

        if(isset($settings))
        {
            foreach($settings as $k=>$v)
                $this->model_channel->add_setting_to_channel($id,$v['id'],$v['value'],$v['type']);
        }

        My_Url_Helper::redirect(DOMAIN .'/adm/channel');
    }

    function view($id)
    {
        if(!is_numeric($id))
        {
            show_404("������������ �������� ��������");
        }


        $this->_display('channel/chanel_item.tpl');

    }


    // �������� �������� ������������� �������� �������� � ����������� � ����� id_banner=0
    function delete($id)
    {
        $this->model_channel->delete($id);
        My_Url_Helper::redirect($this->input->server('HTTP_REFERER'));
    }

    // ��� ������ ������� ���� ����� ��� ���������� ���������
    function get_setting_field()
    {
        $name = $_GET['name'];
        $id = $_GET['id'];

        switch($name) {
            case "limit_days" : { $tpl = "blocks/field_days_utf.tpl"; break; }
            case "limit_time" : { $tpl = "blocks/field_time.tpl"; break; }
            case "limit_mobile" : { $tpl = "blocks/field_mobile_utf.tpl"; break; }
            case "keyword" : { $tpl = "blocks/field_text.tpl"; break; }
            case "limit_geo" :
            {
                $tpl = "blocks/field_geo.tpl";
                $cities = $this->model_channel->select_cities("city");
                $this->smarty->assign('geo_array',$cities);
                break;
            }
            case "limit_geo_state" :
            {
                $tpl = "blocks/field_geo.tpl";
                $cities = $this->model_channel->select_cities("state");
                $this->smarty->assign('geo_array',$cities);
                break;
            }
            case "limit_geo_region" :
            {
                $tpl = "blocks/field_geo.tpl";
                $cities = $this->model_channel->select_cities("region");
                $this->smarty->assign('geo_array',$cities);
                break;
            }
            default: { $tpl = "blocks/field_default.tpl"; break; }
        }

        $this->smarty->assign('id',$id);
        $this->smarty->assign('key',$id);

        $this->_display($tpl);
    }

    function ajax_list_settings()
    {
        $list = $this->model_channel->select_settings();
        foreach($list as &$v)
        {
            foreach($v as &$vv)
                $vv = iconv('windows-1251','utf-8',$vv);
        }
        echo json_encode($list);
    }

    function ajax_list()
    {
        $list = $this->model_channel->select();
        foreach($list as &$v)
        {
            foreach($v as &$vv)
                $vv = iconv('windows-1251','utf-8',$vv);
        }
        echo json_encode($list);
    }

}

?>