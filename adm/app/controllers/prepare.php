<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 16:03
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once (CPPATH . "/starter.php");

class Prepare extends Starter
{

    public function index()
    {

        // �������� ���� ���� ���, ������� �� ������� ��������� �� ���������� �������

        $this->load->model("model_banner");

        $this->make_zones_cache();

        // �������� ���� ������ ������
        $sites = $this->db->select("*")->from("site")->order_by("id")->get()->result_array();
        file_put_contents("../cache/sites.txt",base64_encode(serialize($sites)));

        $this->load->model("model_stat");

        // ������� ����� ���������� �� ������ � ����, ��������� �� �������, �.�. � ���� ���� ������
        $logfiles = scandir("../cache/impressions");
        unset($logfiles[0]);
        unset($logfiles[1]);
        foreach($logfiles as $log)
        {
            if(date("i")!=$log)
            {
                $data = file("../cache/impressions/".$log);
                foreach($data as $impression)
                {
                    $impression_data = explode(";",$impression);
                    if(!empty($impression_data[3]) && !empty($impression_data[2]))
                        $this->model_stat->add_stat($impression_data[2],$impression_data[3],"impressions",$impression_data[0],$impression_data[1]);
                }

                unlink("../cache/impressions/".$log);
            }
        }

    }
}

?>