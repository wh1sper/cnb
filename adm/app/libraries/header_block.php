<?php


/**
 * ����� ��� ��������� ����� �����
 * @author ashmits by 05.12.2012 16:34
 *
 */

class Header_Block
{
	
	private $title = null;
	private $js = array();
	private $css = array();
	
	private $css_compile_file = null;
	private $js_compile_file = null;
	
	private $css_version = null;
	private $js_version = null;
	
	private $iframe = false;
	
	private $CI = null;
	
	public function __construct()
	{
		$this->CI = &get_instance();
		//�������������� ��������� ����� � �������
		$this->css = $this->CI->config->item('css_admin');
		$this->js = $this->CI->config->item('js_admin');
		
		$this->css_version = sha1(TEMPLATE_VERSION);
		$this->js_version = sha1(TEMPLATE_VERSION);
	}
	
	public function set_iframe($value)
	{
		$this->iframe = (bool) $value;
	}
	
	public function set_title($title)
	{
		$this->title = (string)$title;
	}
	
	
	public function get_title()
	{
		return $this->title;
	}
	
	
	public function set_js($js, $clear=false)
	{
		if ($clear)
		{
			$this->js = $js;
		}
		else
		{
			$this->js = array_merge($this->js, $js);
		}
	}
	
	
	public function get_js()
	{
		
		/*$content = @file_get_contents(JS_ROOT . 'image_upload_wysi.js');
		if (preg_match_all("/\/\/.+?\n/s", $content, $matches))
		{
			print_r($matches);
		}
		exit();*/
		
		if (!$this->CI->config->item('jscss_cache_enable'))
		{
			foreach ($this->js as $key => $val)
			{
				$this->js[$key] = str_replace(JS_ROOT, DOMAIN.'/adm/inc/js/', $this->js[$key]);
			}
			return $this->js;
		}
		
		$this->js_compile_file = str_replace("__file__", (string)sha1(TEMPLATE_VERSION . md5( implode(',', array_values( $this->js ) ) ) ) . ".js", JS_COMPILE_FILE);
		//������ ��������� �� ������� ���� ���������� ������
		$js_header = "JavaScript files includes by " . date("Y-m-d H:i").":\n" . implode("\n", array_values( $this->js ) ) . "\n\n";
		$js_header = preg_replace("/\/www.*?\/inc.*?/i", DOMAIN."/inc$2", $js_header);
		//������� ��������� � ��������
		$js_header = "/*" . $js_header . "*/";
		
		if (!file_exists(ROOT . $this->js_compile_file))
		{
			
			@system("touch " . ROOT . $this->js_compile_file);
			@chmod(ROOT . $this->js_compile_file, 0777);
			@file_put_contents(ROOT . $this->js_compile_file, $js_header , FILE_APPEND);
			foreach ($this->js as $js)
			{
				
				$js_file_content = @file_get_contents($js);
				$js_file_content = iconv('windows-1251', 'utf-8', $js_file_content);
				
				//$js_file_content = preg_replace("/\/\*.+?\*\//s","", $js_file_content);
				//$js_file_content = preg_replace("/\/\/(?!').+?\n/s", "", $js_file_content);
				//$js_file_content = preg_replace("/([^\pL\pN\pP\pS\pZ])|([\xC2\xA0])/u", "", $js_file_content);
				
				$js_file_content = iconv('utf-8', 'windows-1251', $js_file_content);
				
				@file_put_contents(ROOT . $this->js_compile_file, "\n" , FILE_APPEND);
				@file_put_contents(ROOT . $this->js_compile_file, $js_file_content , FILE_APPEND);
				
			}
			
		}
		
		return DOMAIN . $this->js_compile_file;
		
	}
	
	
	public function set_css($css, $clear = false)
	{
		if ($clear)
			$this->css = (array)$css;
		else
			$this->css = array_merge($this->css, (array)$css);
	}
	
	
	public function get_css()
	{
		
		if ($this->iframe)
		{
			$this->set_css($this->CI->config->item('css_admin_iframe'), true);
		}
		
		if (!$this->CI->config->item('jscss_cache_enable'))
		{
			
			foreach ($this->css as $key=>$val)
			{
				$this->css[$key] = str_replace(CSS_ROOT, DOMAIN.'/adm/inc/css/', $this->css[$key]);
			}
			
			return $this->css;
		}
		
		
		$this->css_compile_file = str_replace("__file__", (string)sha1(TEMPLATE_VERSION . md5( implode(',', array_values( $this->css ) ) ) ) . ".css", CSS_COMPILE_FILE); 
		$css_header = "CSS files includes by ".date("Y-m-d H:i").":\n".implode("\n", array_values( $this->css ) ) . "\n";
		$css_header = preg_replace("/\/www.*?\/inc.*?/i", DOMAIN."/inc$2", $css_header);
		
		//������� ��������� � ��������
		$css_header = "/*" . $css_header . "*/\n";
		
		if (!file_exists(ROOT . $this->css_compile_file))
		{
			
			@system("touch " . ROOT . $this->css_compile_file);
			@chmod(ROOT . $this->css_compile_file, 0777);
			@file_put_contents(ROOT . $this->css_compile_file, $css_header , FILE_APPEND);
			foreach ($this->css as $css)
			{
				$css_content = @file_get_contents($css);
				//������� ���������� �������
				$css_content = preg_replace("/\n/", "", $css_content);
				$css_content = preg_replace("/\t/", "", $css_content);
				$css_content = preg_replace("/\/\*.+?\*\//s", "", $css_content);
				
				@file_put_contents(ROOT . $this->css_compile_file, $css_content , FILE_APPEND);
			}
			
		}
		
		return DOMAIN . $this->css_compile_file;
		
	}
	
	
	/**
	 * �������� ������ � ������������
	 */
	public function set()
	{
		$this->get_css();
		$this->get_js();
		
		$this->CI->smarty->assign('header_block', $this);
		$this->CI->smarty->assign('config', $this->CI->config);
		$this->CI->smarty->assign('DOMAIN', DOMAIN);
		
	}
	
	
	private function clear_files($path)
	{
		
		$dir = opendir($path);
		while(false !== ($file = readdir($dir)))
		{
			if ($file != '.' and $file != '..')
			{
				@unlink($path.$file);
			}
		}
		
		closedir($dir);
		
	}
	
}

?>