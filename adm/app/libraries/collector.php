<?php
/**
 * Created by PhpStorm.
 * User: dbokov
 * Date: 17.10.14
 * Time: 18:18
 */

require_once("validate.php");

class Collector extends Validate
{
    private $data = array();
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('my_text');
        $this->CI->load->library("input");

    }

    public function client()
    {
        $post = $this->CI->input->post();
        $this->data['name'] = $this->set_text($post['name'],true,'������� ��� �������');
        $this->data['phone'] = $this->set_text($post['phone']);
        $this->data['addit'] = $this->set_text($post['addit']);
        $this->data['email'] = $this->set_email($post['email']);
        if(isset($post['id'])) $this->data['id'] = $this->set_int($post['id']);
        return $this->data;
    }

    public function banner()
    {
        $post = $this->CI->input->post();
        $this->data['type'] = $this->set_enum($post['banner_type'],'popup,redirect,image,swf,text,html');
        $this->data['name'] = $this->set_text($post['name'],true,"������� �������� �������");
        $this->data['content'] = $this->set_text($post['content'],true,"������� ������� �������");
        $this->data['link'] = $this->set_text($post['link'],true,"������� ������");
        $this->data['weight'] = $this->set_int($post['weight']);
        $this->data['insert_after'] = $this->set_text($post['insert_after']);
        $this->data['insert_before'] = $this->set_text($post['insert_before']);
        $this->data['id_campaign'] = $this->set_int($post['id_campaign']);
        $this->data['width'] = $this->set_text($post['width']);
        $this->data['height'] = $this->set_int($post['height']);
        $this->data['flashvars'] = $this->set_flashvars($post['flashvars']);
        $this->data['is_blank'] = $this->set_int($post['is_blank']);
        $this->data['flash_image'] = $this->set_text($post['flash_image']);
        $this->data['popup_type'] = $this->set_enum($post['popup_type'],'center,bottom');

        if(!isset($post['id']))
        {
            $this->data['date_add'] = date("Y-m-d H:i:s");
        }
        else
        {
            $this->data['id'] = $this->set_int($post['id']);
        }

        // �������� � �������
        if(isset($post['channel']))
        {
            foreach($post['channel'] as $channel)
            {
                $this->data['channels'][] = $channel;
            }
        }


        // ������������ ��������� - ��������� ��������� ����� ��� ��������
        if(isset($post['setting']))
        {
            foreach($post['setting'] as $k=>$v)
            {
              //  $this->data['settings'][$v]['value'] = $post['value'][$k];
             //   $type = isset($post['type'][$k]) ? $post['type'][$k] : "and";
           //     $this->data['settings'][$v]['type'] = $type;

                $type = isset($post['type'][$k]) ? $post['type'][$k] : "and";
                $setitem['value'] =  $post['value'][$k];
                $setitem['type'] = $type;
                $setitem['id'] = $v;

                $this->data['settings'][] = $setitem;
            }
        }

        return $this->data;

    }

    public function campaign()
    {
        $post = $this->CI->input->post();
        $this->data['name'] = $this->set_text($post['name'],true,'������� �������� ��������');
        $this->data['descr'] = $this->set_text($post['descr']);
        $this->data['weight'] = $this->set_int($post['weight']);
        $this->data['type'] = $this->set_enum($post['campaign_type'],"regular,exclusive");
        $this->data['date_start'] = empty($post['date_start']) ? date("Y-m-d H:i:s") : $this->set_date($post['date_start']);
        $this->data['date_end'] = empty($post['date_end']) ? null : $this->set_date($post['date_end']);
        $this->data['id_client'] = $this->set_int($post['id_client']);

        if(isset($post['id']))  $this->data['id'] = $this->set_int($post['id']);
        $id = isset($this->data['id']) ? $this->data['id'] : 0;

        // �������� � �������
        if(isset($post['channel']))
        {
            foreach($post['channel'] as $channel)
            {
                $this->data['channels'][] = $channel;
            }
        }
        // ������������ ��������� - ��������� ��������� ����� ��� ��������
        if(isset($post['setting']))
        {
            foreach($post['setting'] as $k=>$v)
            {
                //$this->data['settings'][$v]['value'] = $post['value'][$k];
                //$this->data['settings'][$v]['type'] = $type;

                $type = isset($post['type'][$k]) ? $post['type'][$k] : "and";
                $setitem['value'] =  $post['value'][$k];
                $setitem['type'] = $type;
                $setitem['id'] = $v;
                $this->data['settings'][] = $setitem;

            }
        }


        return $this->data;
    }

    public function site()
    {
        $post = $this->CI->input->post();

        $this->data['name'] = $this->set_text($post['name'],true,'������� �������� �����');
        $this->data['url'] = $this->set_text($post['url']);

        if(!isset($post['id']))
        {
            $this->data['date_add'] = date("Y-m-d");
        }
        else
        {
            $this->data['id'] = $this->set_int($post['id']);
        }

        return $this->data;
    }

    public function zone()
    {
        $post = $this->CI->input->post();
        $this->data['name'] = $this->set_text($post['name'],true,'������� �������� ����');
        $this->data['descr'] = $this->set_text($post['descr']);
        $this->data['insert_after'] = $this->set_text($post['insert_after']);
        $this->data['insert_before'] = $this->set_text($post['insert_before']);
        $this->data['type'] = $this->set_text($post['type']);
        $this->data['id_site'] = $this->set_int($post['id_site']);
        if(isset($post['id'])) $this->data['id'] = $this->set_int($post['id']);

        return $this->data;
    }

    public function banner_filter()
    {
        $get = $this->CI->input->get();
        if($get['client']) $this->data['client'] = $get['client'];
        if(isset($get['campaign']) && $get['campaign']) $this->data['campaign'] = $get['campaign'];
        if($get['name']) $this->data['name'] = $get['name'];
        return $this->data;
    }


    public function campaign_filter()
    {
        $get = $this->CI->input->get();
        if($get['client']) $this->data['client'] = $get['client'];
        if($get['name']) $this->data['name'] = $get['name'];
        return $this->data;
    }

    public function client_filter()
    {
        $get = $this->CI->input->get();
        if($get['name']) $this->data['name'] = $get['name'];
        return $this->data;
    }

    public function stat()
    {
        $get = $this->CI->input->get();

        if(isset($get['period']))
        {
            $date_to = date("Y-m-d");

            if($get['period']=='month')
            {
                $sec_from = time() - 2592000;
                $date_from = date("Y-m-d",$sec_from);
            }
            if($get['period']=='week')
            {
                $sec_from = time() - 604800;
                $date_from = date("Y-m-d",$sec_from);
            }
            if($get['period']=='custom')
            {
                $date_from = $get['date_from'];
                $date_to =  $get['date_to'];
            }

            $this->data['period'] = $get['period'];
            $this->data['date_from'] = $date_from;
            $this->data['date_to'] = $date_to;
        }

        if($get['site']) $this->data['site'] = $get['site'];
        if($get['client']) $this->data['client'] = $get['client'];

        if(isset($get['zone']) && $get['zone']) $this->data['zone'] = $get['zone'];
        if(isset($get['campaign']) && $get['campaign']) $this->data['campaign'] = $get['campaign'];
        if(isset($get['banner']) && $get['banner']) $this->data['banner'] = $get['banner'];

        return $this->data;

    }


    public function channel()
    {
        $post = $this->CI->input->post();
        $this->data['name'] = $this->set_text($post['name'],true,"������� �������� ������");

        if(isset($post['id'])) $this->data['id'] = $this->set_int($post['id']);

        if(isset($post['setting']))
        {
            foreach($post['setting'] as $k=>$v)
            {
//               $this->data['settings'][$v]['value'] = $post['value'][$k];
  //             $this->data['settings'][$v]['type'] = $type;
                $type = isset($post['type'][$k]) ? $post['type'][$k] : "and";
                $setitem['value'] =  $post['value'][$k];
                $setitem['type'] = $type;
                $setitem['id'] = $v;
                $this->data['settings'][] = $setitem;
            }
        }

        $this->data['share'] = 1;

        return $this->data;
    }




}