{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}


<div class="wrapper">
    <div class="content">

        {if $client}
            <div class="infoplace">
                <b>������:</b> <a href="{$DOMAIN}/adm/campaign/index?filter&client=/{$client.id}">{$client.name}</a><br>
            </div>
        {/if}

        <h1>��������</h1>

        <a href="{$DOMAIN}/adm/campaign/addraw" style='margin-top:20px;' class="btn btn-success"><i class="icon-plus"></i> �������� ��������</a>


        <div class="navbar" style='clear:both; margin-top:20px;'>
            <div class="navbar-inner">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">
                    <select id="client" style="width:300px !important;" name="client">
                        <option value="0">��� �������</option>
                        {foreach from=$clients item=cli}
                            <option {if $client.id eq $cli.id} selected {/if} value="{$cli.id}">{$cli.name}</option>
                        {/foreach}
                    </select>

                    <input type="text" style="width:300px !important;" name="name" placeholder="��������" value="{$name}"/>

                    <button class="btn btn-primary" name="filter" type="submit">�����������</button>

                </form>
            </div>
        </div>

        {if !empty($campaign_list)}
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>��������</th><th>������</th><th>�����������</th><th>�������</th><th>������/<b>�����</b>/<i>CTR</i></th><th>��������</th></thead>
                </thead>
                {foreach from=$campaign_list key=key item=item}
                    <tr>
                        <td>{$item.id}</td>
                        <td>
                            <a href="{$DOMAIN}/adm/campaign/edit/{$item.id}">{$item.name}</a>
                            <div class="stat_external">
                                <a href="{$DOMAIN}/stat.php?code={$item.code}">{$DOMAIN}/stat.php?code={$item.code}</a>
                            </div>
                        </td>
                        <td><a href="{$DOMAIN}/adm/campaign/index?filter&client={$item.id_client}">{$item.clientname}</a></td>

                        <td style="max-width:250px; font-size:12px;">
                            {if isset($item.settings)}
                                {foreach from=$item.settings key=key item=setting}
                                    <div style="font-size:12px;">
                                        {if $key > 0 && $key}
                                            <span style="color:#44ba28; font-weight: bold;">
                                                {if $setting.type eq "and"}�{/if}
                                                {if $setting.type eq "or"}���{/if}
                                            </span>
                                        {/if}
                                        <i>{$setting.label}</i>
                                        {if $setting.id neq "keyword"}
                                             -
                                            {if $setting.value|is_array}
                                                {', '|implode:$setting.value}
                                            {else}
                                                {$setting.value}
                                            {/if}
                                        {/if}
                                    </div>
                                {/foreach}
                            {/if}

                            {if isset($item.channels)}
                                <div style="margin-top:3px; padding:2px; background:#f4f4f6;">
                                    <b>������ ����������</b><br>
                                    {foreach from=$item.channels item=ch}
                                        <a href='{$DOMAIN}/adm/channel/edit/{$ch.id}'>{$ch.name}</a><br>
                                    {/foreach}
                                </div>
                            {/if}

                        </td>
                        <td>
                            {if $item.cnt_banners}
                                <a href="{$DOMAIN}/adm/banner/index/?filter&campaign={$item.id}"><i class="icon-list"></i> ������ (����� {$item.cnt_banners})</a><br>
                            {else}
                                -
                            {/if}
                        </td>
                        <td>
                            <div style='padding:3px;'>
                                {if $item.stat}
                                    {$item.stat.impressions} / <b>{$item.stat.clicks}</b> / <i>{$item.stat.ctr}</i>
                                {else}
                                    -
                                {/if}
                            </div>
                        </td>
                        <td>
                            <div class="edit_links">
                                <div><a href="{$DOMAIN}/adm/banner/add/{$item.id}"><i class="icon-plus"></i> �������� ������</a></div>
                                <div><a href="{$DOMAIN}/adm/campaign/edit/{$item.id}"><i class="icon-edit"></i> ������������� ��������</a></div>
                                <div><a class='confirmation' href="{$DOMAIN}/adm/campaign/delete/{$item.id}"><i class="icon-remove"></i> <span style="color:#a60018">������� �������� </span> </a></div>
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>

            {if $array_link}<div style="margin: 8px; "><B>��������:  {foreach name=outer item=item from=$array_link}{$item}{/foreach}</B></div>{/if}

        {else}
            <p>�� ����� �������� �� ��������� � �������.</p>
        {/if}



    </div>
</div>

{include file="blocks/footer_block.tpl"}