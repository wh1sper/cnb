{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

<div class="wrapper">
    <div class="content">

        <form action="{$DOMAIN}/adm/client/save" method="post" class="add_form form-new" id="client_add">

            {if ($client)}<input type='hidden' name='id' value='{$client.id}'/>{/if}

            <fieldset>
                <legend>{if $client}�������������� ������� {$client.name}{else}���������� �������{/if}</legend>

                <div class="control-group">
                    <label class="control-label" for="name">���</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" required="required" name="name" id="name" value="{$client.name|strip_tags}" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name">E-mail</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="email" id="email" value="{$client.email|strip_tags}" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name">�������</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="phone" id="phone" value="{$client.phone|strip_tags}" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">�������������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="addit" rows="5" id="addit">{$client.addit}</textarea>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">{if $client}<i class="icon-ok"></i> �������� �������{else}<i class="icon-plus"></i> �������� �������{/if}</button>

            </fieldset>
        </form>

    </div>
</div>

{include file="blocks/footer_block.tpl"}
