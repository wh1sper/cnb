{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}


<div class="wrapper">
    <div class="content">
        <h1>�������</h1>


        <a href="/adm/client/add" style='margin-top:20px;' class="btn btn-success"><i class="icon-plus"></i> �������� �������</a>


        <div class="navbar" style='clear:both; margin-top:20px;'>
            <div class="navbar-inner">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">

                    <input type="text" style="width:300px !important;" name="name" placeholder="��������" value="{$name}"/>

                    <button class="btn btn-primary" name="filter" type="submit">�����������</button>

                </form>
            </div>
        </div>

        {if !empty($client_list)}
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>������</th><th>��������</th><th>������� (�����/��������)</th><th>������/<b>�����</b>/<i>CTR</i></th><th>��������</th></thead>
                </thead>
                {foreach from=$client_list key=key item=item}
                    <tr>
                        <td>{$item.id}</td>
                        <td><a href="{$DOMAIN}/adm/client/edit/{$item.id}">{$item.name}</a></td>
                        <td style="padding-right: 0px;">
                            {foreach from=$item.campaigns item=campaign}
                                <div style='padding:3px;  border-bottom:1px dashed #4d494a;'><a href='{$DOMAIN}/adm/campaign/edit/{$campaign.id}'>{$campaign.name}</a></div>
                            {/foreach}
                        </td>
                        <td style="border-left:0 !important; padding-left:0px; padding-right:0px;">
                            {foreach from=$item.campaigns item=campaign}
                                <div style='padding:3px;  border-bottom:1px dashed #4d494a;'><a href='{$DOMAIN}/adm/banner/index/?filter&campaign={$campaign.id}'>{$campaign.totalcount} / <b>{$campaign.oncount}</b></a></div>
                            {/foreach}
                        </td>
                        <td style="border-left:0 !important; padding-left:0px; padding-right:0px;">
                            {foreach from=$item.campaigns item=campaign}
                                <div style='padding:3px;  border-bottom:1px dashed #4d494a;'>
                                    {if $campaign.stat}
                                        {$campaign.stat.impressions} / <b>{$campaign.stat.clicks}</b> / <i>{$campaign.stat.ctr}</i>
                                    {else}
                                        -
                                    {/if}
                                    </div>
                            {/foreach}
                            <div style='padding:3px; color:#000; background: #e0f3ef;'>�����: {$item.stat.impressions} / <b>{$item.stat.clicks}</b> / <i>{$item.stat.ctr}</i></div>
                        </td>
                        <td><div class="edit_links">
                                <div><a href="{$DOMAIN}/adm/campaign/add/{$item.id}"><i class="icon-plus"></i> �������� ��������</a></div>
                                <div><a href="{$DOMAIN}/adm/client/edit/{$item.id}"><i class="icon-edit"></i> ������������� �������</a></div>
                                <div><a class='confirmation' href="{$DOMAIN}/adm/client/delete/{$item.id}"><i class="icon-remove"></i> <span style="color:#a60018">������� ������� </span></a></div>
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p>�� ������ ������� �� ��������� � �������.</p>
        {/if}

    </div>
</div>

{include file="blocks/footer_block.tpl"}