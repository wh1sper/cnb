<select class="input-xxlarge" name="value[{$key}]">
    {foreach from=$geo_array item="geo"}
        <option value="{$geo.name}" {if $geo.name eq $setting.value}selected{/if}>{$geo.name}</option>
    {/foreach}
</select>
