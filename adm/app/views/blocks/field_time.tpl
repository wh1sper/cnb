<table cellspacing="0" class="timetable">
    <tbody>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="0" {if "0"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 0:00-0:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="1" {if "1"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 1:00-1:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="2" {if "2"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 2:00-2:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="3" {if "3"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 3:00-3:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="4" {if "4"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 4:00-4:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="5" {if "5"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 5:00-5:59</label></td>
    </tr>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="6" {if "6"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 6:00-6:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="7" {if "7"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 7:00-7:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="8" {if "8"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 8:00-8:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="9" {if "9"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 9:00-9:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="10" {if "10"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 10:00-10:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="11" {if "11"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 11:00-11:59</label></td>
    </tr>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="12" {if "12"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 12:00-12:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="13" {if "13"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 13:00-13:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="14" {if "14"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 14:00-14:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="15" {if "15"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 15:00-15:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="16" {if "16"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 16:00-16:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="17" {if "17"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 17:00-17:59</label></td>
    </tr>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="18" {if "18"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 18:00-18:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="19" {if "19"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 19:00-19:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="20" {if "20"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 20:00-20:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="21" {if "21"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 21:00-21:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="22" {if "22"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 22:00-22:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="23" {if "23"|in_array:$setting.value} checked {/if} name="value[{$key}][]"> 23:00-23:59</label></td>
    </tr>
    </tbody>
</table>