<div class="left_menu">


<ul class="nav nav-list">
	{foreach from=$left_menu item=lmenu}
		<li class="nav-header">{$lmenu.name|strip_tags}
			<ul>
			{foreach from=$lmenu.children item=ch_menu}
				<li{if !empty($ch_menu.active)} class="active"{/if}><a href="{$DOMAIN}{$ch_menu.url}">{$ch_menu.name|strip_tags}</a></li>
			{/foreach}
			</ul>
		</li>
	{/foreach}
</ul>

</div>
