{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}


<div class="wrapper">
    <div class="content">
        <h1>������</h1>

        {if !empty($site_list)}
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>����</th><th>����</th><th>������� (�����/�������)</th><th>������/<b>�����</b>/<i>CTR</i></th></thead>
                </thead>
                {foreach from=$site_list key=key item=item}
                    <tr>
                        <td>{$item.id}</td>
                        <td><b><a href='{$DOMAIN}/adm/site/edit/{$item.id}'>{$item.name}</a></b><br>{$item.url}</td>
                        <td>
                            {foreach from=$item.zones item=zone}
                                <div style='padding:3px; border-bottom:1px dashed #fff;'><a href='{$DOMAIN}/adm/zone/edit/{$zone.id}'>{$zone.name}</a></div>
                            {/foreach}
                        </td>
                        <td style="padding-right: 0px;">
                            {foreach from=$item.zones item=zone}
                                {foreach from=$zone.counts item=count}
                                    <div style='padding:3px;  border-bottom:1px dashed #4d494a;'><a href='{$DOMAIN}/adm/zone/banner/{$zone.id}'>{$count.totalcount} / <b>{$count.oncount}</b></a></div>
                                {/foreach}
                            {/foreach}
                            {if $item.bannercount.total}
                                <div style='padding:3px; color:#000; '>�����: {$item.bannercount.total} / <b>{$item.bannercount.active}</b></div>
                            {/if}
                        </td>
                        <td style="border-left:0 !important; padding-left:0px; padding-right:0px;">
                            {foreach from=$item.zones item=zone}
                                <div style='padding:3px; border-bottom:1px dashed #4d494a;'>
                                    {if $zone.stat}
                                        {$zone.stat.impressions} / <b>{$zone.stat.clicks}</b> / <i>{$zone.stat.ctr}</i>
                                    {else}
                                        -
                                    {/if}
                                </div>
                            {/foreach}

                            {if $item.stat.impressions}
                                <div style='padding:3px; color:#000; background: #e0f3ef;'>�����: {$item.stat.impressions} / <b>{$item.stat.clicks}</b> / <i>{$item.stat.ctr}</i></div>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p>�� ������ ����� �� ��������� � �������.</p>
        {/if}

    </div>
</div>

{include file="blocks/footer_block.tpl"}