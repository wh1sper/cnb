

{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

{literal}
    <script>
        $(document).ready(function(){
            $("#client_list").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client_list").val(), { }, function(data){
                    $("#campaign_list").empty().append('<option value="0" selected>��������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#campaign_list").append(option);
                    });
                    $("#campaign_container").attr('style',"display:inline-block");
                });
            });

            $("#campaign_list").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign_list").val(), { }, function(data){
                    $("#banner_list").empty().append('<option value="0" selected>��������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#banner_list").append(option);
                    });
                    $("#banner_container").attr('style',"display:inline-block");
                });
            });

            $("#banner_list").change(function(){
                $('#add_button').attr('style',"display:inline-block");
            });

            $("#add_button").click(function(){
                $.post("/adm/banner/ajax_setzone", { type:1, banner:$("#banner_list").val(), zone: $("#zone_id").val() }, function(data){
                    if(data=="ok") location.reload();
                });
            });

        });

        function deletebanner(id_banner)
        {
            $.post("/adm/banner/ajax_setzone", { type:0, banner:id_banner, zone: $("#zone_id").val() }, function(data){
                location.reload();
            })
        }


    </script>
{/literal}

<div class="wrapper">
    <div class="content">

        {if $zone}
            <div class="infoplace">
                <b>����:</b> <a href='{$DOMAIN}/adm/site/edit/{$zone.siteid}'>{$zone.sitename}</a><br>
                <b>����:</b> <a href='{$DOMAIN}/adm/zone/edit/{$zone.id}'>{$zone.name}</a>
            </div>
        {/if}

        <h1>������� � ����</h1>
        <input type="hidden" id="zone_id" value="{$zone.id}"/>

        {if !empty($zone.banners)}
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>������</th><th>������</th><th>��������</th><th>�����������</th><th>������/<b>�����</b>/<i>CTR</i></th><th>��������</th></thead>
                </thead>
                {foreach from=$zone.banners key=key item=item}
                    <tr {if $item.status eq 0} class='danger'{/if}>
                        <td>{$key+1}</td>
                        <td>
                            <a href="{$DOMAIN}/adm/banner/edit/{$item.id}">{$item.name}</a> / <span style='color:#7a7676; font-style:italic; font-size:11px;'>{$item.type}</span></td>
                        <td><div style='width:450px;'>{$item.link}</div></td>
                        <td><a href="{$DOMAIN}/adm/banner/index/?filter&campaign={$item.id_campaign}">{$item.clientname} - {$item.campaignname}</a></td>
                        <td>{$item.chance} %</td>
                        <td>
                            {if $item.stat}
                                {$item.stat.impressions} / <b>{$item.stat.clicks}</b> / <i>{$item.stat.ctr}</i>
                            {else}
                                -
                            {/if}
                        </td>
                        <td><div class="edit_links">
                                <a href='' onclick="deletebanner({$item.id}); return false;"><i class="icon-remove"></i> <span style="color:#a60018">������� �� ����</span></a><br>
                                <a href="{$DOMAIN}/adm/banner/power/{$item.id}"><i class="icon-adjust"></i> {if $item.status}���������{else}��������{/if} ������</a>
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p>�� ������ ������� �� ��������� � ����.</p>
        {/if}

        <div id='addbanner' style="background: #f4f4f6; padding:10px; ">
            <p><b>�������� ������</b></p>
            ������
            <select id="client_list">
                <option value="0" selected>��������</option>
                {foreach from=$client_list item=item}
                    <option value="{$item.id}">{$item.name}</option>
                {/foreach}
            </select>

            <div id="campaign_container">
                ��������
                <select id="campaign_list"></select>
            </div>

            <div id='banner_container'>
                ������
                <select id="banner_list"></select>
            </div>

            <a id="add_button" href="#" class="hidden-btn btn btn-success"><i class="icon-plus"></i> �������� ������</a>
        </div>


    </div>
</div>



{include file="blocks/footer_block.tpl"}