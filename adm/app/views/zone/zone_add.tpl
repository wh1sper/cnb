{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

<div class="wrapper">
    <div class="content">

        <form action="{$DOMAIN}/adm/zone/save" method="post" class="add_form form-new" id="site_add">


            <fieldset>
                <legend>{if $zone}�������������� ���� {$zone.name}{else}���������� ���� �� ���� {$site.name}{/if}</legend>

                <div class="infoplace">
                <p>��� ���������� ��������� ����������� <br><b>JQuery</b> � <b>SWFObject</b> � 	&#8249;<i>HEAD</i>&#8250;</p>
                    <textarea style="height:60px; width:300px;"><script type="text/javascript" src="{$DOMAIN}/inc/jquery.js"></script></textarea><br>
                    <textarea style="height:70px; width:300px;"><script type="text/javascript" src="{$DOMAIN}/inc/swfobject.js"></script></textarea>
                </div>

                {if $zone}
                    <div class="infoplace2" style="clear:right;">
                        <p>��� ��� ������� ����:</i></p>
                        {if $redirect}
                            <textarea style="height:150px; width:300px;"><img width="1" height="1" src="{$DOMAIN}/image.php?zone={$zone.id}"><a target="_blank" href="{$DOMAIN}/click.php?zone={$zone.id}">{$redirect.content}</a></textarea>
                        {else}
                                {if $zone.type=='regular'}
                                    <textarea style="height:100px; width:300px;'"><script type="text/javascript" src="{$DOMAIN}/rotator.php?zone={$zone.id}"></script></textarea><br>
                                {elseif $zone.type=='context'}
                                    <textarea style="height:100px; width:300px;'"><script type="text/javascript" src="{$DOMAIN}/rotator.php?type=context&zone={$zone.id}"></script></textarea><br>
                                {/if}
                        {/if}
                    </div>
                {/if}

                {if ($zone)}
                    <input type='hidden' name='id' value='{$zone.id}'/>
                    <div class="control-group">
                        <label class="control-label" for="name">����</label>
                        <div class="controls">
                            <select name="id_site" class="input-xlarge">
                                {foreach from=$site_list item=item key=key}
                                    <option {if $zone.id_site eq $item.id} selected {/if}value="{$item.id}">{$item.name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                {else}
                    <input type="hidden" name="id_site" value="{$site.id}"/>
                {/if}

                <div class="control-group">
                    <label class="control-label" for="name">��������</label>
                    <p class="clue">������: <b>zoom_header_500x200</b></p>
                    <div class="controls">
                        <input class="input-xxlarge" type="text" name="name" id="name" value="{$zone.name|strip_tags}" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name">���</label>
                    <p class="clue" style="padding-bottom:15px;">���� � ���� ���� ����������� �������, ���������� ��� ���� <b>�����������</b>.<br> <u>� ����������� ���� �� ����� ���� �������� �� ������������ ��������, �.�. ������� ������� ������ �� �������� ��������!</u></p>
                    <div class="controls">
                        <select name="type" id="zonetype" class="input-medium">
                            <option {if $zone.type == 'regular'} selected{/if} value="regular">�������</option>
                            <option {if $zone.type == 'context'} selected{/if} value="context">�����������</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">��������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="descr" rows="3" id="descr">{$zone.descr}</textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">������� ���� �� �������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="insert_before" rows="3" id="insert_before">{$zone.insert_before}</textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">������� ���� ����� �������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="insert_after" rows="3" id="insert_after">{$zone.insert_after}</textarea>
                    </div>
                </div>


                <button type="submit" class="btn btn-success">{if $zone}<i class="icon-ok"></i> �������� ����{else}<i class="icon-plus"></i> �������� ����{/if}</button>
                {if $zone}
                <div style="float:right;">
                  <a class="confirmation btn btn-danger" href="{$DOMAIN}/adm/zone/delete/{$zone.id}"><i class="icon-remove"></i> ������� ����</a>
                </div>
                {/if}
            </fieldset>
        </form>

    </div>
</div>

{include file="blocks/footer_block.tpl"}
