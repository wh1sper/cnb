{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}


<div class="wrapper">
    <div class="content">
        <h1>���� �� ����� {$site.name}</h1>

        {if !empty($zone_list)}
            <table class="table">
                <thead>
                <th>�</th><th>����</th><th>��������</th><th>�������</th><th>����������</th><th>��������</th></thead>
                </thead>
                {foreach from=$zone_list key=key item=item}
                    <tr>
                        <td>{$key+1}</td>
                        <td>{$item.name}</td>
                        <td>{$item.descr}</td>
                        <td>
                            <a href="{$DOMAIN}/adm/zone/banner/{$item.id}"><i class="icon-edit"></i> ���������� ���������</a>
                        </td>
                        <td>-</td>
                        <td><div class="edit_links">
                                <a href="{$DOMAIN}/adm/zone/delete/{$item.id}"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a><br>
                                <a href="{$DOMAIN}/adm/zone/edit/{$item.id}"><i class="icon-edit"></i> �������������</a>
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p>�� ������ ���� �� ��������� ����.</p>
        {/if}

        <a href="{$DOMAIN}/adm/zone/add/{$site.id}" style='margin-top:20px;' class="btn btn-success"><i class="icon-plus"></i> �������� ����</a>

    </div>
</div>

{include file="blocks/footer_block.tpl"}