{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

{literal}
    <script>
        function setzone(type,banner,zone){
            $.post("/adm/banner/ajax_setzone", { type:type, banner:banner, zone:zone }, function(data){
                if(data=="ok") location.reload();
            });
            return false;
        }
    </script>
{/literal}

<div class="wrapper">
    <div class="content">


        <div class="infoplace">
            <b>������:</b> <a href="{$DOMAIN}/adm/client/edit/{$banner.clientid}">{$banner.clientname}</a><br>
            <b>��������:</b> <a href="{$DOMAIN}/adm/campaign/{$banner.campaignid}">{$banner.campaignname}</a>
        </div>

        <h1>������ {$banner.name} - ����</h1>

        {if $banner.type != "redirect" && $banner.type!="popup"}
            {include file="blocks/banner.tpl"}
        {/if}

        {if $banner.type eq "redirect"}
            <div style="padding:5px; border:1px dashed #ff8092; margin-bottom:10px; width:450px;">
                ������ ���� <b>��������</b> ����� ���� �������� ������ � ����� ����
            </div>
        {/if}

        {if !empty($site_list)}
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>����</th><th>����</th></thead>
                </thead>
                {foreach from=$site_list key=key item=item}
                    <tr>
                        <td>{$key+1}</td>
                        <td><b><a href="{$DOMAIN}/adm/site/edit/{$item.id}">{$item.name}</a></b><br>{$item.url}</td>
                        <td>
                            <div style="float:left; width:70%;">
                                {foreach from=$item.zones item=zone}
                                    <div {if $zone.id|in_array:$zones}class='success'{/if} style="padding:5px; border-bottom:1px dashed #4d494a;">
                                        <a href="{$DOMAIN}/adm/zone/banner/{$zone.id}">{$zone.name}</a>
                                    </div>
                                {/foreach}
                            </div>
                            <div style='float:left; width:20%;' class="edit_links">
                                {foreach from=$item.zones item=zone}
                                    <div style="padding:5px; border-bottom:1px dashed #4d494a;">
                                        <a href="" onclick='setzone({if $zone.id|in_array:$zones}0{else}1{/if},{$banner.id},{$zone.id}); return false;'>{if $zone.id|in_array:$zones}<i class="icon-remove"></i> <span style="color:#a60018">������� ��������</span>{else}
                                                {if !$full}
                                                    <i class="icon-plus"></i> �������� ��������
                                                {else}
                                                    -
                                                {/if}
                                            {/if}
                                        </a>
                                    </div>
                                {/foreach}
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p>�� ������ ���� �� ��������� ����.</p>
        {/if}

    </div>
</div>

{include file="blocks/footer_block.tpl"}