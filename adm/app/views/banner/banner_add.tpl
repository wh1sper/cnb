{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

<script>
    var domain = "{$DOMAIN}";
</script>

{literal}
<script>
    $(document).ready( function() {
        $("#bannertype").change(function(){
           if($("#bannertype").val() == "popup" || $("#bannertype").val() == "swf") {
               $("#width").attr("required","");
               $("#height").attr("required","");
               $("#width").val('');
               $("#height").val('');
               $("#dimensions").show();

               if($("#bannertype").val() == "swf") {
                   $("#flashvars_container").show();
                   $("#popup_settings").hide();
               }

               if($("#bannertype").val() == "popup") {
                   $("#flashvars_container").hide();
                   $("#popup_settings").show();
               }

               var cnt = $('<input type="text" class="input-xxlarge" name="content" id="content">')

               $("#uploader").show();

           }
           else {
               $("#width").removeAttr("required");
               $("#height").removeAttr("required");
               $("#dimensions").hide();
               $("#flashvars_container").hide();
               $("#popup_settings").hide();

               if($("#bannertype").val() == "text") {
                   var cnt = $('<textarea class="input-xxlarge text" name="content" rows="5" id="content"></textarea>');
                   $("#uploader").hide();
               } else {
                   var cnt = $('<input type="text" class="input-xxlarge" name="content" id="content">');
                   if($("#bannertype").val() != "redirect") {
                        $("#uploader").show();
                   }
                   else {
                       $("#uploader").hide();
                   }
               }
           }

           var tmp_content = $("#content").val();
           $("#content").remove();
           $("#content_container").prepend(cnt);
           $("#content").val(tmp_content);

         });

        $( "#tabs" ).tabs();


        var options = {iframe: {url: '/upload.php'}}
        var zone = new FileDrop('zone', options)

        zone.event('send', function (files) {
            files.each(function (file) {
                file.event('done', function (xhr) {
                    $("#content").val(domain + '/content/'+this.name);
                });

                file.event('error', function (e, xhr) {
                    alert('Error uploading ' + this.name + ': ' +
                            xhr.status + ', ' + xhr.statusText);
                });

                file.sendTo('/upload.php');
            });
        });

    });
</script>
{/literal}

<div class="wrapper">
    <div class="content">
      <form action="{$DOMAIN}/adm/banner/save" method="post" class="add_form form-new" id="banner_add">

            <fieldset>

                {if $banner}<input type='hidden' name='id' value='{$banner.id}'/>{/if}

                <legend>{if $banner}�������������� ������� {$banner.name}{else}���������� �������
                        {if $campaign} � �������� {$campaign.name}{/if}{/if}</legend>


                        <div id="tabs">
                        <ul>
                            <li><a href="#tabs-1">���������</a></li>
                            <li><a href="#tabs-2">����������� ������</a></li>
                            <li><a href="#tabs-3">������ ����������</a></li>
                            <li><a href="#tabs-4">������� ����</a></li>
                        </ul>

                        <div id="tabs-1">

                            <div style='float:left; width:45%'>

                                <div class="control-group">
                                    <label class="control-label" for="name">��������</label>
                                    <div class="controls">
                                        <input class="input-xxlarge" type="text" required="required" name="name" id="name" value="{if $banner}{$banner.name|strip_tags}{/if}" />
                                    </div>
                                </div>

                                {if $campaign && !$banner}
                                    <input type="hidden" name="id_campaign" value="{$campaign.id}"/>
                                {else}
                                    <div class="control-group">
                                        <label class="control-label" for="id_campaign">��������</label>
                                        <div class="controls">
                                        <select name="id_campaign" class="input-xxlarge">
                                            {foreach from=$campaign_list item=item}
                                                <option {if $banner.id_campaign eq $item.id}selected{/if} value="{$item.id}">{$item.clientname} - {$item.name}</option>
                                            {/foreach}
                                        </select>
                                        </div>
                                    </div>
                                {/if}

                                <div class="control-group">
                                    <label class="control-label" for="type">���</label>
                                    <div class="controls">
                                        <select name="banner_type" class="input-xlarge" id='bannertype'>
                                            <option value="image" {if $banner.type == 'image'} selected {/if}>��������</option>
                                            <option value="swf"{if $banner.type == 'swf'} selected{/if}>Flash</option>
                                            <option value="popup"{if $banner.type == 'popup'} selected{/if}>���-��</option>
                                            <option value="text"{if $banner.type == 'text'} selected{/if}>�����</option>
                                            <option value="html"{if $banner.type == 'html'} selected{/if}>HTML</option>
                                            <option value="redirect"{if $banner.type == 'redirect'} selected{/if}>�������� (��� �������)</option>
                                        </select>
                                    </div>
                                </div>

                                <div id="dimensions" {if $banner.type=="popup" || $banner.type=="swf"} style='display:block;'{/if}>
                                    <div class="control-group">
                                        <label class="control-label" for="url">������</label>
                                        <div class="controls">
                                            <input class="input-small" {if $banner.type=="popup"} required {/if} type="text" name="width" id="width" value="{if $banner}{$banner.width}{/if}" />
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <label class="control-label" for="url">������</label>
                                        <div class="controls">
                                            <input class="input-small" {if $banner.type=="popup"} required {/if} type="text" name="height" id="height" value="{if $banner}{$banner.height}{/if}" />
                                        </div>
                                    </div>
                                </div>

                                <div id="popup_settings" {if $banner.type=="popup"} style='display:block;'{/if}>
                                    <div class="control-group">
                                        <label class="control-label" for="url">��� ���-���</label>
                                        <div class="controls">
                                            <select name="popup_type" id="popuptype" class="input-medium">
                                                <option {if $banner.popup_type == 'center'} selected{/if} value="center">�� ������</option>
                                                <option {if $banner.popup_type == 'bottom'} selected{/if} value="bottom">�����</option>
                                            </select>
                                        </div>
                                   </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="url">�������</label>
                                    <p class="clue">������� ������, ��� ��������� ����</p>
                                    <div class="controls" id="content_container">
                                        {if $banner and $banner.type == 'text'}
                                            <textarea class="input-xxlarge text" name="content" rows="5" id="content">{$banner.content}</textarea>
                                        {else}
                                            <input type="text" class="input-xxlarge" name="content" id="content" value="{$banner.content}">
                                        {/if}
                                        <div id="uploader"  {if $banner and $banner.type eq 'text'} style='display:none;' {/if}>
                                            <i>��� ��������� ����</i><br>
                                            <fieldset id="zone">
                                                <p>���������� ���� ��� ������� ��� ������</p>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name">������</label>
                                    <div class="controls">
                                        <input class="input-xxlarge" type="text" required="required" name="link" id="link" value="{if $banner}{$banner.link|strip_tags}{/if}" />
                                    </div>
                                </div>

                                <div id="flashvars_container"  {if $banner.type=="swf"} style='display:block;'{/if}>
                                    <div class="control-group">
                                        <label class="control-label" for="url">Flashvars</label>
                                        <p class="clue">#link - ������ ��� ������ �� ������, ��������� � ���������� ����</p>
                                        <div class="controls">
                                            <input class="input-xxlarge" {if $banner.type=="swf"} required {/if} type="text" name="flashvars" id="flashvars" value="{if $banner}{$banner.flashvars}{else}link1=#link&link2=#link{/if}" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="name">�������� ��������</label>
                                        <p class="clue">������������, � ������ ���������� ����������� ������������� Flash</p>
                                        <div class="controls">
                                            <input class="input-xxlarge" type="text" name="flash_image" id="flash_image" value="{if $banner}{$banner.flash_image}{/if}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name">���������</label>
                                    <div class="controls">
                                        <input class="input-small" type="text" name="weight" id="weight" value="{if $banner}{$banner.weight|strip_tags}{else}1{/if}" />
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name">� ����� �������</label>
                                    <div class="controls">
                                        <select name="is_blank">
                                            <option {if $banner}{if !$banner.is_blank}selected{/if}{/if} value="0">���</option>
                                            <option {if !$banner or ($banner && $banner.is_blank)}selected{/if} value="1">��</option>
                                        </select>
                                    </div>
                                </div>

                        </div>

                        <div style="float:right; max-width:400px;">
                            {if $banner  && $banner.type!="redirect"}
                                {include file="blocks/banner.tpl"}
                            {/if}
                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div id="tabs-2">

                        <label>����������� ������</label>

                        <div id='settings_container' class="control-group" style="padding:10px;">
                            {if isset($banner.settings)}
                                {foreach from=$banner.settings key=key item=setting}
                                    <div class='setting_row' id="row_{$key}">
                                        {if $key > 0}
                                            <select class='input-xsmall' name="type[{$key}]">
                                                <option {if $setting.type eq "and"}selected="selected"{/if} value="and">�</option>
                                                <option {if $setting.type eq "or"}selected="selected"{/if} value="or">���</option>
                                            </select>
                                        {/if}
                                        <select id='setting_name_{$key}' class="setting_name input-xlarge" name="setting[{$key}]">
                                            {foreach from=$setting_list item=set}
                                                <option {if $set.id eq $setting.id}selected="selected"{/if} value="{$set.id}">{$set.label}</option>
                                            {/foreach}
                                        </select>
                                    <span id='setting_value_container_{$key}'>
                                        {if $setting.id eq "limit_days"}
                                            {include file="blocks/field_days.tpl"}
                                        {elseif $setting.id eq "limit_time"}
                                            {include file="blocks/field_time.tpl"}
                                        {elseif $setting.id eq "limit_geo" or  $setting.id eq "limit_geo_state" or $setting.id eq "limit_geo_region"}
                                            {include file="blocks/field_geo.tpl"}
                                        {elseif $setting.id eq "limit_mobile"}
                                            {include file="blocks/field_mobile.tpl"}
                                        {elseif $setting.id eq "keyword"}
                                            <textarea  style='height:100px;' class="input-xxlarge" name="value[{$key}]">{$setting.value}</textarea>
                                        {else}
                                            <input type="text" class="input-xxlarge" name="value[{$key}]" value="{$setting.value}"/>
                                        {/if}

                                    </span>
                                        <a href="" onclick="deleterow({$key}); return false;"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a>
                                    </div>
                                {/foreach}
                            {/if}
                        </div>

                        <a onclick="addrow(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� ���������</a>

                    </div>


                    <div id="tabs-3">
                        <label>������ ����������</label>

                        <div id="channels_container">
                            {if isset($banner.channels)}
                                {foreach from=$banner.channels key=key item=channel}
                                    <div class="channel_row" id="channel_row_{$key}">
                                        <select class='input-xxlarge' name="channel[{$key}]">
                                            {foreach from=$channel_list item=ch}
                                                <option value="{$ch.id}" {if $ch.id eq $channel.id_channel}selected{/if}>{$ch.name}</option>
                                            {/foreach}
                                        </select>
                                        <a href='' onclick='delete_channel_row("{$key}"); return false;'><i class='icon-remove'></i> <span style='color:#a60018'>�������</span></a>
                                    </div>
                                {/foreach}
                            {/if}
                        </div>

                        <a onclick="add_channel_row(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� �����</a>

                    </div>

                    <div id="tabs-4">
                        <div class="control-group">
                            <label class="control-label" for="url">������� ���� �� �������</label>
                            <div class="controls">
                                <textarea class="input-xxlarge text" name="insert_before" rows="2" id="insert_before">{$banner.insert_before}</textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="url">������� ���� ����� �������</label>
                            <div class="controls">
                                <textarea class="input-xxlarge text" name="insert_after" rows="2" id="insert_after">{$banner.insert_after}</textarea>
                            </div>
                        </div>
                    </div>

                </div>

                <button type="submit" style='margin-top:20px;' class="btn btn-success">{if $banner}<i class="icon-ok"></i> �������� ������{else}<i class="icon-plus"></i> �������� ������{/if}</button>


            </fieldset>
        </form>

    </div>
</div>

{include file="blocks/footer_block.tpl"}
