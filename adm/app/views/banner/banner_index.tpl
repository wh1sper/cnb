{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

{literal}
    <script type="text/javascript">
        $(document).ready(function (){

            $("#client").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client").val(), { }, function(data){
                    $("#campaign").empty().append('<option value="0" selected>��� ��������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#campaign").append(option);
                    });
                    $("#campaign").show();
                });
            });

            $("#campaign").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign").val(), { }, function(data){
                    $("#banner").empty().append('<option value="0" selected>��� �������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#banner").append(option);
                    });
                    $("#banner").show();
                });
            });


        });

    </script>
{/literal}


<div class="wrapper">
    <div class="content">

        {if $campaign or $client}
            <div class="infoplace">
                {if $client}<b>������: </b><a href="{$DOMAIN}/adm/campaign/index?filter&client={$client.id}">{$client.name}</a><br>{/if}
                {if $campaign}<b>��������: </b><a href="{$DOMAIN}/adm/campaign/edit/{$campaign.id}">{$campaign.name}</a>{/if}
            </div>
        {/if}

        <h1>�������</h1>
            <a href="{$DOMAIN}/adm/banner/{if $campaign}add/{$campaign.id}{else}addraw{/if}" style='margin-top:20px;' class="btn btn-success"><i class="icon-plus"></i> �������� ������</a>

        <div class="navbar" style='clear:both; margin-top:20px;'>
            <div class="navbar-inner">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">
                    <select id="client" style="width:300px !important;" name="client">
                        <option value="0">��� �������</option>
                        {foreach from=$clients item=cl}
                            <option {if $cl.id eq $client.id} selected {/if}value="{$cl.id}">{$cl.name}</option>
                        {/foreach}
                    </select>
                    <select id="campaign" class="input-xlarge" name="campaign" style="width:300px !important; {if !isset($client)}display: none;{/if}">
                        <option value="0">��� ��������</option>
                        {if $campaigns}
                            {foreach from=$campaigns item=camp}
                                <option {if $campaign.id eq $camp.id} selected{/if} value="{$camp.id}">{$camp.name}</option>
                            {/foreach}
                        {/if}
                    </select>
                    <input type="text" style="width:300px !important;" name="name" placeholder="��������" value="{$name}"/>

                    <button class="btn btn-primary" name="filter" type="submit">�����������</button>


                </form>
            </div>
        </div>


        {if !empty($banner_list)}
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>������</th>
                {if !$campaign}<th>��������</th>{/if}
                <th>���������</th><th>����</th><th>������/<b>�����</b>/<i>CTR</i></th><th>��������</th></thead>
                </thead>
                {foreach from=$banner_list key=key item=item}
                    <tr class="{if !$item.status}danger{/if}">
                        <td>{$item.id}</td>
                        <td>
                            <a href="{$DOMAIN}/adm/banner/edit/{$item.id}">{$item.bannername}</a>  <span style='color:#7a7676; font-style:italic; font-size:11px;'>{$item.type}</span>
                            <div style='width:400px; overflow-x:hidden; text-overflow:ellipsis; padding-top:4px; font-weight: bold'>{$item.link}</div>
                            {if isset($item.settings)}

                            {/if}
                        </td>
                        {if !$campaign}
                            <td style="max-width:250px;"><a href="{$DOMAIN}/adm/banner/index/?filter&campaign={$item.id_campaign}">{$item.clientname} - {$item.name}</a></td>
                        {/if}


                        <td style="max-width:250px; font-size:12px;">
                            {if isset($item.settings)}
                                {foreach from=$item.settings key=key item=setting}
                                    <div style="font-size:12px;">
                                        {if $key > 0 }
                                            <span style="color:#44ba28; font-weight: bold;">
                                                {if $setting.type eq "and"}�{/if}
                                                {if $setting.type eq "or"}���{/if}
                                            </span>
                                        {/if}
                                        <i>{$setting.label}</i> -
                                        {if $setting.value|is_array}
                                            {', '|implode:$setting.value}
                                        {else}
                                            {$setting.value}
                                        {/if}
                                    </div>
                                {/foreach}
                            {/if}

                            {if isset($item.channels)}
                                <div style="margin-top:3px; padding:2px; background:#f4f4f6;">
                                    <b>������ ����������</b><br>
                                    {foreach from=$item.channels item=ch}
                                        <a href='{$DOMAIN}/adm/channel/edit/{$ch.id}'>{$ch.name}</a><br>
                                    {/foreach}
                                </div>
                            {/if}

                        </td>

                        <td style="padding-right:0;">
                            {foreach from=$item.zones item=zone}
                                <div style='padding:3px; border-bottom:1px dashed #4d494a;'><a href="{$DOMAIN}/adm/zone/banner/{$zone.id}">{$zone.name}</a></div>
                            {/foreach}
                        </td>

                        <td style="padding-left:0; border-left:0;">
                            {foreach from=$item.zones item=zone}
                                <div style='padding:3px; border-bottom:1px dashed #4d494a;'>
                                    {if $zone.stat}
                                        {$zone.stat.impressions} / <b>{$zone.stat.clicks}</b> / <i>{$zone.stat.ctr}</i>
                                    {else}
                                        -
                                    {/if}
                                </div>
                            {/foreach}
                            <div style='padding:3px; color:#000; background: #e0f3ef;'>�����: {$item.stat.impressions} / <b>{$item.stat.clicks}</b> / <i>{$item.stat.ctr}</i></div>
                        </td>

                        <td style="min-width:120px;">
                            <div class="edit_links">
                                <div><a href="{$DOMAIN}/adm/banner/zone/{$item.id}"><i class="icon-list"></i> ��������� ���</a></div>
                                <div><a href="{$DOMAIN}/adm/banner/power/{$item.id}"><i class="icon-adjust"></i> {if $item.status}���������{else}��������{/if}</a></div>
                                <div><a href="{$DOMAIN}/adm/banner/copy/{$item.id}"><i class="icon-edit"></i> ����������</a></div>
                                <div><a class='confirmation' href="{$DOMAIN}/adm/banner/delete/{$item.id}"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a></div>
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>

            {if $array_link}<div style="margin: 8px; "><B>��������:  {foreach name=outer item=item from=$array_link}{$item}{/foreach}</B></div>{/if}

        {else}
            <p>�� ������ ������� �� ��������� � �������.</p>
        {/if}



    </div>
</div>

{include file="blocks/footer_block.tpl"}