{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}
{literal}
    <script type="text/javascript">
        $(document).ready(function (){
            $("#period").change(function(){
                if($("#period").val() != "custom") {
                    $("#datepicker1").hide();
                    $("#datepicker2").hide();
                }
                else {
                    $("#datepicker1").show();
                    $("#datepicker2").show();
                }
            });

            $("#client").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client").val(), { }, function(data){
                    $("#campaign").empty().append('<option value="0" selected>��� ��������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#campaign").append(option);
                    });
                    $("#campaign").show();
                });
            });

            $("#campaign").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign").val(), { }, function(data){
                    $("#banner").empty().append('<option value="0" selected>��� �������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#banner").append(option);
                    });
                    $("#banner").show();
                });
            });

            $("#site").change(function(){
                $.getJSON("/adm/zone/ajax_list/" + $("#site").val(), { }, function(data){
                    $("#zone").empty().append('<option value="0" selected>��� ����</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#zone").append(option);
                    });
                    $("#zone").show();
                });
            });

        });


    </script>
{/literal}


<div class="wrapper">
    <div class="content">

        <h1 style="margin-bottom:25px;">����������</h1>

        <div class="navbar">
            <div class="navbar-inner" style="padding-bottom:10px; padding-top:10px;">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">

                   <table class="filter-table">
                        <tr>
                            <td>����: </td>
                            <td>
                                <select id="period" name="period" class="input-large">
                                    <option {if $period eq "week"} selected {/if}value="week">������</option>
                                    <option {if $period eq "month"} selected {/if}value="month">�����</option>
                                    <option {if $period eq "custom"} selected {/if}value="custom">�������� ����</option>
                                </select>
                            </td>
                            <td>
                                <input class="input-large select_date" type="text" name="date_from" id="datepicker1" value="{$date_from}" />
                            </td>
                            <td colspan="2">
                                <input class="input-large select_date" type="text" name="date_to" id="datepicker2" value="{$date_to}" />
                            </td>
                        </tr>
                        <tr>
                            <td>����: </td>
                            <td>
                                <select id="site" name="site" class="input-large">
                                    <option value="0">��� �����</option>
                                    {foreach from=$sites item=st}
                                        <option {if $site eq $st.id} selected {/if} value="{$st.id}">{$st.name}</option>
                                    {/foreach}
                                </select>
                            </td>
                            <td colspan="2">
                                <select id="zone" class="input-large" name="zone" {if !isset($zones)}style="display:none;"{/if}>
                                    <option value="0">��� ����</option>
                                    {if $zones}
                                        {foreach from=$zones item=zn}
                                            <option {if $zone eq $zn.id} selected{/if} value="{$zn.id}">{$zn.name}</option>
                                        {/foreach}
                                    {/if}

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>������: </td>
                            <td>
                                <select id="client" class="input-large" name="client">
                                    <option value="0">��� �������</option>
                                    {foreach from=$clients item=cli}
                                        <option {if $client eq $cli.id} selected {/if} value="{$cli.id}">{$cli.name}</option>
                                    {/foreach}
                                </select>
                            </td>
                            <td>
                                <select id="campaign" class="input-large" name="campaign" {if !isset($campaigns)}style="display: none;"{/if}>
                                    <option value="0">��� ��������</option>
                                    {if $campaigns}
                                        {foreach from=$campaigns item=camp}
                                            <option {if $campaign eq $camp.id} selected{/if} value="{$camp.id}">{$camp.name}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </td>
                            <td>
                                <select id="banner" class="input-large" name="banner" {if !isset($campaign)}style="display: none;"{/if}>
                                    <option value="0">��� �������</option>
                                    {if $banners}
                                        {foreach from=$banners item=bann}
                                            <option {if $banner eq $bann.id} selected{/if} value="{$bann.id}">{$bann.name}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </td>
                        </tr>
                       <tr>
                           <td colspan="4" style="padding-top:6px;">
                               <button class="btn btn-primary" name="filter" type="submit">�����������</button>
                               <a class="btn btn-default" href="{$DOMAIN}/adm/stat/export/days/?{$getstr}">�������</a>
                           </td>
                       </tr>
                   </table>


                </form>
            </div>
        </div>

        <div class="clear" style="margin-bottom:30px;"></div>

        {if isset($textdata)}
            {*
            <h2>
                {if isset($textdata['site'])} ����: {$textdata.site.name} {/if}
                {if isset($textdata['zone'])}  ����: {$textdata.zone.name} {/if}
                {if isset($textdata['client'])} ������: {$textdata.client.name} {/if}
                {if isset($textdata['campaign'])} / ��������: {$textdata.campaign.name} {/if}
                {if isset($textdata['banner'])} / ������: {$textdata.banner.name}{/if}
            </h2>
        *}
        {/if}
        <table class="table table-bordered">
            <thead>
            <th>����</th><th>������</th><th>�����</th><th>CTR</th>
            </thead>
            <tbody>
                {foreach from=$dates item=date}
                    <tr>
                        <td>
                            {if $date.clk}
                                <a href="{$DOMAIN}/adm/stat/day/{$date.date}{if isset($getstr)}/?{$getstr}{/if}">{if $date.date eq $today}<b>{$date.date}</b>{else}{$date.date}{/if}</a>
                            {else}
                                {if $date.date eq $today}<b>{$date.date}</b>{else}{$date.date}{/if}
                            {/if}
                        </td>
                        <td>{$date.imp}</td>
                        <td>{$date.clk}</td>
                        <td>{$date.ctr|string_format:"%.2f"}</td>
                    </tr>
                {/foreach}
            <tr>
                <td>�����:</td>
                <td>{$total_imp}</td>
                <td>{$total_clicks}</td>
                <td>{$total_ctr}</td>
            </tr>
            </tbody>
        </table>


    </div>
</div>

{include file="blocks/footer_block.tpl"}