{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}
{literal}
    <script type="text/javascript">
        $(document).ready(function (){
            $("#period").change(function(){
                if($("#period").val() != "custom") {
                    $("#datepicker1").hide();
                    $("#datepicker2").hide();
                }
                else {
                    $("#datepicker1").show();
                    $("#datepicker2").show();
                }
            });

            $("#client").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client").val(), { }, function(data){
                    $("#campaign").empty().append('<option value="0" selected>��� ��������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#campaign").append(option);
                    });
                    $("#campaign").show();
                });
            });

            $("#campaign").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign").val(), { }, function(data){
                    $("#banner").empty().append('<option value="0" selected>��� �������</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#banner").append(option);
                    });
                    $("#banner").show();
                });
            });

            $("#site").change(function(){
                $.getJSON("/adm/zone/ajax_list/" + $("#site").val(), { }, function(data){
                    $("#zone").empty().append('<option value="0" selected>��� ����</option>');
                    $(data).each( function (index, element){
                        var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
                        $("#zone").append(option);
                    });
                    $("#zone").show();
                });
            });

        });


    </script>
{/literal}


<div class="wrapper">
    <div class="content">

        <h1 style="margin-bottom:25px;">���������� ������ �� {$date}</h1>

        <div class="navbar">
            <div class="navbar-inner">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">

                    <div style="float:left;">

                        <select id="site" name="site">
                            <option value="0">��� �����</option>
                            {foreach from=$sites item=st}
                                <option {if $site eq $st.id} selected {/if} value="{$st.id}">{$st.name}</option>
                            {/foreach}
                        </select>

                        <select id="zone" name="zone" {if !isset($zone)}style="display:none;"{/if}>
                            <option value="0">��� ����</option>
                            {if $zone}
                                {foreach from=$zones item=zn}
                                    <option {if $zone eq $zn.id} selected{/if} value="{$zn.id}">{$zn.name}</option>
                                {/foreach}
                            {/if}

                        </select>

                    </div>


                    <div style="float:left; margin-left:100px;">

                        <select id="client" name="client">
                            <option value="0">��� �������</option>
                            {foreach from=$clients item=cli}
                                <option {if $client eq $cli.id} selected {/if} value="{$cli.id}">{$cli.name}</option>
                            {/foreach}
                        </select>

                        <select id="campaign" name="campaign" {if !isset($client)}style="display: none;"{/if}>
                            <option value="0">��� ��������</option>
                            {if $campaigns}
                                {foreach from=$campaigns item=camp}
                                    <option {if $campaign eq $camp.id} selected{/if} value="{$camp.id}">{$camp.name}</option>
                                {/foreach}
                            {/if}
                        </select>

                        <select id="banner" name="banner" {if !isset($campaign)}style="display: none;"{/if}>
                            <option value="0">��� �������</option>
                            {if $banners}
                                {foreach from=$banners item=bann}
                                    <option {if $banner eq $bann.id} selected{/if} value="{$bann.id}">{$bann.name}</option>
                                {/foreach}
                            {/if}
                        </select>

                    </div>

                    <div style="float:left; margin-left:50px;">
                        <button class="btn btn-success" name="filter" type="submit">�����������</button>
                    </div>

                </form>
            </div>
        </div>

        <div class="clear" style="margin-bottom:30px;"></div>

        <table class="table table-bordered table-narrow">
            <thead>
            <th>���</th><th>������</th><th>�����</th><th>CTR</th>
            </thead>
            <tbody>

            {foreach from=$hours key=key item=i}
                <tr>
                    <td>{$key}:00 - {$key}:59</td>
                    <td>{$i.imp}</td>
                    <td>{$i.clk}</td>
                    <td>{$i.ctr}</td>
                </tr>
            {/foreach}
            <tr>
                <td>�����</td>
                <td>{$total.imp}</td>
                <td>{$total.clk}</td>
                <td>{$total.ctr}</td>
            </tr>
            </tbody>
        </table>

    </div>
</div>

{include file="blocks/footer_block.tpl"}