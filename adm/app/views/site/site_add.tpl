{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

<div class="wrapper">
    <div class="content">

        <form action="{$DOMAIN}/adm/site/save" method="post" class="add_form form-new" id="site_add">

            {if ($site)}<input type='hidden' name='id' value='{$site.id}'/>{/if}

            <fieldset>
                <legend>{if $site}�������������� ����� {$site.name}{else}���������� �����{/if}</legend>

                <div class="control-group">
                    <label class="control-label" for="name">��������</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="name" id="name" value="{$site.name|strip_tags}" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">URL</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="url" id="url" value="{$site.url|strip_tags}" />
                    </div>
                </div>

                <button type="submit" class="btn btn-success">{if $site}<i class="icon-ok"></i> �������� ����{else}<i class="icon-plus"></i> �������� ����{/if}</button>
            </fieldset>
        </form>

    </div>
</div>

{include file="blocks/footer_block.tpl"}
