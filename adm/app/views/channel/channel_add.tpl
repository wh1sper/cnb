{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}

<div class="wrapper">
    <div class="content">

        <form action="{$DOMAIN}/adm/channel/save" method="post" class="add_form form-new" id="channel_add">

            {if ($channel)}<input type='hidden' name='id' value='{$channel.id}'/>{/if}

            <fieldset>
                <legend>{if $channel}�������������� ������ {$channel.name}{else}���������� ������ ����������{/if}</legend>

                <div class="control-group">
                    <label class="control-label" for="name">��������</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" required="required" name="name" id="name" value="{$channel.name|strip_tags}" />
                    </div>
                </div>

                <div id='settings_container' class="control-group" style="background: #f4f4f6; padding:10px;">
                    <label class="control-label" for="name">���������</label>
                    {if $channel}
                        {if isset($channel.settings)}
                            {foreach from=$channel.settings key=key item=setting}
                                <div class='setting_row' id="row_{$key}">
                                    {if $key > 0}
                                        <select class='input-xsmall' name="type[{$key}]">
                                            <option {if $setting.type eq "and"}selected="selected"{/if} value="and">�</option>
                                            <option {if $setting.type eq "or"}selected="selected"{/if} value="or">���</option>
                                        </select>
                                    {/if}
                                    <select id='setting_name_{$key}' class="setting_name input-xlarge" name="setting[{$key}]">
                                        {foreach from=$setting_list item=set}
                                            <option {if $set.id eq $setting.id}selected="selected"{/if} value="{$set.id}">{$set.label}</option>
                                        {/foreach}
                                    </select>
                                    <span id='setting_value_container_{$key}'>
                                        {if $setting.id eq "limit_days"}
                                            {include file="blocks/field_days.tpl"}
                                        {elseif $setting.id eq "limit_time"}
                                            {include file="blocks/field_time.tpl"}
                                        {elseif $setting.id eq "limit_geo" or  $setting.id eq "limit_geo_state" or $setting.id eq "limit_geo_region"}
                                            {include file="blocks/field_geo.tpl"}
                                        {elseif $setting.id eq "limit_mobile"}
                                              {include file="blocks/field_mobile.tpl"}
                                        {else}
                                            <input type="text" class="input-xxlarge" name="value[{$key}]" value="{$setting.value}"/>
                                        {/if}

                                    </span>
                                    <a href="" onclick="deleterow({$key}); return false;"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a>
                                </div>
                            {/foreach}
                        {/if}
                    {/if}
                </div>

                <a onclick="addrow(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� ���������</a>

                <button style='margin-top:50px;' type="submit" class="btn btn-success">{if $channel}<i class="icon-ok"></i> �������� �����{else}<i class="icon-plus"></i> �������� �����{/if}</button>

            </fieldset>
        </form>

    </div>
</div>

{include file="blocks/footer_block.tpl"}
