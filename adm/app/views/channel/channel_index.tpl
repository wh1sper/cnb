{include file="blocks/header_block.tpl"}
{include file="blocks/breadcrumbs_block.tpl"}
{include file="blocks/left_menu_block.tpl"}


<div class="wrapper">
    <div class="content">
        <h1>������ ����������</h1>

        {if !empty($channel_list)}
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>��������</th><th>���������</th><th>��������</th></thead>
                </thead>
                {foreach from=$channel_list key=key item=item}
                    <tr>
                        <td>{$key+1}</td>
                        <td><a href="{$DOMAIN}/adm/channel/edit/{$item.id}">{$item.name}</a></td>
                        <td>
                            {if isset($item.settings)}
                                {foreach from=$item.settings key=key item=setting}
                                    <div style="font-size:15px;">
                                        {if $key > 0 }
                                            <span style="color:#44ba28; font-weight: bold;">
                                                {if $setting.type eq "and"}�{/if}
                                                {if $setting.type eq "or"}���{/if}
                                            </span>
                                        {/if}
                                        <i>{$setting.label}</i> -
                                        {if $setting.value|is_array}
                                            {', '|implode:$setting.value}
                                        {else}
                                            {$setting.value}
                                        {/if}
                                    </div>
                                {/foreach}
                            {/if}
                        </td>
                        <td>
                            <div><a href="{$DOMAIN}/adm/channel/edit/{$item.id}"><i class="icon-edit"></i> ������������� �����</a></div>
                            <div><a class='confirmation' href="{$DOMAIN}/adm/channel/delete/{$item.id}"><i class="icon-remove"></i> <span style="color:#a60018">������� ����� </span></a></div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p>�� ������ ������ �� ��������� � �������.</p>
        {/if}

        <a href="/adm/channel/addraw" style='margin-top:20px;' class="btn btn-success"><i class="icon-plus"></i> �������� �����</a>

    </div>
</div>

{include file="blocks/footer_block.tpl"}