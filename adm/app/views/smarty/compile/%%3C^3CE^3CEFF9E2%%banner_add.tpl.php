<?php /* Smarty version 2.6.18, created on 2015-04-06 14:27:24
         compiled from banner/banner_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'banner/banner_add.tpl', 115, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<script>
    var domain = "<?php echo $this->_tpl_vars['DOMAIN']; ?>
";
</script>

<?php echo '
<script>
    $(document).ready( function() {
        $("#bannertype").change(function(){
           if($("#bannertype").val() == "popup" || $("#bannertype").val() == "swf") {
               $("#width").attr("required","");
               $("#height").attr("required","");
               $("#width").val(\'\');
               $("#height").val(\'\');
               $("#dimensions").show();

               if($("#bannertype").val() == "swf") {
                   $("#flashvars_container").show();
                   $("#popup_settings").hide();
               }

               if($("#bannertype").val() == "popup") {
                   $("#flashvars_container").hide();
                   $("#popup_settings").show();
               }

               var cnt = $(\'<input type="text" class="input-xxlarge" name="content" id="content">\')

               $("#uploader").show();

           }
           else {
               $("#width").removeAttr("required");
               $("#height").removeAttr("required");
               $("#dimensions").hide();
               $("#flashvars_container").hide();
               $("#popup_settings").hide();

               if($("#bannertype").val() == "text") {
                   var cnt = $(\'<textarea class="input-xxlarge text" name="content" rows="5" id="content"></textarea>\');
                   $("#uploader").hide();
               } else {
                   var cnt = $(\'<input type="text" class="input-xxlarge" name="content" id="content">\');
                   if($("#bannertype").val() != "redirect") {
                        $("#uploader").show();
                   }
                   else {
                       $("#uploader").hide();
                   }
               }
           }

           var tmp_content = $("#content").val();
           $("#content").remove();
           $("#content_container").prepend(cnt);
           $("#content").val(tmp_content);

         });

        $( "#tabs" ).tabs();


        var options = {iframe: {url: \'/upload.php\'}}
        var zone = new FileDrop(\'zone\', options)

        zone.event(\'send\', function (files) {
            files.each(function (file) {
                file.event(\'done\', function (xhr) {
                    $("#content").val(domain + \'/content/\'+this.name);
                });

                file.event(\'error\', function (e, xhr) {
                    alert(\'Error uploading \' + this.name + \': \' +
                            xhr.status + \', \' + xhr.statusText);
                });

                file.sendTo(\'/upload.php\');
            });
        });

    });
</script>
'; ?>


<div class="wrapper">
    <div class="content">
      <form action="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/save" method="post" class="add_form form-new" id="banner_add">

            <fieldset>

                <?php if ($this->_tpl_vars['banner']): ?><input type='hidden' name='id' value='<?php echo $this->_tpl_vars['banner']['id']; ?>
'/><?php endif; ?>

                <legend><?php if ($this->_tpl_vars['banner']): ?>�������������� ������� <?php echo $this->_tpl_vars['banner']['name']; ?>
<?php else: ?>���������� �������
                        <?php if ($this->_tpl_vars['campaign']): ?> � �������� <?php echo $this->_tpl_vars['campaign']['name']; ?>
<?php endif; ?><?php endif; ?></legend>


                        <div id="tabs">
                        <ul>
                            <li><a href="#tabs-1">���������</a></li>
                            <li><a href="#tabs-2">����������� ������</a></li>
                            <li><a href="#tabs-3">������ ����������</a></li>
                            <li><a href="#tabs-4">������� ����</a></li>
                        </ul>

                        <div id="tabs-1">

                            <div style='float:left; width:45%'>

                                <div class="control-group">
                                    <label class="control-label" for="name">��������</label>
                                    <div class="controls">
                                        <input class="input-xxlarge" type="text" required="required" name="name" id="name" value="<?php if ($this->_tpl_vars['banner']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['banner']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
<?php endif; ?>" />
                                    </div>
                                </div>

                                <?php if ($this->_tpl_vars['campaign'] && ! $this->_tpl_vars['banner']): ?>
                                    <input type="hidden" name="id_campaign" value="<?php echo $this->_tpl_vars['campaign']['id']; ?>
"/>
                                <?php else: ?>
                                    <div class="control-group">
                                        <label class="control-label" for="id_campaign">��������</label>
                                        <div class="controls">
                                        <select name="id_campaign" class="input-xxlarge">
                                            <?php $_from = $this->_tpl_vars['campaign_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                                <option <?php if ($this->_tpl_vars['banner']['id_campaign'] == $this->_tpl_vars['item']['id']): ?>selected<?php endif; ?> value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['clientname']; ?>
 - <?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                                            <?php endforeach; endif; unset($_from); ?>
                                        </select>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="control-group">
                                    <label class="control-label" for="type">���</label>
                                    <div class="controls">
                                        <select name="banner_type" class="input-xlarge" id='bannertype'>
                                            <option value="image" <?php if ($this->_tpl_vars['banner']['type'] == 'image'): ?> selected <?php endif; ?>>��������</option>
                                            <option value="swf"<?php if ($this->_tpl_vars['banner']['type'] == 'swf'): ?> selected<?php endif; ?>>Flash</option>
                                            <option value="popup"<?php if ($this->_tpl_vars['banner']['type'] == 'popup'): ?> selected<?php endif; ?>>���-��</option>
                                            <option value="text"<?php if ($this->_tpl_vars['banner']['type'] == 'text'): ?> selected<?php endif; ?>>�����</option>
                                            <option value="html"<?php if ($this->_tpl_vars['banner']['type'] == 'html'): ?> selected<?php endif; ?>>HTML</option>
                                            <option value="redirect"<?php if ($this->_tpl_vars['banner']['type'] == 'redirect'): ?> selected<?php endif; ?>>�������� (��� �������)</option>
                                        </select>
                                    </div>
                                </div>

                                <div id="dimensions" <?php if ($this->_tpl_vars['banner']['type'] == 'popup' || $this->_tpl_vars['banner']['type'] == 'swf'): ?> style='display:block;'<?php endif; ?>>
                                    <div class="control-group">
                                        <label class="control-label" for="url">������</label>
                                        <div class="controls">
                                            <input class="input-small" <?php if ($this->_tpl_vars['banner']['type'] == 'popup'): ?> required <?php endif; ?> type="text" name="width" id="width" value="<?php if ($this->_tpl_vars['banner']): ?><?php echo $this->_tpl_vars['banner']['width']; ?>
<?php endif; ?>" />
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <label class="control-label" for="url">������</label>
                                        <div class="controls">
                                            <input class="input-small" <?php if ($this->_tpl_vars['banner']['type'] == 'popup'): ?> required <?php endif; ?> type="text" name="height" id="height" value="<?php if ($this->_tpl_vars['banner']): ?><?php echo $this->_tpl_vars['banner']['height']; ?>
<?php endif; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <div id="popup_settings" <?php if ($this->_tpl_vars['banner']['type'] == 'popup'): ?> style='display:block;'<?php endif; ?>>
                                    <div class="control-group">
                                        <label class="control-label" for="url">��� ���-���</label>
                                        <div class="controls">
                                            <select name="popup_type" id="popuptype" class="input-medium">
                                                <option <?php if ($this->_tpl_vars['banner']['popup_type'] == 'center'): ?> selected<?php endif; ?> value="center">�� ������</option>
                                                <option <?php if ($this->_tpl_vars['banner']['popup_type'] == 'bottom'): ?> selected<?php endif; ?> value="bottom">�����</option>
                                            </select>
                                        </div>
                                   </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="url">�������</label>
                                    <p class="clue">������� ������, ��� ��������� ����</p>
                                    <div class="controls" id="content_container">
                                        <?php if ($this->_tpl_vars['banner'] && $this->_tpl_vars['banner']['type'] == 'text'): ?>
                                            <textarea class="input-xxlarge text" name="content" rows="5" id="content"><?php echo $this->_tpl_vars['banner']['content']; ?>
</textarea>
                                        <?php else: ?>
                                            <input type="text" class="input-xxlarge" name="content" id="content" value="<?php echo $this->_tpl_vars['banner']['content']; ?>
">
                                        <?php endif; ?>
                                        <div id="uploader"  <?php if ($this->_tpl_vars['banner'] && $this->_tpl_vars['banner']['type'] == 'text'): ?> style='display:none;' <?php endif; ?>>
                                            <i>��� ��������� ����</i><br>
                                            <fieldset id="zone">
                                                <p>���������� ���� ��� ������� ��� ������</p>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name">������</label>
                                    <div class="controls">
                                        <input class="input-xxlarge" type="text" required="required" name="link" id="link" value="<?php if ($this->_tpl_vars['banner']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['banner']['link'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
<?php endif; ?>" />
                                    </div>
                                </div>

                                <div id="flashvars_container"  <?php if ($this->_tpl_vars['banner']['type'] == 'swf'): ?> style='display:block;'<?php endif; ?>>
                                    <div class="control-group">
                                        <label class="control-label" for="url">Flashvars</label>
                                        <p class="clue">#link - ������ ��� ������ �� ������, ��������� � ���������� ����</p>
                                        <div class="controls">
                                            <input class="input-xxlarge" <?php if ($this->_tpl_vars['banner']['type'] == 'swf'): ?> required <?php endif; ?> type="text" name="flashvars" id="flashvars" value="<?php if ($this->_tpl_vars['banner']): ?><?php echo $this->_tpl_vars['banner']['flashvars']; ?>
<?php else: ?>link1=#link&link2=#link<?php endif; ?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="name">�������� ��������</label>
                                        <p class="clue">������������, � ������ ���������� ����������� ������������� Flash</p>
                                        <div class="controls">
                                            <input class="input-xxlarge" type="text" name="flash_image" id="flash_image" value="<?php if ($this->_tpl_vars['banner']): ?><?php echo $this->_tpl_vars['banner']['flash_image']; ?>
<?php endif; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name">���������</label>
                                    <div class="controls">
                                        <input class="input-small" type="text" name="weight" id="weight" value="<?php if ($this->_tpl_vars['banner']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['banner']['weight'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
<?php else: ?>1<?php endif; ?>" />
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="name">� ����� �������</label>
                                    <div class="controls">
                                        <select name="is_blank">
                                            <option <?php if ($this->_tpl_vars['banner']): ?><?php if (! $this->_tpl_vars['banner']['is_blank']): ?>selected<?php endif; ?><?php endif; ?> value="0">���</option>
                                            <option <?php if (! $this->_tpl_vars['banner'] || ( $this->_tpl_vars['banner'] && $this->_tpl_vars['banner']['is_blank'] )): ?>selected<?php endif; ?> value="1">��</option>
                                        </select>
                                    </div>
                                </div>

                        </div>

                        <div style="float:right; max-width:400px;">
                            <?php if ($this->_tpl_vars['banner'] && $this->_tpl_vars['banner']['type'] != 'redirect'): ?>
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/banner.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <?php endif; ?>
                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div id="tabs-2">

                        <label>����������� ������</label>

                        <div id='settings_container' class="control-group" style="padding:10px;">
                            <?php if (isset ( $this->_tpl_vars['banner']['settings'] )): ?>
                                <?php $_from = $this->_tpl_vars['banner']['settings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['setting']):
?>
                                    <div class='setting_row' id="row_<?php echo $this->_tpl_vars['key']; ?>
">
                                        <?php if ($this->_tpl_vars['key'] > 0): ?>
                                            <select class='input-xsmall' name="type[<?php echo $this->_tpl_vars['key']; ?>
]">
                                                <option <?php if ($this->_tpl_vars['setting']['type'] == 'and'): ?>selected="selected"<?php endif; ?> value="and">�</option>
                                                <option <?php if ($this->_tpl_vars['setting']['type'] == 'or'): ?>selected="selected"<?php endif; ?> value="or">���</option>
                                            </select>
                                        <?php endif; ?>
                                        <select id='setting_name_<?php echo $this->_tpl_vars['key']; ?>
' class="setting_name input-xlarge" name="setting[<?php echo $this->_tpl_vars['key']; ?>
]">
                                            <?php $_from = $this->_tpl_vars['setting_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['set']):
?>
                                                <option <?php if ($this->_tpl_vars['set']['id'] == $this->_tpl_vars['setting']['id']): ?>selected="selected"<?php endif; ?> value="<?php echo $this->_tpl_vars['set']['id']; ?>
"><?php echo $this->_tpl_vars['set']['label']; ?>
</option>
                                            <?php endforeach; endif; unset($_from); ?>
                                        </select>
                                    <span id='setting_value_container_<?php echo $this->_tpl_vars['key']; ?>
'>
                                        <?php if ($this->_tpl_vars['setting']['id'] == 'limit_days'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_days.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_time'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_time.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_geo' || $this->_tpl_vars['setting']['id'] == 'limit_geo_state' || $this->_tpl_vars['setting']['id'] == 'limit_geo_region'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_geo.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_mobile'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_mobile.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'keyword'): ?>
                                            <textarea  style='height:100px;' class="input-xxlarge" name="value[<?php echo $this->_tpl_vars['key']; ?>
]"><?php echo $this->_tpl_vars['setting']['value']; ?>
</textarea>
                                        <?php else: ?>
                                            <input type="text" class="input-xxlarge" name="value[<?php echo $this->_tpl_vars['key']; ?>
]" value="<?php echo $this->_tpl_vars['setting']['value']; ?>
"/>
                                        <?php endif; ?>

                                    </span>
                                        <a href="" onclick="deleterow(<?php echo $this->_tpl_vars['key']; ?>
); return false;"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a>
                                    </div>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endif; ?>
                        </div>

                        <a onclick="addrow(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� ���������</a>

                    </div>


                    <div id="tabs-3">
                        <label>������ ����������</label>

                        <div id="channels_container">
                            <?php if (isset ( $this->_tpl_vars['banner']['channels'] )): ?>
                                <?php $_from = $this->_tpl_vars['banner']['channels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['channel']):
?>
                                    <div class="channel_row" id="channel_row_<?php echo $this->_tpl_vars['key']; ?>
">
                                        <select class='input-xxlarge' name="channel[<?php echo $this->_tpl_vars['key']; ?>
]">
                                            <?php $_from = $this->_tpl_vars['channel_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ch']):
?>
                                                <option value="<?php echo $this->_tpl_vars['ch']['id']; ?>
" <?php if ($this->_tpl_vars['ch']['id'] == $this->_tpl_vars['channel']['id_channel']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['ch']['name']; ?>
</option>
                                            <?php endforeach; endif; unset($_from); ?>
                                        </select>
                                        <a href='' onclick='delete_channel_row("<?php echo $this->_tpl_vars['key']; ?>
"); return false;'><i class='icon-remove'></i> <span style='color:#a60018'>�������</span></a>
                                    </div>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endif; ?>
                        </div>

                        <a onclick="add_channel_row(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� �����</a>

                    </div>

                    <div id="tabs-4">
                        <div class="control-group">
                            <label class="control-label" for="url">������� ���� �� �������</label>
                            <div class="controls">
                                <textarea class="input-xxlarge text" name="insert_before" rows="2" id="insert_before"><?php echo $this->_tpl_vars['banner']['insert_before']; ?>
</textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="url">������� ���� ����� �������</label>
                            <div class="controls">
                                <textarea class="input-xxlarge text" name="insert_after" rows="2" id="insert_after"><?php echo $this->_tpl_vars['banner']['insert_after']; ?>
</textarea>
                            </div>
                        </div>
                    </div>

                </div>

                <button type="submit" style='margin-top:20px;' class="btn btn-success"><?php if ($this->_tpl_vars['banner']): ?><i class="icon-ok"></i> �������� ������<?php else: ?><i class="icon-plus"></i> �������� ������<?php endif; ?></button>


            </fieldset>
        </form>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>