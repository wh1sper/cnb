<?php /* Smarty version 2.6.18, created on 2014-10-28 15:44:28
         compiled from blocks/breadcrumbs_block.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'blocks/breadcrumbs_block.tpl', 9, false),)), $this); ?>


<div class="top_line">

<?php if ($this->_tpl_vars['breadcrumbs_block']): ?>
<ul class="breadcrumb">
	<?php $_from = $this->_tpl_vars['breadcrumbs_block']->get(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['brc'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['brc']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['brc']['iteration']++;
?>
		<?php if (! ($this->_foreach['brc']['iteration'] == $this->_foreach['brc']['total'])): ?>
  			<li><a href="<?php echo $this->_tpl_vars['item']['url']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</a> <span class="divider">/</span></li>
  		<?php else: ?>
  			<li class="active"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</li>
  		<?php endif; ?>
  	<?php endforeach; endif; unset($_from); ?>
</ul>
<?php endif; ?>

<div style="top: 7px; left: 20px; font-size: 15px; width: 200px;   position: absolute;"><a style="color: #999; font-size: 18px; " href="/">CNB.CNEWS.RU</a></div>


</div>
<div style="clear:both;"></div>