<?php /* Smarty version 2.6.18, created on 2015-02-10 18:36:50
         compiled from channel/channel_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'channel/channel_add.tpl', 18, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="wrapper">
    <div class="content">

        <form action="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/channel/save" method="post" class="add_form form-new" id="channel_add">

            <?php if (( $this->_tpl_vars['channel'] )): ?><input type='hidden' name='id' value='<?php echo $this->_tpl_vars['channel']['id']; ?>
'/><?php endif; ?>

            <fieldset>
                <legend><?php if ($this->_tpl_vars['channel']): ?>�������������� ������ <?php echo $this->_tpl_vars['channel']['name']; ?>
<?php else: ?>���������� ������ ����������<?php endif; ?></legend>

                <div class="control-group">
                    <label class="control-label" for="name">��������</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" required="required" name="name" id="name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['channel']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                    </div>
                </div>

                <div id='settings_container' class="control-group" style="background: #f4f4f6; padding:10px;">
                    <label class="control-label" for="name">���������</label>
                    <?php if ($this->_tpl_vars['channel']): ?>
                        <?php if (isset ( $this->_tpl_vars['channel']['settings'] )): ?>
                            <?php $_from = $this->_tpl_vars['channel']['settings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['setting']):
?>
                                <div class='setting_row' id="row_<?php echo $this->_tpl_vars['key']; ?>
">
                                    <?php if ($this->_tpl_vars['key'] > 0): ?>
                                        <select class='input-xsmall' name="type[<?php echo $this->_tpl_vars['key']; ?>
]">
                                            <option <?php if ($this->_tpl_vars['setting']['type'] == 'and'): ?>selected="selected"<?php endif; ?> value="and">�</option>
                                            <option <?php if ($this->_tpl_vars['setting']['type'] == 'or'): ?>selected="selected"<?php endif; ?> value="or">���</option>
                                        </select>
                                    <?php endif; ?>
                                    <select id='setting_name_<?php echo $this->_tpl_vars['key']; ?>
' class="setting_name input-xlarge" name="setting[<?php echo $this->_tpl_vars['key']; ?>
]">
                                        <?php $_from = $this->_tpl_vars['setting_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['set']):
?>
                                            <option <?php if ($this->_tpl_vars['set']['id'] == $this->_tpl_vars['setting']['id']): ?>selected="selected"<?php endif; ?> value="<?php echo $this->_tpl_vars['set']['id']; ?>
"><?php echo $this->_tpl_vars['set']['label']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                    <span id='setting_value_container_<?php echo $this->_tpl_vars['key']; ?>
'>
                                        <?php if ($this->_tpl_vars['setting']['id'] == 'limit_days'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_days.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_time'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_time.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_geo' || $this->_tpl_vars['setting']['id'] == 'limit_geo_state' || $this->_tpl_vars['setting']['id'] == 'limit_geo_region'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_geo.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_mobile'): ?>
                                              <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_mobile.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php else: ?>
                                            <input type="text" class="input-xxlarge" name="value[<?php echo $this->_tpl_vars['key']; ?>
]" value="<?php echo $this->_tpl_vars['setting']['value']; ?>
"/>
                                        <?php endif; ?>

                                    </span>
                                    <a href="" onclick="deleterow(<?php echo $this->_tpl_vars['key']; ?>
); return false;"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a>
                                </div>
                            <?php endforeach; endif; unset($_from); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

                <a onclick="addrow(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� ���������</a>

                <button style='margin-top:50px;' type="submit" class="btn btn-success"><?php if ($this->_tpl_vars['channel']): ?><i class="icon-ok"></i> �������� �����<?php else: ?><i class="icon-plus"></i> �������� �����<?php endif; ?></button>

            </fieldset>
        </form>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>