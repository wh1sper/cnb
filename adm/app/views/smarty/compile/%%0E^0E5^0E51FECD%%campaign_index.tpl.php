<?php /* Smarty version 2.6.18, created on 2015-04-29 10:52:52
         compiled from campaign/campaign_index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'is_array', 'campaign/campaign_index.tpl', 67, false),array('modifier', 'implode', 'campaign/campaign_index.tpl', 68, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<div class="wrapper">
    <div class="content">

        <?php if ($this->_tpl_vars['client']): ?>
            <div class="infoplace">
                <b>������:</b> <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/index?filter&client=/<?php echo $this->_tpl_vars['client']['id']; ?>
"><?php echo $this->_tpl_vars['client']['name']; ?>
</a><br>
            </div>
        <?php endif; ?>

        <h1>��������</h1>

        <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/addraw" style='margin-top:20px;' class="btn btn-success"><i class="icon-plus"></i> �������� ��������</a>


        <div class="navbar" style='clear:both; margin-top:20px;'>
            <div class="navbar-inner">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">
                    <select id="client" style="width:300px !important;" name="client">
                        <option value="0">��� �������</option>
                        <?php $_from = $this->_tpl_vars['clients']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cli']):
?>
                            <option <?php if ($this->_tpl_vars['client']['id'] == $this->_tpl_vars['cli']['id']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['cli']['id']; ?>
"><?php echo $this->_tpl_vars['cli']['name']; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>

                    <input type="text" style="width:300px !important;" name="name" placeholder="��������" value="<?php echo $this->_tpl_vars['name']; ?>
"/>

                    <button class="btn btn-primary" name="filter" type="submit">�����������</button>

                </form>
            </div>
        </div>

        <?php if (! empty ( $this->_tpl_vars['campaign_list'] )): ?>
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>��������</th><th>������</th><th>�����������</th><th>�������</th><th>������/<b>�����</b>/<i>CTR</i></th><th>��������</th></thead>
                </thead>
                <?php $_from = $this->_tpl_vars['campaign_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                    <tr>
                        <td><?php echo $this->_tpl_vars['item']['id']; ?>
</td>
                        <td>
                            <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/edit/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</a>
                            <div class="stat_external">
                                <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/stat.php?code=<?php echo $this->_tpl_vars['item']['code']; ?>
"><?php echo $this->_tpl_vars['DOMAIN']; ?>
/stat.php?code=<?php echo $this->_tpl_vars['item']['code']; ?>
</a>
                            </div>
                        </td>
                        <td><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/index?filter&client=<?php echo $this->_tpl_vars['item']['id_client']; ?>
"><?php echo $this->_tpl_vars['item']['clientname']; ?>
</a></td>

                        <td style="max-width:250px; font-size:12px;">
                            <?php if (isset ( $this->_tpl_vars['item']['settings'] )): ?>
                                <?php $_from = $this->_tpl_vars['item']['settings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['setting']):
?>
                                    <div style="font-size:12px;">
                                        <?php if ($this->_tpl_vars['key'] > 0 && $this->_tpl_vars['key']): ?>
                                            <span style="color:#44ba28; font-weight: bold;">
                                                <?php if ($this->_tpl_vars['setting']['type'] == 'and'): ?>�<?php endif; ?>
                                                <?php if ($this->_tpl_vars['setting']['type'] == 'or'): ?>���<?php endif; ?>
                                            </span>
                                        <?php endif; ?>
                                        <i><?php echo $this->_tpl_vars['setting']['label']; ?>
</i>
                                        <?php if ($this->_tpl_vars['setting']['id'] != 'keyword'): ?>
                                             -
                                            <?php if (((is_array($_tmp=$this->_tpl_vars['setting']['value'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp))): ?>
                                                <?php echo ((is_array($_tmp=', ')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['setting']['value']) : implode($_tmp, $this->_tpl_vars['setting']['value'])); ?>

                                            <?php else: ?>
                                                <?php echo $this->_tpl_vars['setting']['value']; ?>

                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endif; ?>

                            <?php if (isset ( $this->_tpl_vars['item']['channels'] )): ?>
                                <div style="margin-top:3px; padding:2px; background:#f4f4f6;">
                                    <b>������ ����������</b><br>
                                    <?php $_from = $this->_tpl_vars['item']['channels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ch']):
?>
                                        <a href='<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/channel/edit/<?php echo $this->_tpl_vars['ch']['id']; ?>
'><?php echo $this->_tpl_vars['ch']['name']; ?>
</a><br>
                                    <?php endforeach; endif; unset($_from); ?>
                                </div>
                            <?php endif; ?>

                        </td>
                        <td>
                            <?php if ($this->_tpl_vars['item']['cnt_banners']): ?>
                                <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/index/?filter&campaign=<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-list"></i> ������ (����� <?php echo $this->_tpl_vars['item']['cnt_banners']; ?>
)</a><br>
                            <?php else: ?>
                                -
                            <?php endif; ?>
                        </td>
                        <td>
                            <div style='padding:3px;'>
                                <?php if ($this->_tpl_vars['item']['stat']): ?>
                                    <?php echo $this->_tpl_vars['item']['stat']['impressions']; ?>
 / <b><?php echo $this->_tpl_vars['item']['stat']['clicks']; ?>
</b> / <i><?php echo $this->_tpl_vars['item']['stat']['ctr']; ?>
</i>
                                <?php else: ?>
                                    -
                                <?php endif; ?>
                            </div>
                        </td>
                        <td>
                            <div class="edit_links">
                                <div><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/add/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-plus"></i> �������� ������</a></div>
                                <div><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/edit/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-edit"></i> ������������� ��������</a></div>
                                <div><a class='confirmation' href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/delete/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-remove"></i> <span style="color:#a60018">������� �������� </span> </a></div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; endif; unset($_from); ?>
            </table>

            <?php if ($this->_tpl_vars['array_link']): ?><div style="margin: 8px; "><B>��������:  <?php $_from = $this->_tpl_vars['array_link']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['outer'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['outer']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['outer']['iteration']++;
?><?php echo $this->_tpl_vars['item']; ?>
<?php endforeach; endif; unset($_from); ?></B></div><?php endif; ?>

        <?php else: ?>
            <p>�� ����� �������� �� ��������� � �������.</p>
        <?php endif; ?>



    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>