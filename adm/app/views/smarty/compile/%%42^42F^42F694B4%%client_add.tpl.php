<?php /* Smarty version 2.6.18, created on 2014-11-21 16:34:27
         compiled from client/client_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'client/client_add.tpl', 18, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="wrapper">
    <div class="content">

        <form action="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/client/save" method="post" class="add_form form-new" id="client_add">

            <?php if (( $this->_tpl_vars['client'] )): ?><input type='hidden' name='id' value='<?php echo $this->_tpl_vars['client']['id']; ?>
'/><?php endif; ?>

            <fieldset>
                <legend><?php if ($this->_tpl_vars['client']): ?>�������������� ������� <?php echo $this->_tpl_vars['client']['name']; ?>
<?php else: ?>���������� �������<?php endif; ?></legend>

                <div class="control-group">
                    <label class="control-label" for="name">���</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" required="required" name="name" id="name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['client']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name">E-mail</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="email" id="email" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['client']['email'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name">�������</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="phone" id="phone" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['client']['phone'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">�������������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="addit" rows="5" id="addit"><?php echo $this->_tpl_vars['client']['addit']; ?>
</textarea>
                    </div>
                </div>

                <button type="submit" class="btn btn-success"><?php if ($this->_tpl_vars['client']): ?><i class="icon-ok"></i> �������� �������<?php else: ?><i class="icon-plus"></i> �������� �������<?php endif; ?></button>

            </fieldset>
        </form>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>