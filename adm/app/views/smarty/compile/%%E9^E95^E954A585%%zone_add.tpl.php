<?php /* Smarty version 2.6.18, created on 2015-05-25 16:35:13
         compiled from zone/zone_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'zone/zone_add.tpl', 55, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="wrapper">
    <div class="content">

        <form action="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/zone/save" method="post" class="add_form form-new" id="site_add">


            <fieldset>
                <legend><?php if ($this->_tpl_vars['zone']): ?>�������������� ���� <?php echo $this->_tpl_vars['zone']['name']; ?>
<?php else: ?>���������� ���� �� ���� <?php echo $this->_tpl_vars['site']['name']; ?>
<?php endif; ?></legend>

                <div class="infoplace">
                <p>��� ���������� ��������� ����������� <br><b>JQuery</b> � <b>SWFObject</b> � 	&#8249;<i>HEAD</i>&#8250;</p>
                    <textarea style="height:60px; width:300px;"><script type="text/javascript" src="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/inc/jquery.js"></script></textarea><br>
                    <textarea style="height:70px; width:300px;"><script type="text/javascript" src="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/inc/swfobject.js"></script></textarea>
                </div>

                <?php if ($this->_tpl_vars['zone']): ?>
                    <div class="infoplace2" style="clear:right;">
                        <p>��� ��� ������� ����:</i></p>
                        <?php if ($this->_tpl_vars['redirect']): ?>
                            <textarea style="height:150px; width:300px;"><img width="1" height="1" src="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/image.php?zone=<?php echo $this->_tpl_vars['zone']['id']; ?>
"><a target="_blank" href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/click.php?zone=<?php echo $this->_tpl_vars['zone']['id']; ?>
"><?php echo $this->_tpl_vars['redirect']['content']; ?>
</a></textarea>
                        <?php else: ?>
                                <?php if ($this->_tpl_vars['zone']['type'] == 'regular'): ?>
                                    <textarea style="height:100px; width:300px;'"><script type="text/javascript" src="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/rotator.php?zone=<?php echo $this->_tpl_vars['zone']['id']; ?>
"></script></textarea><br>
                                <?php elseif ($this->_tpl_vars['zone']['type'] == 'context'): ?>
                                    <textarea style="height:100px; width:300px;'"><script type="text/javascript" src="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/rotator.php?type=context&zone=<?php echo $this->_tpl_vars['zone']['id']; ?>
"></script></textarea><br>
                                <?php endif; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?php if (( $this->_tpl_vars['zone'] )): ?>
                    <input type='hidden' name='id' value='<?php echo $this->_tpl_vars['zone']['id']; ?>
'/>
                    <div class="control-group">
                        <label class="control-label" for="name">����</label>
                        <div class="controls">
                            <select name="id_site" class="input-xlarge">
                                <?php $_from = $this->_tpl_vars['site_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                    <option <?php if ($this->_tpl_vars['zone']['id_site'] == $this->_tpl_vars['item']['id']): ?> selected <?php endif; ?>value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                                <?php endforeach; endif; unset($_from); ?>
                            </select>
                        </div>
                    </div>
                <?php else: ?>
                    <input type="hidden" name="id_site" value="<?php echo $this->_tpl_vars['site']['id']; ?>
"/>
                <?php endif; ?>

                <div class="control-group">
                    <label class="control-label" for="name">��������</label>
                    <p class="clue">������: <b>zoom_header_500x200</b></p>
                    <div class="controls">
                        <input class="input-xxlarge" type="text" name="name" id="name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['zone']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="name">���</label>
                    <p class="clue" style="padding-bottom:15px;">���� � ���� ���� ����������� �������, ���������� ��� ���� <b>�����������</b>.<br> <u>� ����������� ���� �� ����� ���� �������� �� ������������ ��������, �.�. ������� ������� ������ �� �������� ��������!</u></p>
                    <div class="controls">
                        <select name="type" id="zonetype" class="input-medium">
                            <option <?php if ($this->_tpl_vars['zone']['type'] == 'regular'): ?> selected<?php endif; ?> value="regular">�������</option>
                            <option <?php if ($this->_tpl_vars['zone']['type'] == 'context'): ?> selected<?php endif; ?> value="context">�����������</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">��������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="descr" rows="3" id="descr"><?php echo $this->_tpl_vars['zone']['descr']; ?>
</textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">������� ���� �� �������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="insert_before" rows="3" id="insert_before"><?php echo $this->_tpl_vars['zone']['insert_before']; ?>
</textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">������� ���� ����� �������</label>
                    <div class="controls">
                        <textarea class="input-xxlarge text" name="insert_after" rows="3" id="insert_after"><?php echo $this->_tpl_vars['zone']['insert_after']; ?>
</textarea>
                    </div>
                </div>


                <button type="submit" class="btn btn-success"><?php if ($this->_tpl_vars['zone']): ?><i class="icon-ok"></i> �������� ����<?php else: ?><i class="icon-plus"></i> �������� ����<?php endif; ?></button>
                <?php if ($this->_tpl_vars['zone']): ?>
                <div style="float:right;">
                  <a class="confirmation btn btn-danger" href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/zone/delete/<?php echo $this->_tpl_vars['zone']['id']; ?>
"><i class="icon-remove"></i> ������� ����</a>
                </div>
                <?php endif; ?>
            </fieldset>
        </form>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>