<?php /* Smarty version 2.6.18, created on 2015-01-13 18:24:13
         compiled from zone/zone_banner.tpl */ ?>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo '
    <script>
        $(document).ready(function(){
            $("#client_list").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client_list").val(), { }, function(data){
                    $("#campaign_list").empty().append(\'<option value="0" selected>��������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#campaign_list").append(option);
                    });
                    $("#campaign_container").attr(\'style\',"display:inline-block");
                });
            });

            $("#campaign_list").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign_list").val(), { }, function(data){
                    $("#banner_list").empty().append(\'<option value="0" selected>��������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#banner_list").append(option);
                    });
                    $("#banner_container").attr(\'style\',"display:inline-block");
                });
            });

            $("#banner_list").change(function(){
                $(\'#add_button\').attr(\'style\',"display:inline-block");
            });

            $("#add_button").click(function(){
                $.post("/adm/banner/ajax_setzone", { type:1, banner:$("#banner_list").val(), zone: $("#zone_id").val() }, function(data){
                    if(data=="ok") location.reload();
                });
            });

        });

        function deletebanner(id_banner)
        {
            $.post("/adm/banner/ajax_setzone", { type:0, banner:id_banner, zone: $("#zone_id").val() }, function(data){
                location.reload();
            })
        }


    </script>
'; ?>


<div class="wrapper">
    <div class="content">

        <?php if ($this->_tpl_vars['zone']): ?>
            <div class="infoplace">
                <b>����:</b> <a href='<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/site/edit/<?php echo $this->_tpl_vars['zone']['siteid']; ?>
'><?php echo $this->_tpl_vars['zone']['sitename']; ?>
</a><br>
                <b>����:</b> <a href='<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/zone/edit/<?php echo $this->_tpl_vars['zone']['id']; ?>
'><?php echo $this->_tpl_vars['zone']['name']; ?>
</a>
            </div>
        <?php endif; ?>

        <h1>������� � ����</h1>
        <input type="hidden" id="zone_id" value="<?php echo $this->_tpl_vars['zone']['id']; ?>
"/>

        <?php if (! empty ( $this->_tpl_vars['zone']['banners'] )): ?>
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>������</th><th>������</th><th>��������</th><th>�����������</th><th>������/<b>�����</b>/<i>CTR</i></th><th>��������</th></thead>
                </thead>
                <?php $_from = $this->_tpl_vars['zone']['banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                    <tr <?php if ($this->_tpl_vars['item']['status'] == 0): ?> class='danger'<?php endif; ?>>
                        <td><?php echo $this->_tpl_vars['key']+1; ?>
</td>
                        <td>
                            <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/edit/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</a> / <span style='color:#7a7676; font-style:italic; font-size:11px;'><?php echo $this->_tpl_vars['item']['type']; ?>
</span></td>
                        <td><div style='width:450px;'><?php echo $this->_tpl_vars['item']['link']; ?>
</div></td>
                        <td><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/index/?filter&campaign=<?php echo $this->_tpl_vars['item']['id_campaign']; ?>
"><?php echo $this->_tpl_vars['item']['clientname']; ?>
 - <?php echo $this->_tpl_vars['item']['campaignname']; ?>
</a></td>
                        <td><?php echo $this->_tpl_vars['item']['chance']; ?>
 %</td>
                        <td>
                            <?php if ($this->_tpl_vars['item']['stat']): ?>
                                <?php echo $this->_tpl_vars['item']['stat']['impressions']; ?>
 / <b><?php echo $this->_tpl_vars['item']['stat']['clicks']; ?>
</b> / <i><?php echo $this->_tpl_vars['item']['stat']['ctr']; ?>
</i>
                            <?php else: ?>
                                -
                            <?php endif; ?>
                        </td>
                        <td><div class="edit_links">
                                <a href='' onclick="deletebanner(<?php echo $this->_tpl_vars['item']['id']; ?>
); return false;"><i class="icon-remove"></i> <span style="color:#a60018">������� �� ����</span></a><br>
                                <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/power/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-adjust"></i> <?php if ($this->_tpl_vars['item']['status']): ?>���������<?php else: ?>��������<?php endif; ?> ������</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; endif; unset($_from); ?>
            </table>
        <?php else: ?>
            <p>�� ������ ������� �� ��������� � ����.</p>
        <?php endif; ?>

        <div id='addbanner' style="background: #f4f4f6; padding:10px; ">
            <p><b>�������� ������</b></p>
            ������
            <select id="client_list">
                <option value="0" selected>��������</option>
                <?php $_from = $this->_tpl_vars['client_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                    <option value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
            </select>

            <div id="campaign_container">
                ��������
                <select id="campaign_list"></select>
            </div>

            <div id='banner_container'>
                ������
                <select id="banner_list"></select>
            </div>

            <a id="add_button" href="#" class="hidden-btn btn btn-success"><i class="icon-plus"></i> �������� ������</a>
        </div>


    </div>
</div>



<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>