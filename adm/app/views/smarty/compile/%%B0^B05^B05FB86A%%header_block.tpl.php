<?php /* Smarty version 2.6.18, created on 2014-10-28 15:44:28
         compiled from blocks/header_block.tpl */ ?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php if ($this->_tpl_vars['header_block']): ?><?php echo $this->_tpl_vars['header_block']->get_title(); ?>
<?php endif; ?></title>

<?php if ($this->_tpl_vars['header_block']): ?>

	<?php $_from = $this->_tpl_vars['header_block']->get_css(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['css_item']):
?>
		<link href="<?php echo $this->_tpl_vars['css_item']; ?>
" rel="stylesheet" media="screen">
	<?php endforeach; endif; unset($_from); ?>

	<?php $_from = $this->_tpl_vars['header_block']->get_js(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['js_item']):
?>
		<script src="<?php echo $this->_tpl_vars['js_item']; ?>
" language="JavaScript"></script>
	<?php endforeach; endif; unset($_from); ?>

<?php endif; ?>




</head>

<body id="main">