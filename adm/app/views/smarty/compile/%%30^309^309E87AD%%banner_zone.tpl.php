<?php /* Smarty version 2.6.18, created on 2014-12-05 12:51:36
         compiled from banner/banner_zone.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'in_array', 'banner/banner_zone.tpl', 49, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo '
    <script>
        function setzone(type,banner,zone){
            $.post("/adm/banner/ajax_setzone", { type:type, banner:banner, zone:zone }, function(data){
                if(data=="ok") location.reload();
            });
            return false;
        }
    </script>
'; ?>


<div class="wrapper">
    <div class="content">


        <div class="infoplace">
            <b>������:</b> <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/client/edit/<?php echo $this->_tpl_vars['banner']['clientid']; ?>
"><?php echo $this->_tpl_vars['banner']['clientname']; ?>
</a><br>
            <b>��������:</b> <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/<?php echo $this->_tpl_vars['banner']['campaignid']; ?>
"><?php echo $this->_tpl_vars['banner']['campaignname']; ?>
</a>
        </div>

        <h1>������ <?php echo $this->_tpl_vars['banner']['name']; ?>
 - ����</h1>

        <?php if ($this->_tpl_vars['banner']['type'] != 'redirect' && $this->_tpl_vars['banner']['type'] != 'popup'): ?>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/banner.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <?php endif; ?>

        <?php if ($this->_tpl_vars['banner']['type'] == 'redirect'): ?>
            <div style="padding:5px; border:1px dashed #ff8092; margin-bottom:10px; width:450px;">
                ������ ���� <b>��������</b> ����� ���� �������� ������ � ����� ����
            </div>
        <?php endif; ?>

        <?php if (! empty ( $this->_tpl_vars['site_list'] )): ?>
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>����</th><th>����</th></thead>
                </thead>
                <?php $_from = $this->_tpl_vars['site_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                    <tr>
                        <td><?php echo $this->_tpl_vars['key']+1; ?>
</td>
                        <td><b><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/site/edit/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</a></b><br><?php echo $this->_tpl_vars['item']['url']; ?>
</td>
                        <td>
                            <div style="float:left; width:70%;">
                                <?php $_from = $this->_tpl_vars['item']['zones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['zone']):
?>
                                    <div <?php if (((is_array($_tmp=$this->_tpl_vars['zone']['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['zones']) : in_array($_tmp, $this->_tpl_vars['zones']))): ?>class='success'<?php endif; ?> style="padding:5px; border-bottom:1px dashed #4d494a;">
                                        <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/zone/banner/<?php echo $this->_tpl_vars['zone']['id']; ?>
"><?php echo $this->_tpl_vars['zone']['name']; ?>
</a>
                                    </div>
                                <?php endforeach; endif; unset($_from); ?>
                            </div>
                            <div style='float:left; width:20%;' class="edit_links">
                                <?php $_from = $this->_tpl_vars['item']['zones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['zone']):
?>
                                    <div style="padding:5px; border-bottom:1px dashed #4d494a;">
                                        <a href="" onclick='setzone(<?php if (((is_array($_tmp=$this->_tpl_vars['zone']['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['zones']) : in_array($_tmp, $this->_tpl_vars['zones']))): ?>0<?php else: ?>1<?php endif; ?>,<?php echo $this->_tpl_vars['banner']['id']; ?>
,<?php echo $this->_tpl_vars['zone']['id']; ?>
); return false;'><?php if (((is_array($_tmp=$this->_tpl_vars['zone']['id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['zones']) : in_array($_tmp, $this->_tpl_vars['zones']))): ?><i class="icon-remove"></i> <span style="color:#a60018">������� ��������</span><?php else: ?>
                                                <?php if (! $this->_tpl_vars['full']): ?>
                                                    <i class="icon-plus"></i> �������� ��������
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                <?php endforeach; endif; unset($_from); ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; endif; unset($_from); ?>
            </table>
        <?php else: ?>
            <p>�� ������ ���� �� ��������� ����.</p>
        <?php endif; ?>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>