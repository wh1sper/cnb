<?php /* Smarty version 2.6.18, created on 2015-04-06 11:34:53
         compiled from stat/stat_day.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
    <script type="text/javascript">
        $(document).ready(function (){
            $("#period").change(function(){
                if($("#period").val() != "custom") {
                    $("#datepicker1").hide();
                    $("#datepicker2").hide();
                }
                else {
                    $("#datepicker1").show();
                    $("#datepicker2").show();
                }
            });

            $("#client").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client").val(), { }, function(data){
                    $("#campaign").empty().append(\'<option value="0" selected>��� ��������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#campaign").append(option);
                    });
                    $("#campaign").show();
                });
            });

            $("#campaign").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign").val(), { }, function(data){
                    $("#banner").empty().append(\'<option value="0" selected>��� �������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#banner").append(option);
                    });
                    $("#banner").show();
                });
            });

            $("#site").change(function(){
                $.getJSON("/adm/zone/ajax_list/" + $("#site").val(), { }, function(data){
                    $("#zone").empty().append(\'<option value="0" selected>��� ����</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#zone").append(option);
                    });
                    $("#zone").show();
                });
            });

        });


    </script>
'; ?>



<div class="wrapper">
    <div class="content">

        <h1 style="margin-bottom:25px;">���������� ������ �� <?php echo $this->_tpl_vars['date']; ?>
</h1>

        <div class="navbar">
            <div class="navbar-inner">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">

                    <div style="float:left;">

                        <select id="site" name="site">
                            <option value="0">��� �����</option>
                            <?php $_from = $this->_tpl_vars['sites']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['st']):
?>
                                <option <?php if ($this->_tpl_vars['site'] == $this->_tpl_vars['st']['id']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['st']['id']; ?>
"><?php echo $this->_tpl_vars['st']['name']; ?>
</option>
                            <?php endforeach; endif; unset($_from); ?>
                        </select>

                        <select id="zone" name="zone" <?php if (! isset ( $this->_tpl_vars['zone'] )): ?>style="display:none;"<?php endif; ?>>
                            <option value="0">��� ����</option>
                            <?php if ($this->_tpl_vars['zone']): ?>
                                <?php $_from = $this->_tpl_vars['zones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['zn']):
?>
                                    <option <?php if ($this->_tpl_vars['zone'] == $this->_tpl_vars['zn']['id']): ?> selected<?php endif; ?> value="<?php echo $this->_tpl_vars['zn']['id']; ?>
"><?php echo $this->_tpl_vars['zn']['name']; ?>
</option>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endif; ?>

                        </select>

                    </div>


                    <div style="float:left; margin-left:100px;">

                        <select id="client" name="client">
                            <option value="0">��� �������</option>
                            <?php $_from = $this->_tpl_vars['clients']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cli']):
?>
                                <option <?php if ($this->_tpl_vars['client'] == $this->_tpl_vars['cli']['id']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['cli']['id']; ?>
"><?php echo $this->_tpl_vars['cli']['name']; ?>
</option>
                            <?php endforeach; endif; unset($_from); ?>
                        </select>

                        <select id="campaign" name="campaign" <?php if (! isset ( $this->_tpl_vars['client'] )): ?>style="display: none;"<?php endif; ?>>
                            <option value="0">��� ��������</option>
                            <?php if ($this->_tpl_vars['campaigns']): ?>
                                <?php $_from = $this->_tpl_vars['campaigns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['camp']):
?>
                                    <option <?php if ($this->_tpl_vars['campaign'] == $this->_tpl_vars['camp']['id']): ?> selected<?php endif; ?> value="<?php echo $this->_tpl_vars['camp']['id']; ?>
"><?php echo $this->_tpl_vars['camp']['name']; ?>
</option>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endif; ?>
                        </select>

                        <select id="banner" name="banner" <?php if (! isset ( $this->_tpl_vars['campaign'] )): ?>style="display: none;"<?php endif; ?>>
                            <option value="0">��� �������</option>
                            <?php if ($this->_tpl_vars['banners']): ?>
                                <?php $_from = $this->_tpl_vars['banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['bann']):
?>
                                    <option <?php if ($this->_tpl_vars['banner'] == $this->_tpl_vars['bann']['id']): ?> selected<?php endif; ?> value="<?php echo $this->_tpl_vars['bann']['id']; ?>
"><?php echo $this->_tpl_vars['bann']['name']; ?>
</option>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endif; ?>
                        </select>

                    </div>

                    <div style="float:left; margin-left:50px;">
                        <button class="btn btn-success" name="filter" type="submit">�����������</button>
                    </div>

                </form>
            </div>
        </div>

        <div class="clear" style="margin-bottom:30px;"></div>

        <table class="table table-bordered table-narrow">
            <thead>
            <th>���</th><th>������</th><th>�����</th><th>CTR</th>
            </thead>
            <tbody>

            <?php $_from = $this->_tpl_vars['hours']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['i']):
?>
                <tr>
                    <td><?php echo $this->_tpl_vars['key']; ?>
:00 - <?php echo $this->_tpl_vars['key']; ?>
:59</td>
                    <td><?php echo $this->_tpl_vars['i']['imp']; ?>
</td>
                    <td><?php echo $this->_tpl_vars['i']['clk']; ?>
</td>
                    <td><?php echo $this->_tpl_vars['i']['ctr']; ?>
</td>
                </tr>
            <?php endforeach; endif; unset($_from); ?>
            <tr>
                <td>�����</td>
                <td><?php echo $this->_tpl_vars['total']['imp']; ?>
</td>
                <td><?php echo $this->_tpl_vars['total']['clk']; ?>
</td>
                <td><?php echo $this->_tpl_vars['total']['ctr']; ?>
</td>
            </tr>
            </tbody>
        </table>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>