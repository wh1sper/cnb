<?php /* Smarty version 2.6.18, created on 2015-04-29 11:13:17
         compiled from banner/banner_index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'is_array', 'banner/banner_index.tpl', 111, false),array('modifier', 'implode', 'banner/banner_index.tpl', 112, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo '
    <script type="text/javascript">
        $(document).ready(function (){

            $("#client").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client").val(), { }, function(data){
                    $("#campaign").empty().append(\'<option value="0" selected>��� ��������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#campaign").append(option);
                    });
                    $("#campaign").show();
                });
            });

            $("#campaign").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign").val(), { }, function(data){
                    $("#banner").empty().append(\'<option value="0" selected>��� �������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#banner").append(option);
                    });
                    $("#banner").show();
                });
            });


        });

    </script>
'; ?>



<div class="wrapper">
    <div class="content">

        <?php if ($this->_tpl_vars['campaign'] || $this->_tpl_vars['client']): ?>
            <div class="infoplace">
                <?php if ($this->_tpl_vars['client']): ?><b>������: </b><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/index?filter&client=<?php echo $this->_tpl_vars['client']['id']; ?>
"><?php echo $this->_tpl_vars['client']['name']; ?>
</a><br><?php endif; ?>
                <?php if ($this->_tpl_vars['campaign']): ?><b>��������: </b><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/edit/<?php echo $this->_tpl_vars['campaign']['id']; ?>
"><?php echo $this->_tpl_vars['campaign']['name']; ?>
</a><?php endif; ?>
            </div>
        <?php endif; ?>

        <h1>�������</h1>
            <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/<?php if ($this->_tpl_vars['campaign']): ?>add/<?php echo $this->_tpl_vars['campaign']['id']; ?>
<?php else: ?>addraw<?php endif; ?>" style='margin-top:20px;' class="btn btn-success"><i class="icon-plus"></i> �������� ������</a>

        <div class="navbar" style='clear:both; margin-top:20px;'>
            <div class="navbar-inner">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">
                    <select id="client" style="width:300px !important;" name="client">
                        <option value="0">��� �������</option>
                        <?php $_from = $this->_tpl_vars['clients']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cl']):
?>
                            <option <?php if ($this->_tpl_vars['cl']['id'] == $this->_tpl_vars['client']['id']): ?> selected <?php endif; ?>value="<?php echo $this->_tpl_vars['cl']['id']; ?>
"><?php echo $this->_tpl_vars['cl']['name']; ?>
</option>
                        <?php endforeach; endif; unset($_from); ?>
                    </select>
                    <select id="campaign" class="input-xlarge" name="campaign" style="width:300px !important; <?php if (! isset ( $this->_tpl_vars['client'] )): ?>display: none;<?php endif; ?>">
                        <option value="0">��� ��������</option>
                        <?php if ($this->_tpl_vars['campaigns']): ?>
                            <?php $_from = $this->_tpl_vars['campaigns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['camp']):
?>
                                <option <?php if ($this->_tpl_vars['campaign']['id'] == $this->_tpl_vars['camp']['id']): ?> selected<?php endif; ?> value="<?php echo $this->_tpl_vars['camp']['id']; ?>
"><?php echo $this->_tpl_vars['camp']['name']; ?>
</option>
                            <?php endforeach; endif; unset($_from); ?>
                        <?php endif; ?>
                    </select>
                    <input type="text" style="width:300px !important;" name="name" placeholder="��������" value="<?php echo $this->_tpl_vars['name']; ?>
"/>

                    <button class="btn btn-primary" name="filter" type="submit">�����������</button>


                </form>
            </div>
        </div>


        <?php if (! empty ( $this->_tpl_vars['banner_list'] )): ?>
            <table class="table table-bordered">
                <thead>
                <th>�</th><th>������</th>
                <?php if (! $this->_tpl_vars['campaign']): ?><th>��������</th><?php endif; ?>
                <th>���������</th><th>����</th><th>������/<b>�����</b>/<i>CTR</i></th><th>��������</th></thead>
                </thead>
                <?php $_from = $this->_tpl_vars['banner_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                    <tr class="<?php if (! $this->_tpl_vars['item']['status']): ?>danger<?php endif; ?>">
                        <td><?php echo $this->_tpl_vars['item']['id']; ?>
</td>
                        <td>
                            <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/edit/<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['bannername']; ?>
</a>  <span style='color:#7a7676; font-style:italic; font-size:11px;'><?php echo $this->_tpl_vars['item']['type']; ?>
</span>
                            <div style='width:400px; overflow-x:hidden; text-overflow:ellipsis; padding-top:4px; font-weight: bold'><?php echo $this->_tpl_vars['item']['link']; ?>
</div>
                            <?php if (isset ( $this->_tpl_vars['item']['settings'] )): ?>

                            <?php endif; ?>
                        </td>
                        <?php if (! $this->_tpl_vars['campaign']): ?>
                            <td style="max-width:250px;"><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/index/?filter&campaign=<?php echo $this->_tpl_vars['item']['id_campaign']; ?>
"><?php echo $this->_tpl_vars['item']['clientname']; ?>
 - <?php echo $this->_tpl_vars['item']['name']; ?>
</a></td>
                        <?php endif; ?>


                        <td style="max-width:250px; font-size:12px;">
                            <?php if (isset ( $this->_tpl_vars['item']['settings'] )): ?>
                                <?php $_from = $this->_tpl_vars['item']['settings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['setting']):
?>
                                    <div style="font-size:12px;">
                                        <?php if ($this->_tpl_vars['key'] > 0): ?>
                                            <span style="color:#44ba28; font-weight: bold;">
                                                <?php if ($this->_tpl_vars['setting']['type'] == 'and'): ?>�<?php endif; ?>
                                                <?php if ($this->_tpl_vars['setting']['type'] == 'or'): ?>���<?php endif; ?>
                                            </span>
                                        <?php endif; ?>
                                        <i><?php echo $this->_tpl_vars['setting']['label']; ?>
</i> -
                                        <?php if (((is_array($_tmp=$this->_tpl_vars['setting']['value'])) ? $this->_run_mod_handler('is_array', true, $_tmp) : is_array($_tmp))): ?>
                                            <?php echo ((is_array($_tmp=', ')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['setting']['value']) : implode($_tmp, $this->_tpl_vars['setting']['value'])); ?>

                                        <?php else: ?>
                                            <?php echo $this->_tpl_vars['setting']['value']; ?>

                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; endif; unset($_from); ?>
                            <?php endif; ?>

                            <?php if (isset ( $this->_tpl_vars['item']['channels'] )): ?>
                                <div style="margin-top:3px; padding:2px; background:#f4f4f6;">
                                    <b>������ ����������</b><br>
                                    <?php $_from = $this->_tpl_vars['item']['channels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ch']):
?>
                                        <a href='<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/channel/edit/<?php echo $this->_tpl_vars['ch']['id']; ?>
'><?php echo $this->_tpl_vars['ch']['name']; ?>
</a><br>
                                    <?php endforeach; endif; unset($_from); ?>
                                </div>
                            <?php endif; ?>

                        </td>

                        <td style="padding-right:0;">
                            <?php $_from = $this->_tpl_vars['item']['zones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['zone']):
?>
                                <div style='padding:3px; border-bottom:1px dashed #4d494a;'><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/zone/banner/<?php echo $this->_tpl_vars['zone']['id']; ?>
"><?php echo $this->_tpl_vars['zone']['name']; ?>
</a></div>
                            <?php endforeach; endif; unset($_from); ?>
                        </td>

                        <td style="padding-left:0; border-left:0;">
                            <?php $_from = $this->_tpl_vars['item']['zones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['zone']):
?>
                                <div style='padding:3px; border-bottom:1px dashed #4d494a;'>
                                    <?php if ($this->_tpl_vars['zone']['stat']): ?>
                                        <?php echo $this->_tpl_vars['zone']['stat']['impressions']; ?>
 / <b><?php echo $this->_tpl_vars['zone']['stat']['clicks']; ?>
</b> / <i><?php echo $this->_tpl_vars['zone']['stat']['ctr']; ?>
</i>
                                    <?php else: ?>
                                        -
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; endif; unset($_from); ?>
                            <div style='padding:3px; color:#000; background: #e0f3ef;'>�����: <?php echo $this->_tpl_vars['item']['stat']['impressions']; ?>
 / <b><?php echo $this->_tpl_vars['item']['stat']['clicks']; ?>
</b> / <i><?php echo $this->_tpl_vars['item']['stat']['ctr']; ?>
</i></div>
                        </td>

                        <td style="min-width:120px;">
                            <div class="edit_links">
                                <div><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/zone/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-list"></i> ��������� ���</a></div>
                                <div><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/power/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-adjust"></i> <?php if ($this->_tpl_vars['item']['status']): ?>���������<?php else: ?>��������<?php endif; ?></a></div>
                                <div><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/copy/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-edit"></i> ����������</a></div>
                                <div><a class='confirmation' href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/banner/delete/<?php echo $this->_tpl_vars['item']['id']; ?>
"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a></div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; endif; unset($_from); ?>
            </table>

            <?php if ($this->_tpl_vars['array_link']): ?><div style="margin: 8px; "><B>��������:  <?php $_from = $this->_tpl_vars['array_link']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['outer'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['outer']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['outer']['iteration']++;
?><?php echo $this->_tpl_vars['item']; ?>
<?php endforeach; endif; unset($_from); ?></B></div><?php endif; ?>

        <?php else: ?>
            <p>�� ������ ������� �� ��������� � �������.</p>
        <?php endif; ?>



    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>