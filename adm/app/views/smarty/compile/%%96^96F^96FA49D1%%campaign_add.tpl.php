<?php /* Smarty version 2.6.18, created on 2015-03-13 10:58:19
         compiled from campaign/campaign_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'campaign/campaign_add.tpl', 62, false),array('modifier', 'date_format', 'campaign/campaign_add.tpl', 84, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="wrapper">
    <div class="content">
        <?php echo '
        <script>
            $(function() {
                $( "#tabs" ).tabs();
            });
        </script>
        '; ?>



        <form action="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/campaign/save" method="post" class="add_form form-new" id="campaign_add">

            <fieldset>
            <?php if (( $this->_tpl_vars['campaign'] )): ?>
                <legend style="border-bottom: 0px;">�������������� �������� <?php echo $this->_tpl_vars['campaign']['name']; ?>
</legend>
                <input type='hidden' name='id' value='<?php echo $this->_tpl_vars['campaign']['id']; ?>
'/>
            <?php else: ?>
                <legend style="border-bottom: 0px;">���������� ��������</legend>
            <?php endif; ?>

            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">���������</a></li>
                    <li><a href="#tabs-2">����������� ������</a></li>
                    <li><a href="#tabs-3">������ ����������</a></li>
                </ul>
                <div id="tabs-1">
                    <?php if (isset ( $this->_tpl_vars['client'] )): ?>
                        <input type="hidden" name="id_client" value="<?php echo $this->_tpl_vars['client']['id']; ?>
"/>
                    <?php else: ?>
                        <div class="control-group">
                            <label class="control-label" for="name">������</label>
                            <div class="controls">
                                <select name="id_client" class="input-xlarge">
                                    <?php $_from = $this->_tpl_vars['client_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                                        <option <?php if ($this->_tpl_vars['campaign']): ?><?php if ($this->_tpl_vars['campaign']['id_client'] == $this->_tpl_vars['item']['id']): ?> selected <?php endif; ?><?php endif; ?>value="<?php echo $this->_tpl_vars['item']['id']; ?>
"><?php echo $this->_tpl_vars['item']['name']; ?>
</option>
                                    <?php endforeach; endif; unset($_from); ?>
                                </select>
                            </div>
                        </div>
                    <?php endif; ?>


                    <div class="control-group">
                        <label class="control-label" for="name">���</label>
                        <div class="controls">
                            <select name="campaign_type" class="input-xlarge">
                                <option value="regular">�����������</option>
                                <option value="exclusive"<?php if ($this->_tpl_vars['campaign']['type'] == 'exclusive'): ?> selected<?php endif; ?>>������������</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name">���������</label>
                        <div class="controls">
                            <input class="input-small" type="text" name="weight" id="weight" value="<?php if ($this->_tpl_vars['campaign']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['campaign']['weight'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
<?php else: ?>1<?php endif; ?>" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name">��������</label>
                        <div class="controls">
                            <input class="input-xxlarge" type="text" name="name" required='required' id="name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['campaign']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="name">��������</label>
                        <div class="controls">
                            <textarea class="input-xxlarge text" name="descr" rows="5" id="article_text"><?php echo $this->_tpl_vars['campaign']['descr']; ?>
</textarea>
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="name">���� ������</label>
                        <div class="controls">
                            <input class="input-xlarge select_date" type="text" name="date_start" id="date_start_picker" value="<?php if ($this->_tpl_vars['campaign']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['campaign']['date_start'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%d.%m.%Y %H:%M:%S') : smarty_modifier_date_format($_tmp, '%d.%m.%Y %H:%M:%S')); ?>
<?php endif; ?>" />
                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="name">���� ����������</label>
                        <div class="controls">
                            <input class="input-xlarge select_date" type="text" name="date_end" id="date_end_picker" value="<?php if ($this->_tpl_vars['campaign']): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['campaign']['date_end'])) ? $this->_run_mod_handler('date_format', true, $_tmp, '%d.%m.%Y %H:%M:%S') : smarty_modifier_date_format($_tmp, '%d.%m.%Y %H:%M:%S')); ?>
<?php endif; ?>" />
                        </div>
                    </div>

                </div>

                <div id="tabs-2">
                    <label>����������� ������</label>

                    <div id='settings_container' class="control-group" style="padding:10px;">
                        <?php if (isset ( $this->_tpl_vars['campaign']['settings'] )): ?>
                            <?php $_from = $this->_tpl_vars['campaign']['settings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['setting']):
?>
                                <div class='setting_row' id="row_<?php echo $this->_tpl_vars['key']; ?>
">
                                    <?php if ($this->_tpl_vars['key'] > 0): ?>
                                        <select class='input-small' name="type[<?php echo $this->_tpl_vars['key']; ?>
]">
                                            <option <?php if ($this->_tpl_vars['setting']['type'] == 'and'): ?>selected="selected"<?php endif; ?> value="and">�</option>
                                            <option <?php if ($this->_tpl_vars['setting']['type'] == 'or'): ?>selected="selected"<?php endif; ?> value="or">���</option>
                                        </select>
                                    <?php endif; ?>
                                    <select id='setting_name_<?php echo $this->_tpl_vars['key']; ?>
' class="setting_name input-xlarge" name="setting[<?php echo $this->_tpl_vars['key']; ?>
]">
                                        <?php $_from = $this->_tpl_vars['setting_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['set']):
?>
                                            <option <?php if ($this->_tpl_vars['set']['id'] == $this->_tpl_vars['setting']['id']): ?>selected="selected"<?php endif; ?> value="<?php echo $this->_tpl_vars['set']['id']; ?>
"><?php echo $this->_tpl_vars['set']['label']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                    <span id='setting_value_container_<?php echo $this->_tpl_vars['key']; ?>
'>
                                        <?php if ($this->_tpl_vars['setting']['id'] == 'limit_days'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_days.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_time'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_time.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_geo' || $this->_tpl_vars['setting']['id'] == 'limit_geo_state' || $this->_tpl_vars['setting']['id'] == 'limit_geo_region'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_geo.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'limit_mobile'): ?>
                                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/field_mobile.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                        <?php elseif ($this->_tpl_vars['setting']['id'] == 'keyword'): ?>
                                            <textarea  style='height:100px;' class="input-xxlarge" name="value[<?php echo $this->_tpl_vars['key']; ?>
]"><?php echo $this->_tpl_vars['setting']['value']; ?>
</textarea>
                                        <?php else: ?>
                                            <input type="text" class="input-xxlarge" name="value[<?php echo $this->_tpl_vars['key']; ?>
]" value="<?php echo $this->_tpl_vars['setting']['value']; ?>
"/>
                                        <?php endif; ?>

                                    </span>
                                    <a href="" onclick="deleterow(<?php echo $this->_tpl_vars['key']; ?>
); return false;"><i class="icon-remove"></i> <span style="color:#a60018">�������</span></a>
                                </div>
                            <?php endforeach; endif; unset($_from); ?>
                        <?php endif; ?>
                    </div>

                    <a onclick="addrow(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� ���������</a>


                </div>

                <div id="tabs-3">

                    <label>������ ����������</label>

                    <div id="channels_container">
                        <?php if (isset ( $this->_tpl_vars['campaign']['channels'] )): ?>
                            <?php $_from = $this->_tpl_vars['campaign']['channels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['channel']):
?>
                                <div class="channel_row" id="channel_row_<?php echo $this->_tpl_vars['key']; ?>
">
                                    <select class='input-xxlarge' name="channel[<?php echo $this->_tpl_vars['key']; ?>
]">
                                        <?php $_from = $this->_tpl_vars['channel_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ch']):
?>
                                            <option value="<?php echo $this->_tpl_vars['ch']['id']; ?>
" <?php if ($this->_tpl_vars['ch']['id'] == $this->_tpl_vars['channel']['id_channel']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['ch']['name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </select>
                                    <a href='' onclick='delete_channel_row("<?php echo $this->_tpl_vars['key']; ?>
"); return false;'><i class='icon-remove'></i> <span style='color:#a60018'>�������</span></a>
                                </div>
                            <?php endforeach; endif; unset($_from); ?>
                        <?php endif; ?>
                    </div>

                    <a onclick="add_channel_row(); return false;" href="" class="btn btn-primary"><i class="icon-plus"></i> �������� �����</a>

                </div>


            </div>


            <button type="submit" style='margin-top:30px;' class="btn btn-success"><?php if ($this->_tpl_vars['campaign']): ?> �������� ��������<?php else: ?><i class="icon icon-plus"></i> �������� ��������<?php endif; ?></button>



            </fieldset>
            </div>

            <div class="clearfix"></div>

        </form>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>