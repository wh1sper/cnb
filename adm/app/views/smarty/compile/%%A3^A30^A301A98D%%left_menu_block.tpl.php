<?php /* Smarty version 2.6.18, created on 2015-04-06 14:06:29
         compiled from blocks/left_menu_block.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'blocks/left_menu_block.tpl', 6, false),)), $this); ?>
<div class="left_menu">


<ul class="nav nav-list">
	<?php $_from = $this->_tpl_vars['left_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['lmenu']):
?>
		<li class="nav-header"><?php echo ((is_array($_tmp=$this->_tpl_vars['lmenu']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>

			<ul>
			<?php $_from = $this->_tpl_vars['lmenu']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ch_menu']):
?>
				<li<?php if (! empty ( $this->_tpl_vars['ch_menu']['active'] )): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
<?php echo $this->_tpl_vars['ch_menu']['url']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['ch_menu']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
</a></li>
			<?php endforeach; endif; unset($_from); ?>
			</ul>
		</li>
	<?php endforeach; endif; unset($_from); ?>
</ul>

</div>