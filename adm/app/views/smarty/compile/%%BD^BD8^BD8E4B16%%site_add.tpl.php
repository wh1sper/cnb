<?php /* Smarty version 2.6.18, created on 2014-10-29 13:47:34
         compiled from site/site_add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'strip_tags', 'site/site_add.tpl', 18, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div class="wrapper">
    <div class="content">

        <form action="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/site/save" method="post" class="add_form form-new" id="site_add">

            <?php if (( $this->_tpl_vars['site'] )): ?><input type='hidden' name='id' value='<?php echo $this->_tpl_vars['site']['id']; ?>
'/><?php endif; ?>

            <fieldset>
                <legend><?php if ($this->_tpl_vars['site']): ?>�������������� ����� <?php echo $this->_tpl_vars['site']['name']; ?>
<?php else: ?>���������� �����<?php endif; ?></legend>

                <div class="control-group">
                    <label class="control-label" for="name">��������</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="name" id="name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['site']['name'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="url">URL</label>
                    <div class="controls">
                        <input class="input-xlarge" type="text" name="url" id="url" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['site']['url'])) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)); ?>
" />
                    </div>
                </div>

                <button type="submit" class="btn btn-success"><?php if ($this->_tpl_vars['site']): ?><i class="icon-ok"></i> �������� ����<?php else: ?><i class="icon-plus"></i> �������� ����<?php endif; ?></button>
            </fieldset>
        </form>

    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>