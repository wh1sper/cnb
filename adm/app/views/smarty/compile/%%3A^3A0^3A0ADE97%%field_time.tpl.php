<?php /* Smarty version 2.6.18, created on 2014-11-27 15:51:26
         compiled from blocks/field_time.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'in_array', 'blocks/field_time.tpl', 4, false),)), $this); ?>
<table cellspacing="0" class="timetable">
    <tbody>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="0" <?php if (((is_array($_tmp='0')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 0:00-0:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="1" <?php if (((is_array($_tmp='1')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 1:00-1:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="2" <?php if (((is_array($_tmp='2')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 2:00-2:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="3" <?php if (((is_array($_tmp='3')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 3:00-3:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="4" <?php if (((is_array($_tmp='4')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 4:00-4:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="5" <?php if (((is_array($_tmp='5')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 5:00-5:59</label></td>
    </tr>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="6" <?php if (((is_array($_tmp='6')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 6:00-6:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="7" <?php if (((is_array($_tmp='7')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 7:00-7:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="8" <?php if (((is_array($_tmp='8')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 8:00-8:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="9" <?php if (((is_array($_tmp='9')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 9:00-9:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="10" <?php if (((is_array($_tmp='10')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 10:00-10:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="11" <?php if (((is_array($_tmp='11')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 11:00-11:59</label></td>
    </tr>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="12" <?php if (((is_array($_tmp='12')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 12:00-12:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="13" <?php if (((is_array($_tmp='13')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 13:00-13:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="14" <?php if (((is_array($_tmp='14')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 14:00-14:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="15" <?php if (((is_array($_tmp='15')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 15:00-15:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="16" <?php if (((is_array($_tmp='16')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 16:00-16:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="17" <?php if (((is_array($_tmp='17')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 17:00-17:59</label></td>
    </tr>
    <tr>
        <td class="checkbox"><label><input type="checkbox" value="18" <?php if (((is_array($_tmp='18')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 18:00-18:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="19" <?php if (((is_array($_tmp='19')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 19:00-19:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="20" <?php if (((is_array($_tmp='20')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 20:00-20:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="21" <?php if (((is_array($_tmp='21')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 21:00-21:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="22" <?php if (((is_array($_tmp='22')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 22:00-22:59</label></td>
        <td class="checkbox"><label><input type="checkbox" value="23" <?php if (((is_array($_tmp='23')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['setting']['value']) : in_array($_tmp, $this->_tpl_vars['setting']['value']))): ?> checked <?php endif; ?> name="value[<?php echo $this->_tpl_vars['key']; ?>
][]"> 23:00-23:59</label></td>
    </tr>
    </tbody>
</table>