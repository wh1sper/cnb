<?php /* Smarty version 2.6.18, created on 2015-03-02 17:03:12
         compiled from stat/stat_index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'string_format', 'stat/stat_index.tpl', 179, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/header_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/breadcrumbs_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/left_menu_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php echo '
    <script type="text/javascript">
        $(document).ready(function (){
            $("#period").change(function(){
                if($("#period").val() != "custom") {
                    $("#datepicker1").hide();
                    $("#datepicker2").hide();
                }
                else {
                    $("#datepicker1").show();
                    $("#datepicker2").show();
                }
            });

            $("#client").change(function(){
                $.getJSON("/adm/campaign/ajax_list/" + $("#client").val(), { }, function(data){
                    $("#campaign").empty().append(\'<option value="0" selected>��� ��������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#campaign").append(option);
                    });
                    $("#campaign").show();
                });
            });

            $("#campaign").change(function(){
                $.getJSON("/adm/banner/ajax_list/" + $("#campaign").val(), { }, function(data){
                    $("#banner").empty().append(\'<option value="0" selected>��� �������</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#banner").append(option);
                    });
                    $("#banner").show();
                });
            });

            $("#site").change(function(){
                $.getJSON("/adm/zone/ajax_list/" + $("#site").val(), { }, function(data){
                    $("#zone").empty().append(\'<option value="0" selected>��� ����</option>\');
                    $(data).each( function (index, element){
                        var option = $("<option value=\'"+element.id+"\'>"+element.name+"</option>");
                        $("#zone").append(option);
                    });
                    $("#zone").show();
                });
            });

        });


    </script>
'; ?>



<div class="wrapper">
    <div class="content">

        <h1 style="margin-bottom:25px;">����������</h1>

        <div class="navbar">
            <div class="navbar-inner" style="padding-bottom:10px; padding-top:10px;">
                <form method="get"  id="form_search" class="navbar-form form-inline pull-left" action="">

                   <table class="filter-table">
                        <tr>
                            <td>����: </td>
                            <td>
                                <select id="period" name="period" class="input-large">
                                    <option <?php if ($this->_tpl_vars['period'] == 'week'): ?> selected <?php endif; ?>value="week">������</option>
                                    <option <?php if ($this->_tpl_vars['period'] == 'month'): ?> selected <?php endif; ?>value="month">�����</option>
                                    <option <?php if ($this->_tpl_vars['period'] == 'custom'): ?> selected <?php endif; ?>value="custom">�������� ����</option>
                                </select>
                            </td>
                            <td>
                                <input class="input-large select_date" type="text" name="date_from" id="datepicker1" value="<?php echo $this->_tpl_vars['date_from']; ?>
" />
                            </td>
                            <td colspan="2">
                                <input class="input-large select_date" type="text" name="date_to" id="datepicker2" value="<?php echo $this->_tpl_vars['date_to']; ?>
" />
                            </td>
                        </tr>
                        <tr>
                            <td>����: </td>
                            <td>
                                <select id="site" name="site" class="input-large">
                                    <option value="0">��� �����</option>
                                    <?php $_from = $this->_tpl_vars['sites']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['st']):
?>
                                        <option <?php if ($this->_tpl_vars['site'] == $this->_tpl_vars['st']['id']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['st']['id']; ?>
"><?php echo $this->_tpl_vars['st']['name']; ?>
</option>
                                    <?php endforeach; endif; unset($_from); ?>
                                </select>
                            </td>
                            <td colspan="2">
                                <select id="zone" class="input-large" name="zone" <?php if (! isset ( $this->_tpl_vars['zones'] )): ?>style="display:none;"<?php endif; ?>>
                                    <option value="0">��� ����</option>
                                    <?php if ($this->_tpl_vars['zones']): ?>
                                        <?php $_from = $this->_tpl_vars['zones']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['zn']):
?>
                                            <option <?php if ($this->_tpl_vars['zone'] == $this->_tpl_vars['zn']['id']): ?> selected<?php endif; ?> value="<?php echo $this->_tpl_vars['zn']['id']; ?>
"><?php echo $this->_tpl_vars['zn']['name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    <?php endif; ?>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>������: </td>
                            <td>
                                <select id="client" class="input-large" name="client">
                                    <option value="0">��� �������</option>
                                    <?php $_from = $this->_tpl_vars['clients']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['cli']):
?>
                                        <option <?php if ($this->_tpl_vars['client'] == $this->_tpl_vars['cli']['id']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['cli']['id']; ?>
"><?php echo $this->_tpl_vars['cli']['name']; ?>
</option>
                                    <?php endforeach; endif; unset($_from); ?>
                                </select>
                            </td>
                            <td>
                                <select id="campaign" class="input-large" name="campaign" <?php if (! isset ( $this->_tpl_vars['campaigns'] )): ?>style="display: none;"<?php endif; ?>>
                                    <option value="0">��� ��������</option>
                                    <?php if ($this->_tpl_vars['campaigns']): ?>
                                        <?php $_from = $this->_tpl_vars['campaigns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['camp']):
?>
                                            <option <?php if ($this->_tpl_vars['campaign'] == $this->_tpl_vars['camp']['id']): ?> selected<?php endif; ?> value="<?php echo $this->_tpl_vars['camp']['id']; ?>
"><?php echo $this->_tpl_vars['camp']['name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    <?php endif; ?>
                                </select>
                            </td>
                            <td>
                                <select id="banner" class="input-large" name="banner" <?php if (! isset ( $this->_tpl_vars['campaign'] )): ?>style="display: none;"<?php endif; ?>>
                                    <option value="0">��� �������</option>
                                    <?php if ($this->_tpl_vars['banners']): ?>
                                        <?php $_from = $this->_tpl_vars['banners']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['bann']):
?>
                                            <option <?php if ($this->_tpl_vars['banner'] == $this->_tpl_vars['bann']['id']): ?> selected<?php endif; ?> value="<?php echo $this->_tpl_vars['bann']['id']; ?>
"><?php echo $this->_tpl_vars['bann']['name']; ?>
</option>
                                        <?php endforeach; endif; unset($_from); ?>
                                    <?php endif; ?>
                                </select>
                            </td>
                        </tr>
                       <tr>
                           <td colspan="4" style="padding-top:6px;">
                               <button class="btn btn-primary" name="filter" type="submit">�����������</button>
                               <a class="btn btn-default" href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/stat/export/days/?<?php echo $this->_tpl_vars['getstr']; ?>
">�������</a>
                           </td>
                       </tr>
                   </table>


                </form>
            </div>
        </div>

        <div class="clear" style="margin-bottom:30px;"></div>

        <?php if (isset ( $this->_tpl_vars['textdata'] )): ?>
                    <?php endif; ?>
        <table class="table table-bordered">
            <thead>
            <th>����</th><th>������</th><th>�����</th><th>CTR</th>
            </thead>
            <tbody>
                <?php $_from = $this->_tpl_vars['dates']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['date']):
?>
                    <tr>
                        <td>
                            <?php if ($this->_tpl_vars['date']['clk']): ?>
                                <a href="<?php echo $this->_tpl_vars['DOMAIN']; ?>
/adm/stat/day/<?php echo $this->_tpl_vars['date']['date']; ?>
<?php if (isset ( $this->_tpl_vars['getstr'] )): ?>/?<?php echo $this->_tpl_vars['getstr']; ?>
<?php endif; ?>"><?php if ($this->_tpl_vars['date']['date'] == $this->_tpl_vars['today']): ?><b><?php echo $this->_tpl_vars['date']['date']; ?>
</b><?php else: ?><?php echo $this->_tpl_vars['date']['date']; ?>
<?php endif; ?></a>
                            <?php else: ?>
                                <?php if ($this->_tpl_vars['date']['date'] == $this->_tpl_vars['today']): ?><b><?php echo $this->_tpl_vars['date']['date']; ?>
</b><?php else: ?><?php echo $this->_tpl_vars['date']['date']; ?>
<?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td><?php echo $this->_tpl_vars['date']['imp']; ?>
</td>
                        <td><?php echo $this->_tpl_vars['date']['clk']; ?>
</td>
                        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['date']['ctr'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</td>
                    </tr>
                <?php endforeach; endif; unset($_from); ?>
            <tr>
                <td>�����:</td>
                <td><?php echo $this->_tpl_vars['total_imp']; ?>
</td>
                <td><?php echo $this->_tpl_vars['total_clicks']; ?>
</td>
                <td><?php echo $this->_tpl_vars['total_ctr']; ?>
</td>
            </tr>
            </tbody>
        </table>


    </div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/footer_block.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>