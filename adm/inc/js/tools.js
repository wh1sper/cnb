
var Tools = new function()
{
	var instance = false;
	
	//constructor
	var _constructor = function ()
	{
		if (instance)
			return instance;
		instance = this;
		
		//private
		var random = Math.random();
		
		//public
		this.get_random = function(){ return random; };
		
	};
	
	//static method
	_constructor.prop2 = function(){ return Math.random(); };
	
	//public
	_constructor.prototype = {
		
		prop3 : function(){ return this.prop1(); },
		prop4 : function(){ return this.prop3(); },
		getRandom : function(){ return this.get_random(); }
	
	};
	
	return _constructor;
	
};

$(document).ready(function(){


		$('#date_picker').datetimepicker({
			timeFormat:'HH:mm:ss',
			dateFormat:'dd.mm.yy'
		});
		$('#date_start_picker').datetimepicker({
			timeFormat:'HH:mm:ss',
			dateFormat:'dd.mm.yy'
		});
		$('#date_end_picker').datetimepicker({
			timeFormat:'HH:mm:ss',
			dateFormat:'dd.mm.yy'
		});
	
	//��������� ���������
	$.datepicker.regional['ru'] = {
		closeText: '�������',
		prevText: '<����',
		nextText: '����>',
		currentText: '�������',
		monthNames: ['������','�������','����','������','���','����',
		'����','������','��������','�������','������','�������'],
		monthNamesShort: ['���','���','���','���','���','���',
		'���','���','���','���','���','���'],
		dayNames: ['�����������','�����������','�������','�����','�������','�������','�������'],
		dayNamesShort: ['���','���','���','���','���','���','���'],
		dayNamesMin: ['��','��','��','��','��','��','��'],
		weekHeader: '��',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'});
    $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd'})

    $(".setting_name").live("change",function() {
        var id = $(this).attr('id');
        id = id.slice(13);

        $("#setting_value_container_"+id).empty();
        $.get("/adm/channel/get_setting_field", { name:$(this).val(), id:id }, function(data) {
            $("#setting_value_container_"+id).html(data);
        });


    });

    $(".channel_name").live("change",function() {
        var id = $(this).attr('id');
        id = id.slice(8);
        $("#viewlink_"+id).attr('href','/adm/channel/edit/' + $(this).val());
    });

});



function addrow()
{
    var count = $(".setting_row").length;
    var type = '';
    if(count) {
        type = "<select class='input-xsmall' name='type["+count+"]'><option value='and'>�</option><option value='or'>���</option></select>";
    }
    var row = $("<div id='row_"+count+"' class='setting_row'>"+type+"<select name='setting["+count+"]' class='input-xlarge setting_name' id='setting_name_"+count+"'></select> <span id='setting_value_container_"+count+"'><input type='text' class='input-xxlarge' name='value["+count+"]'> </span> <a href=''  onclick='deleterow("+count+"); return false;'><i class='icon-remove'></i> <span style='color:#a60018'>�������</span></a></div>");
    $("#settings_container").append(row);

    $.getJSON("/adm/channel/ajax_list_settings", function(data) {

        $(data).each( function (index, element){
            var option = $("<option value='"+element.id+"'>"+element.label+"</option>");
            $("#setting_name_"+count).append(option);
        });

    });
}

function deleterow(id)
{
    $("#row_"+id).remove();
}

function add_channel_row()
{
    var count = $(".channel_row").length;
    var row = $("<div id='channel_row_"+count+"' class='channel_row'><select name='channel["+count+"]' class='input-xxlarge channel_name' id='channel_"+count+"'></select> <a href='' onclick='delete_channel_row("+count+"); return false;'><i class='icon-remove'></i> <span style='color:#a60018'>�������</span></a></div>");
    $("#channels_container").append(row);

    $.getJSON("/adm/channel/ajax_list", function(data) {

        $(data).each( function (index, element){
            var option = $("<option value='"+element.id+"'>"+element.name+"</option>");
            $("#channel_"+count).append(option);
            if(index==0)
            {
                var viewlink = $("<a id='viewlink_"+count+"' style='display:inline-block; margin-left:20px;' target='_blank' href='/adm/channel/edit/"+element.id+"'><i class='icon-list'></i> ��������</a>");
                $("#channel_row_"+count).append(viewlink);
            }
        });

    });

}

function delete_channel_row(id)
{
    $("#channel_row_"+id).remove();
}

