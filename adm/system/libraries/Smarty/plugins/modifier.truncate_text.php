<?php 

function smarty_modifier_truncate_text($text, $length = 80, $etc = '...')
{
    
	if ($length == 0)
        return '';


		if (strlen($text) > $length)
		{
			$t_text = "";
			
			if (preg_match_all("/([^(\s|\,|\.)]+)(\s{1,}|\,|\.)/", $text, $matches))
			{
				
				if (!empty($matches[1]))
				{
					foreach ($matches[1] as $key=>$val)
					{
						if (strlen($t_text) < $length)
						{
							$separator = $matches[2][$key];
							$t_text .= $val;
							if (strlen($t_text) < $length)
								$t_text .= $separator;
						}
						else 
						{
							//$t_text .= "...";
							break;
						}
					}
				}
				
			}
			
		}
		else 
		{
			$t_text = $text;
		}
		
		return $t_text;
        
        
}


?>