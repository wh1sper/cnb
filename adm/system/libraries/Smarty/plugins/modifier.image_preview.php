<?php

function smarty_modifier_image_preview($image_origin, $preview)
{
    return preg_replace("/\.(gif|jpg|jpeg|png)$/i", "_{$preview}.$1", $image_origin);
}

?>