<?php

/**
 * ����������� ����� ������ ��������
 * @author ashmits by 27.12.2012 12:39
 * @param array $params - ������ ��������
 * @param object $smarty - ������������
 * @param int $level - ������� ����������� �������
 * @return string template
 */
function smarty_function_get_randomizer_sections($params, &$smarty, $level = 1, $type = 'item')
{
//    echo '<PRE>';
////    var_dump($params);
//    var_dump($current_randomizer);
//    echo '</PRE>';

    if (!empty($params['randomizer']))
    {

        if (!empty($params['type']))
        {
            $type = $params['type'];
        }

        $smarty->assign('randomizer', $params['randomizer']);
        $smarty->assign('level', $level);
        $smarty->assign('levelspace', str_repeat("&nbsp;", $level*3) );

        switch ($type)
        {
            case "option":
                $template[] = $smarty->fetch('admin/randomizer/randomizer_node_option.tpl');
                break;
            default:
                $template[] = $smarty->fetch('admin/randomizer/randomizer_tree_row.tpl');
                break;
        }

        if (!empty($params['randomizer']['children']))
        {
            $level += 1;
            foreach ($params['randomizer']['children'] as $key => $val)
            {
                $template[] = smarty_function_get_randomizer_sections(array("randomizer" => $params['randomizer']['children'][$key]), $smarty, $level, $type);
            }
        }

        return implode("\n", $template);
    }
}

?>