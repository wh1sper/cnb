<?php 

function smarty_function_get_tasks_to_select($params, $smarty, $level=1)
{
	if (!empty($params['task']))
	{
		
		$smarty->assign('task', $params['task']);
		$smarty->assign('level', $level);
		
		$template[] = $smarty->fetch('admin/tasks/task_option.tpl');
		
		if (!empty($params['task']['children']))
		{
			$level += 1;
			foreach($params['task']['children'] as $key=>$val)
			{
				$template[] = smarty_function_get_sections(array("task" => $params['task']['children'][$key]), $smarty, $level);
			}
		}
		
		return implode("\n", $template);
		
	}
}

?>