<?php

function smarty_function_get_randomizer_sections_to_select($params, $smarty, $level=1)
{
	if (!empty($params['randomizer']))
	{

		$smarty->assign('randomizer', $params['randomizer']);
		$smarty->assign('level', $level);

		$template[] = $smarty->fetch('admin/randomizer/section_option.tpl');

		if (!empty($params['randomizer']['children']))
		{
			$level += 1;
			foreach($params['randomizer']['children'] as $key=>$val)
			{
				$template[] = smarty_function_get_randomizer_sections(array("randomizer" => $params['randomizer']['children'][$key]), $smarty, $level);
			}
		}

		return implode("\n", $template);

	}
}

?>