<?php

// отличие контекстного ротатора - выдача в клиентский скрипт всех доступных баннеров с дальнейшей обработкой на фронте

class RotatorRegular extends RotatorBase {

    public function __construct()
    {
        parent::__construct();
    }

    public function rotate()
    {
        $array = array();
        // рандомная выборка из массива дает ротацию с приоритетом

        foreach($this->banners as $k=>$v)
        {
            // дата и время кампании
            if(strlen($v['campaign']['dates']['date_start'])>1)
            {
                $dt = new DateTime($v['campaign']['dates']['date_start']);
                if(time() < $dt->getTimestamp()) continue;
            }

            if(strlen($v['campaign']['dates']['date_end'])>1)
            {
                $dt = new DateTime($v['campaign']['dates']['date_end']);
                if(time() > $dt->getTimestamp()) continue;
            }

            $this->filters->init($v,$this->referer);
            $filters_result = $this->filters->apply();
            // если фильтры прошли успешно, вернется 0 и баннер добавится в массив ротации
            // попутно узнаем, нужно ли  на баннер ставить куки (есть фильты по просмотрам)
            $v['use_cookie'] = $this->filters->use_cookie;

            if($filters_result)
            {
                for($i=0;$i<$v['chance'];$i++)
                {
                    $array[] = $v;
                }
            }

        }

        if(isset($array) && sizeof($array))
        {
            $to = sizeof($array) - 1;
            $rand = rand(0,$to);
            $banner = $array[$rand];
        }

        if(!isset($banner)) $ret = 0;
        else $ret = $banner;

        return $ret;
    }


    public function out()
    {

        $banner = $this->banner;

        if(isset($banner['insert_before']) && !empty($banner['insert_before']))
        {
            echo 'document.write("'.$banner['insert_before'].'");';
        }
        $blank = $banner['is_blank'] ? 'target="_blank" ' : "";

        $out = '';

        // если не контекст, считаем просмотры по обычному. у контекста считается через зеропиксель
        $logfile_name = "cache/impressions/".date("i");
        $handle = fopen($logfile_name,"a+");
        $str = date("Y-m-d").";".date("H").";".$this->zone.";".$banner['id'];
        fwrite($handle,$str."\r\n");
        fclose($handle);

        include("inc/scripts/cookie.php");

        if($banner['type']=='image')
        {
            $out .= '<a '.$blank.'href="'.$this->click_url.'&banner='.$banner['id'].'"><img src="'.$banner['content'].'"></a>';
            echo 'document.write(\''.$out.'\'); ';
        }

        if($banner['type']=='swf')
        {
            echo $this->writeswf($banner,$blank);
        }

        if($banner['type']=='text')
        {
            $out .= '<a '.$blank.'href="'.$this->click_url.'&banner='.$banner['id'].'">'.$banner['content'].'</a>';
            echo 'document.write(\''.$out.'\'); ';
        }

        if($banner['type']=='html')
        {
            echo $banner['content'];
        }

        if($banner['type']=='popup')
        {
            if($banner['popup_type']=='bottom') $popup_template = "inc/popup2.html";
            else $popup_template = "inc/popup.html";

            echo 'if(window.screen.width > 699) { ';

            $margin = isset($banner['margin']) ? ' style="margin-left:-'.$banner['margin']['left'].'px; margin-top: -'.$banner['margin']['top'].'px;"' : '';
            $out = '<div class="overlay"></div><div'.$margin.' class="popup" id="popup1"><span class="close_text">Пропустить рекламу</span><span class="close close_right"><img src="'._HOST_.'/inc/close.png"></span>';

            echo "if(popup_shown === undefined) { ";
            echo file_get_contents($popup_template);
            echo 'document.write(\''.$out.'\');';
            if(strpos($banner['content'],".jpg") || strpos($banner['content'],".gif") || strpos($banner['content'],".png"))
            { 
                if(strpos($_SERVER['HTTP_USER_AGENT'],'Trident'))
                    $out = '<a '.$blank.' href="'.$this->click_url.'&banner='.$banner['id'].'"><img style="width: '.$banner['width'].'; height: '.$banner['height'].'" src="'.$banner['content'].'"></a></div>';
                else
                    $out = '<a '.$blank.' href="" onclick="go(); return false;" id="popup_link" data-link="'.$this->click_url.'&banner='.$banner['id'].'"><img style="width: '.$banner['width'].'px; height: '.$banner['height'].'px" src="'.$banner['content'].'"></a></div>';
                echo 'document.write(\''.$out.'\');';
            }
            if(strpos($banner['content'],".swf"))
            {
                echo $this->writeswf_popup($banner,$blank);
            }

            echo " var popup_shown = 1; }}";
        }

        if(isset($banner['insert_after']) && !empty($banner['insert_after']))
        {
            echo 'document.write("'.$banner['insert_after'].'");';
        }


    }


    public function writeswf($banner,$blank)
    {
        $out = "if (typeof navigator.plugins['Shockwave Flash'] !== 'undefined' || navigator.userAgent.indexOf('MSIE') !== -1)  { ";
        $out .= "$('head').append('<script type=\"text/javascript\" src=\""._SWFOBJECT_PATH_."\"></script>'); ";
        $out .= "document.write('<div id=\"banner".$banner['width']."\"></div>'); ";

        if(strpos($banner['flashvars'],"#link"))
        {
            $goto = $this->click_url.'&banner='.$banner['id'];
            $banner['flashvars'] = str_replace("#link",$goto,"link1=#link&link2=#link");
        }

        $out .= 'var b = new SWFObject("'.$banner['content'].'?'.$banner['flashvars'].'", "myContent", "'.$banner['width'].'", "'.$banner['height'].'", "9.0.0"); b.addParam("wmode", "opaque"); b.addParam("flashVars","'.$banner['flashvars'].'"); b.write("banner'.$banner['width'].'"); ';
        $out .= '} else { ';
        $out .= 'document.write(\'<a '.$blank.'href="'.$this->click_url.'&banner='.$banner['id'].'"><img src="'.$banner['flash_image'].'"></a>\'); }';

        return $out;
    }


    public function writeswf_popup($banner,$blank)
    {
        $out = "if (typeof navigator.plugins['Shockwave Flash'] !== 'undefined' || navigator.userAgent.indexOf('MSIE') !== -1)  { ";
        $out .= "$('head').append('<script type=\"text/javascript\" src=\"/inc/player/swfobject.js\"></script>'); ";
        $out .= "document.write('<div onclick=\"go(); return false;\" id=\"banner".$banner['width']."\"></div>'); ";

        if(strpos($banner['flashvars'],"#link"))
        {
            $goto = $this->click_url.'&banner='.$banner['id'];
            $banner['flashvars'] = str_replace("#link",$goto,"link1=#link&link2=#link");
        }

        $out .= 'var b = new SWFObject("'.$banner['content'].'?'.$banner['flashvars'].'", "myContent", "'.$banner['width'].'", "'.$banner['height'].'", "9.0.0"); b.addParam("wmode", "opaque"); b.addParam("flashVars","'.$banner['flashvars'].'"); b.write("banner'.$banner['width'].'"); ';
        $out .= '} else { ';
        $out .= 'document.write(\'<a '.$blank.'href="'.$this->click_url.'&banner='.$banner['id'].'"><img src="'.$banner['flash_image'].'"></a>\'); }';

        return $out;
    }


}

?>