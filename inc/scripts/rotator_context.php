<?php

// ������� ������������ �������� - ������ � ���������� ������ ���� ��������� �������� � ���������� ���������� �� ������

class RotatorContext extends RotatorBase {

    public function __construct()
    {
        parent::__construct();
    }

    protected function fetchBanners()
    {
        // ����� ������ �� �����
        $data = unserialize(base64_decode(file_get_contents("cache/cache_zones/".$this->zone.".txt")));

        $banners = $data['exclusive'] ? $data['exclusive'] : $data['regular'];
        if(!sizeof($banners)) exit;
        else $this->banners = $banners;
    }

    public function rotate()
    {
        // ������ ��������� ������� ����� �������, �� ���������� ��������� ������
        $array = array();

        foreach($this->banners as $k=>&$v)
        {
            // ���� � ����� ��������
            if(strlen($v['campaign']['dates']['date_start'])>1)
            {
                $dt = new DateTime($v['campaign']['dates']['date_start']);
                if(time() < $dt->getTimestamp()) continue;
            }

            if(strlen($v['campaign']['dates']['date_end'])>1)
            {
                $dt = new DateTime($v['campaign']['dates']['date_end']);
                if(time() > $dt->getTimestamp()) continue;
            }

            $this->filters->init($v,$this->referer);
            $filters_result = $this->filters->apply();
            // ���� ������� ������ �������, �������� 0 � ������ ��������� � ������ �������
            // ������� ������, ����� ��  �� ������ ������� ���� (���� ������ �� ����������)
            $v['use_cookie'] = $this->filters->use_cookie;
            // � ���� �� � ���� ����������� �� ���������

          // ����� ���� ��� ������� ��� json
           foreach($this->filters->keywords as $kw)
           {
               $v['keywords'][] = str_replace('"','',iconv('windows-1251','utf-8',$kw));
           }

            if($filters_result)
            {
                $array[] = $v;
            }

        }

        if(!sizeof($array)) exit;
        else return $array;

    }

    public function out()
    {

        // �������� ������ ��������. �����-�� �� ��� �����������. JS ������� ������������ �������� �������� ������� �� �������� � ������, ����� ��������
        // �������� ������� ����� ��������

        $banners = $this->rotate();

        if(sizeof($banners))
        {
            $objects = array();
            foreach($banners as $banner)
            {
                $object = array();
                $object['id'] = $banner['id'];
                $object['chance'] = $banner['chance'];
                $object['keywords'] = isset($banner['keywords']) ? array_unique($banner['keywords']) : array();
                $object['type'] = $banner['type'];
                $object['content'] = str_replace("{banner_id}",$banner['id'],$banner['content']);
		$object['content'] = str_replace("{banner_zone}",37,$object['content']);

                $object['link'] = $this->click_url."&banner=".$banner['id'];
                $object['width'] = $banner['width'];
                $object['height'] = $banner['height'];
                $object['is_blank'] = $banner['is_blank'];
                $object['flash_image'] = $banner['flash_image'];
                $object['popup_type'] = $banner['popup_type'];
                $object['insert_after'] = str_replace("{rnd}",rand(1000,100000),$banner['insert_after']);
                $object['insert_before'] = $banner['insert_before'];
                $object['flashvars'] = $banner['flashvars'];
                $object['margin'] = isset($banner['margin']) ? $banner['margin'] : array();
                $object['counter'] = _HOST_."/image.php?zone=".$this->zone."&banner=";
                $object['popup_content'] = false;
                if($banner['type']=='popup')
                {
                    if(strpos($banner['content'],".jpg") || strpos($banner['content'],".gif") || strpos($banner['content'],".png"))
                        $object['popup_content'] = 'img';
                    else if(strpos($object['popup_content'],".swf")) $object['popup_content'] = "swf";
	            else $object['popup_content'] = "html";
                }

                $objects[] = $object;
            }

            $is_ie = strpos($_SERVER['HTTP_USER_AGENT'],'Trident') ? 1 : 0;
            echo "var banners_string = '".json_encode($objects)."'; ";
            echo "var close_image = '"._HOST_."/inc/close.png'; ";
            echo "var is_ie = ".$is_ie.";";
            echo "var swfobject_path ='"._SWFOBJECT_PATH_."'; ";
            echo file_get_contents("inc/context.js");
        }

    }

}



?>