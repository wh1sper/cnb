<?php


class Filters {


    private $ok;
    private $banner;
    private $referer;

    // ���������, ������� �� ����
    public $use_cookie = array("campaign"=>array("limit_total"=>0,"limit_session"=>0,"reset"=>0),"banner"=>array("limit_total"=>0,"limit_session"=>0,"reset"=>0));

    public $banner_impressions;
    public $campaign_impressions;

    // ���� ���� ������ �� ��������, �������� ������ ��������
    public $keywords = array();

    public function init($banner,$referer)
    {
        $this->banner = $banner;
        $this->referer = $referer;
        $this->ok = 1;
    }

    public function apply()
    {
        $from['campaign'] = isset($this->banner['campaign']['channels']) ? $this->banner['campaign']['channels'] : array();
        $from['banner'] = isset($this->banner['channels']) ? $this->banner['channels'] : array();

        foreach($from as $k=>$object)
        {
            foreach($object as $channel)
            {
                // ���������� �������� ���� �������� ������
                $results = array();
                if($this->ok)
                {
                    foreach($channel as $setting)
                    {
                        $method = $setting['id'];
                        $result['type'] = $setting['type'];

                        if(method_exists($this,$method))
                        {
                            $result['res'] = $this->$method($k,$setting);
                                // 3 - �������, ������� �� ����� ���������
                                if($result['res']!=3)  $results[] = $result;
                        }

                    }

                    // � ����� � ��� ���� ������ ���� �������� � ������� and => 1, or => 0, and =>1
                    // ��������� �� ����� �� or, ���� �� ���� �� ������ ������ ������� �������
                    // ���� � ��� ��� ������� ������ � , �� ���� ����� ���� - ���� � ��� ���� �� ���� 0, �� �������� �� ������
                    $i = 0;
                    foreach($results as $res)
                    {
                        if($res['type']=='or') $i++;
                        $parts[$i][] = $res['res'];
                    }


                    // �������� ��� ����� - ���� ����� ��� � �����-�� ���� ������������� �������, �������
                    if(isset($parts))
                    {
                        foreach($parts as $k=>$part){
                            if(in_array(0,$part)) unset($parts[$k]);
                        }

                        // ���� ������ �� �������� - �������� ������ �� ������, ������ �� ��������
                        if(!sizeof($parts)) $this->ok = 0;
                    }

                }
            }
        }
        return $this->ok;
    }


    private function get_geo()
    {
        if (!empty($_SERVER['HTTP_X_REAL_IP'])) // ���� ���������� nginx
        {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        }
        elseif (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

//	$ip = "87.224.211.122";



        include_once("inc/ip/ipgeobase.php");
        $gb = new IPGeoBase();
        $geodata = $gb->getRecord($ip);
        return $geodata;
    }

    ///// ������ � ������� ��������

    private function limit_total($object_type,$data)
    {
        $ret = 1;

        if($object_type=='banner') { $id = $this->banner['id']; }
        if($object_type=='campaign') { $id = $this->banner['id_campaign']; }

        $array = $object_type."_impressions";

        $ar = $this->$array;

        if(isset($ar['total'][$id]))
        {
            if($ar['total'][$id] >= $data['value'])
                $ret = 0;
        }

        // ���� �� ����� �����
        $this->use_cookie[$object_type]['limit_total'] = 1;

        return $ret;
    }

    private function limit_session($object_type,$data)
    {
        $ret = 1;

        if($object_type=='banner') { $id = $this->banner['id']; }
        if($object_type=='campaign') { $id = $this->banner['id_campaign']; }

        $array = $object_type."_impressions";
        $ar = $this->$array;

        if(isset($ar['session'][$id]))
        {
            if($ar['session'][$id] >= $data['value']) {
                $ret = 0;
            }
        }

        // ���� �� ����� �����
        $this->use_cookie[$object_type]['limit_session'] = 1;

        return $ret;
    }

    private function limit_reset($object_type,$data)
    {
        $this->use_cookie[$object_type]['reset'] = $data['value'];
        return 3;
    }

    private function limit_time($object_type,$data)
    {
        return !in_array(date("H"),$data['value']) ? 0 : 1;
    }

    private function limit_days($object_type,$data)
    {
        return !in_array(date("N"),array_keys($data['value'])) ? 0 : 1;
    }

    private function limit_mobile ($object_type,$data)
    {
        $ret = 0;
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) $ret = 1;
        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) $ret = 1;
        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) $ret = 1;

        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');

        if (in_array($mobile_ua,$mobile_agents)) $ret = 1;

        return $ret;

    }

    private function limit_impression($object_type,$data)
    {
        $ret = 0;
        if($this->banner['stat']['impressions'] <= $data['value']) $ret = 1;
        return $ret;
    }

    private function limit_impression_total($object_type,$data)
    {
        $ret = 0;
        if($object_type=='banner') { $field = "total_impressions"; }
        if($object_type=='campaign') { $field = "campaign_impressions"; }
        if($this->banner[$field] <= $data['value']) $ret = 1;
        return $ret;
    }

    private function limit_geo($object_type,$data)
    {
	
        $geodata = $this->get_geo();
	return trim($geodata['city']) == $data['value'] ? 1 : 0;
    }

    private function limit_geo_state($object_type,$data)
    {
        $geodata = $this->get_geo();
        return trim($geodata['region']) == $data['value'] ? 1 : 0;
    }

    private function limit_geo_region($object_type,$data)
    {
        $geodata = $this->get_geo();
        return trim($geodata['district']) == $data['value'] ? 1 : 0;
    }


    private function url_not($object_type,$data)
    {
        if(empty($this->referer)) return 1;
        $ret = 0;

        if(strpos($this->referer,$data['value']) === false)
            $ret = 1;

        return $ret;
    }

    private function url($object_type,$data)
    {
        if(empty($this->referer)) return 1;
        $ret = 1;

        if(strpos($this->referer,$data['value']) === false)
            $ret = 0;

        return $ret;

    }

    // ��������. �������� �������� �� ������-�����, ����� ��-��-��
    private function keyword($object_type,$data)
    {
        if(strpos($data['value'],","))
        {
            $ar = explode(",",$data['value']);
            foreach($ar as $kw)
                $this->keywords[] = '"'.trim($kw).'"';
        }
        else $this->keywords[] = '"'.$data['value'].'"';
        return 1;
    }




}



?>