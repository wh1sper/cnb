<?php

include("inc/scripts/filters.php");

// ������� ����� ��� ��������� (���� ��� - ����������� � �����������)

abstract class RotatorBase {

    // id ����
    protected $zone;
    // ������ ������ � ������� �� ��������� ���� (������������ ������������ prepare � �������)
    protected $sites;
    protected $referer;
    // ��� ������� ��� ���� �� ���� ������������� ����� (��� �� �� ����)
    protected $banners;
    // ��������� ��� ������ ������
    protected $banner;
    // ������ ������ � ���������� ������������ �������� � ��������, �� ���
    protected $user_impressions;
    // ����� ����������, � ��� ���������� ��� ������ ��� ����������� ��������
    protected $filters;
    protected $click_url;

    public function __construct()
    {
        $this->sites = unserialize(base64_decode(file_get_contents("cache/sites.txt")));

        if(!isset($_SERVER['HTTP_REFERER']))
        {
            die("������ ������");
        }
        else $this->referer = $_SERVER['HTTP_REFERER'];

        $ok = 0;

        // �������� �� ����� ������ � ������ ������� + ���������
        foreach($this->sites as $site)
        {
            $url = str_replace(array("http://","www."),"",$site['url']);
            if(strpos($_SERVER['HTTP_REFERER'],"http://".$url) !== false) $ok = 1;
            if(strpos($_SERVER['HTTP_REFERER'],".".$url) !== false) $ok = 1;
        }

        if(!$ok)
        {
           die("������ ������");
        }

        if(!is_numeric($_GET['zone']))
        {
            die("����������� ������� �� �������� ������");
        }
        else $this->zone = $_GET['zone'];

        $this->click_url = _HOST_."/click.php?zone=".$this->zone;

        $this->getCookies();
        $this->initFilters();
        $this->fetchBanners();

    }

    // ����������� ��� ������� ��� ���� �� ��������� ����
    protected function fetchBanners()
    {
        // ����� ������ �� �����
        $data = unserialize(base64_decode(file_get_contents("cache/cache_zones/".$this->zone.".txt")));

        // ������� ��������� ����������� ��������
        if($data['exclusive'])
        {

            $this->banners = $data['exclusive'];
            // ������ �������
            $banner = $this->rotate();
            if($banner)
            {
                $this->banner = $banner;
            }
            // ����������� ������� �� ������ �������, ����� �������
            else
            {
                $this->banners = $data['regular'];
                if(!sizeof($this->banners)) exit;
                $banner = $this->rotate();
                if($banner) $this->banner = $banner;
            }
        }
        else
        {
            $this->banners = $data['regular'];
            if(!sizeof($this->banners)) exit;
            $banner = $this->rotate();
            if($banner) $this->banner = $banner;
        }
        //     $banners = $data['exclusive'] ? $data['exclusive'] : $data['regular'];
        //   if(!sizeof($banners)) exit;
        //  else $this->banners = $banners;

    }

    // ����������� ��� ������ � ���������� ��������/�������� �� ���
    public function getCookies()
    {

        if(isset($_COOKIE['cnb_banner_total']))
        {
            foreach($_COOKIE['cnb_banner_total'] as $key=>$val)
            {
                $this->user_impressions['banner']["total"][$key] = $val;
            }

        }

        if(isset($_COOKIE['cnb_banner_session']))
        {
            foreach($_COOKIE['cnb_banner_session'] as $key=>$val)
            {
                $this->user_impressions['banner']["session"][$key] = $val;
            }
        }

        if(isset($_COOKIE['cnb_campaign_session']))
        {
            foreach($_COOKIE['cnb_campaign_session'] as $key=>$val)
            {
                $this->user_impressions['campaign']["session"][$key] = $val;
            }
        }

        if(isset($_COOKIE['cnb_campaign_total']))
        {
            foreach($_COOKIE['cnb_campaign_total'] as $key=>$val)
            {
                $this->user_impressions['campaign']["total"][$key] = $val;
            }
        }

	
	/* ������������ ���, �� ���� ���������� ����� ������

 	foreach($_COOKIE as $k=>$v)
	{
	if(strpos($k,"cnb_"))
	{
		$cookie_data = explode("_",$k);
		$object = $cookie_data[1];
		$type = $cookie_data[2];
		foreach($v as $cookval_key=>$cookval_val)
		{
			$this->user_impressions[$object][$type][$cookval_key] = $cookval_val;
		}
	}
 	}
  	*/	

    }

    protected function initFilters()
    {
        $this->filters = new Filters();
        $this->filters->campaign_impressions = isset($this->user_impressions['campaign']) ? $this->user_impressions['campaign'] : array();
        $this->filters->banner_impressions = isset($this->user_impressions['banner']) ? $this->user_impressions['banner'] : array();
    }

    // ������� ���������� ���������� ������� � ������ ��� ������
    abstract function rotate();
    // �������� ������� ������������ ������
    abstract function out();

}