<?php
/*!
  FileDrop Revamped - server-side upload handler sample
  in public domain  | http://filedropjs.org

 ***

  This is an example of server-side script that handles both AJAX and IFrame uploads.

  AJAX upload provides raw file data as POST input while IFrame is a POST request
  with $_FILES member set.

  Result is either output as HTML with JavaScript code to invoke the callback
  (like JSONP) or in plain text if none is given (it's usually absent on AJAX).
*/

// If an error causes output to be generated before headers are sent - catch it.
ob_start();

// Callback name is passed if upload happens via iframe, not AJAX (FileAPI).
$callback = &$_REQUEST['fd-callback'];

if (!empty($_FILES['fd-file']) and is_uploaded_file($_FILES['fd-file']['tmp_name'])) {
  // Regular multipart/form-data upload.
  $name = $_FILES['fd-file']['name'];
  $data = file_get_contents($_FILES['fd-file']['tmp_name']);
} else {
  // Raw POST data.
  $name = urldecode(@$_SERVER['HTTP_X_FILE_NAME']);
  $data = file_get_contents("php://input");
}

file_put_contents("content/".$name,$data);

$output = sprintf('%s; received %s bytes, CRC32 = %08X, MD5 = %s', $name,
                  number_format(strlen($data)), crc32($data), strtoupper(md5($data)));

$opt = &$_REQUEST['upload_option'];
isset($opt) and $output .= "\nReceived upload_option with value $opt";

if ($callback) {

  header('Content-Type: text/html; charset=utf-8');

  $output = addcslashes($output, "\\\"\0..\x1F");

  echo '<!DOCTYPE html><html><head></head><body><script type="text/javascript">',
       "try{window.top.$callback(\"$output\")}catch(e){}</script></body></html>";
} else {

  header('Content-Type: text/plain; charset=utf-8');
  echo $output;
}
